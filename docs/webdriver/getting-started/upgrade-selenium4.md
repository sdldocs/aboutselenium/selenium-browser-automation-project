# Selenium 4로 업그레이드
<span style="font-size:120%">아직도 Selenium 3을 사용하고 있나요? 최신 버전으로 업그레이드에 도움을 준다!</span>

공식적으로 지원되는 언어(Ruby, JavaScript, C#, Python, and Java) 중 하나를 사용하는 경우 Selenium 4로 업그레이드하는 것은 어렵지 않은 과정이다. 몇 가지 문제가 발생할 수 있으며 이 사이트를 통해 문제를 해결할 수 있다. 프로젝트 종속성을 업그레이드하는 단계를 수행하고 버전 업그레이드가 가져오는 주요 폐지 및 변경 사항을 알아본다.

Selenium 4로 업그레이드하기 위해 수행할 단계는 다음과 같다.

- 테스트 코드 준비
- 종속성 업그레이드
- 잠재적 오류와 사용 중지 메시지

> **Note**: Selenium 3.x 버전이 개발되는 동안 W3C WebDriver 표준에 대한 지원이 구현되었다. 이 새로운 프로토콜과 레거시 JSON 와이어 프로토콜 모두를 지원한다. 버전 3.11에서 Selenium 코드는 W3C1 규격을 준수한다. 최신 버전의 Selenium 3의 W3C 호환 코드는 Selenium 4에서 예상대로 작동할 것이다.

## 테스트 코드 준비
셀레늄 4는 레거시 프로토콜에 대한 지원을 제거하고 후드 아래에 기본적으로 W3C WebDriver 표준을 사용한다. 대부분의 경우 이 구현은 최종 사용자에게 영향이 없다. `Capabilities`와 `Actions` 클래스가 주요 예외이다.

### Capabilities
테스트 기능이 W3C를 준수하도록 구성되지 않은 경우 세션이 시작되지 않을 수 있다. 다음은 W3C WebDriver의 표준 기능 목록이다.

- `browserName`
- `browserVersion` (`version`으로 변경)
- `platformName` (`platform`으로 변경)
- `acceptInsecureCerts`
- `pageLoadStrategy`
- `proxy`
- `timeouts`
- `unhandledPromptBehavior`

표준 기능의 최신 목록은 [W3C WebDriver](https://www.w3.org/TR/webdriver1/#capabilities)에서 확인할 수 있다.

위 목록에 없는 기능은 벤더 접두사를 포함해야 한다. 이는 브라우저별 기능뿐만 아니라 클라우드 공급업체별 기능에도 적용된다. 예를 들어 클라우드 공급업체가 테스트에 `build`와 `name` 기능을 사용하는 경우 테스트를 `cloud:options` 블록으로 묶어야 한다 (해당 접두사는 클라우드 공급업체에 문의하시오).

#### Before
```Python
caps = {}
caps['browserName'] = 'firefox'
caps['platform'] = 'Windows 10'
caps['version'] = '92'
caps['build'] = my_test_build
caps['name'] = my_test_name
driver = webdriver.Remote(cloud_url, desired_capabilities=caps)
```

#### After
```Python
from selenium.webdriver.firefox.options import Options as FirefoxOptions

options = FirefoxOptions()
options.browser_version = '92'
options.platform_name = 'Windows 10'
cloud_options = {}
cloud_options['build'] = my_test_build
cloud_options['name'] = my_test_name
options.set_capability('cloud:options', cloud_options)
driver = webdriver.Remote(cloud_url, options=options)
```

## 종속성 업그레이드
아래 하위 섹션을 확인하여 Selenium 4를 설치하고 프로젝트 종속성을 업그레이드하시오.

### Python
파이썬을 사용하기 위해 가장 중요한 변경 사항은 최소 요구 버전이다. Selenium 4는 최소 파이썬 3.7 이상이 필요하다. 자세한 내용은 [Python Package Index]()에서 확인할 수 있다. command line에서 업그레이드하려면 다음과 같이 한다.

```Python
pip install selenium==4.4.3
```

## 잠재적 오류와 사용 중지 메시지
다음은 Selenium 4로 업그레이드한 후 발생할 수 있는 사용 중지 메시지를 해결하는 데 도움이 되는 코드 예이다.

### Python
<span style="font-size:120%"> <span style="color:red">executable_path has been deprecated, please pass in a Service object</span></span>

Selenium 4에서는 사용 중지 경고를 방지하기 위해 서비스 객체에서 드라이버의 `executable_path`를 설정해야 한다. (또는 경로를 설정하지 말고 필요한 드라이버가 시스템 `PATH`에 있는지 확인하시오.)

#### Before
```Python
from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option("useAutomationExtension", False)
driver = webdriver.Chrome(executable_path=CHROMEDRIVER_PATH, options=options)
```

#### After
```Python
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService

options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option("useAutomationExtension", False)
service = ChromeService(executable_path=CHROMEDRIVER_PATH)
driver = webdriver.Chrome(service=service, options=options)
```

## Summary
Selenium 4로 업그레이드할 때 고려해야 할 주요 변경 사항을 검토했다. 업그레이드를 위해 테스트 코드가 준비될 때 다룰 다양한 측면을 다루며, 새 버전의 Selenium을 사용할 때 나타날 수 있는 잠재적인 문제를 방지하는 방법에 대하여 알아 보았다. 마지막으로 업그레이드 후 발생할 수 있는 일련의 가능한 문제에 대해서도 다루었으며 이러한 문제에 대한 잠재적인 해결책을 공유하였다.

(출처: [How To Upgrade to Selenium 4](https://saucelabs.com/resources/articles/how-to-upgrade-to-selenium-4))

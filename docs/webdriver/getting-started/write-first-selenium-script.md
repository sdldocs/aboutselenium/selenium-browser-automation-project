# 첫 Selenium 스크립트 작성
<span style="font-size:120%">Selenium 스크립트를 작성하기 위한 단계별 지침.</span>

[Selenium을 설치](install-selenium-library.md)하고 [Driver](install-browser-drivers.md)를 설치하여 Selenium 코드를 작성할 준비를 한다.

## 8 기본 요소

Selenium이 하는 모든 일은 브라우저에 명령을 보내 무언가를 하거나 정보 요청을 보내는 것이다. Selenium을 사용하는 대부분의 작업은 다음과 같은 기본 명령을 조합한 것이다.

### 1. 세션 시작
세션 시작에 대한 보다 자세한 내용은 [드라이버 세션](../driver-sessions.md)에 대한 설명서를 참조하시오

```python
    driver = webdriver.Chrome()
```

### 2. 브라우저에서 작업 수행
이 예에서는 웹 페이지를 방문한다.

```python
    driver.get("https://www.selenium.dev/selenium/web/web-form.html")
```

### 3. 브라우저 정보 요청
요청할 수 있는 브라우저 정보에는 window 핸들, 브라우저 크기/위치, 쿠키, 알림 등 여러 타입이 있다.

```python
    title = driver.title
```

### 4. 대기 전략 수립
코드를 브라우저의 현재 상태와 동기화하는 것은 Selenium의 가장 큰 과제 중 하나이며, 순조롭게 하는 것은 고급 주제이다.

기본적으로 요소를 찾기 전에 페이지에 요소가 있고 요소와 상호 작용하기 전에 요소가 상호 작용이 가능한 상태인지 확인해야 한다.

암묵적인 대기가 최선의 해결책은 아니지만, 여기서 시연하는 것이 가장 쉬우므로 자리 표시자로 이를 사용하겠다.

[대기 전략](../waits.md)에 대해 자세히 알아보시오.

```python
    driver.implicitly_wait(0.5)
```

### 5. 요소 찾기
대부분의 Selenium 세션의 명령은 대부분 요소와 관련되어 있으며, 먼저 [요소를 찾지](../web-elements.md/) 않으면 명령과 상호 작용할 수 없다.

```python
    text_box = driver.find_element(by=By.NAME, value="my-text")
    submit_button = driver.find_element(by=By.CSS_SELECTOR, value="button")
```

### 6. 요소에 작업 수행
요소에 대해 수행할 수 있는 작업은 거의 없지만 자주 사용된다.

```python
    text_box.send_keys("Selenium")
    submit_button.click()
```

### 7. 요소 정보 요청
요소는 다양한 [요청 가능한 정보](../elements/information.md)를 저장하고 있다.

```python
    value = message.text
```

### 8. 세션 종료
이렇게 하면 드라이버 프로세스가 종료되며 기본적으로 브라우저도 닫힙니다. 이 드라이버 인스턴스에 더 이상 명령을 보낼 수 없다.

```python
    driver.quit()
```

## 통합
이 여덟 가지를 테스트 실행기를 수행할 수 있는 assertion과 함께 완전한 스크립트로 결합해 본다.

```python
from selenium import webdriver
from selenium.webdriver.common.by import By


def test_eight_components():
    driver = webdriver.Chrome()

    driver.get("https://www.selenium.dev/selenium/web/web-form.html")

    title = driver.title
    assert title == "Web form"

    driver.implicitly_wait(0.5)

    text_box = driver.find_element(by=By.NAME, value="my-text")
    submit_button = driver.find_element(by=By.CSS_SELECTOR, value="button")

    text_box.send_keys("Selenium")
    submit_button.click()

    message = driver.find_element(by=By.ID, value="message")
    value = message.text
    assert value == "Received!"

    driver.quit()
```

## 테스트 실행기
테스트에 Selenium을 사용하는 경우 테스트 실행 도구를 사용하여 selenium 코드를 실행하려고 한다.

이 문서의 많은 코드 예제는 예제 저장소에서 찾을 수 있다. 각 언어마다 여러 옵션이 있지만 다음은 예제에서 사용하는 옵션들이다.

```python
// Add instructions
```

## 다음 단계
배운 것을 사용하여 Selenium 코드를 구축하세요.

필요한 추가 기능을 찾으려면 [WebDriver 설명서](../../webdriver.md)의 나머지 부분을 읽어 보시오.

# 브라우저 드라이버 설치
<span style="font-size:120%">브라우저를 자동화하도록 시스템을 설정한다.</span>

Selenium은 WebDriver를 통해 Chrome/Chromium, Firefox, Internet Explorer, Edge, and Safari 등 시중 주요 브라우저를 모두 지원한다. 가능한 경우, WebDriver는 자동화를 위한 브라우저의 내장된 지원을 사용하여 브라우저를 구동한다.

Internet Explorer,를 제외한 모든 드라이버들은 브라우저 벤더들이 자체적으로 제공하기 때문에 표준 셀레늄 배포판에 포함되지 않는다. 여기 에서는 다양한 브라우저를 시작하기 위한 기본 요구 사항에 대해 설명한다.

[driver configuration](https://www.selenium.dev/documentation/webdriver/drivers/) 설명서에서 드라이버를 시작하기 위한 고급 옵션에 대해 읽어 보시오.

## 드라이버를 사용하는 4 방법

### 1. Selenium Manager (Beta)

<span style="font-size:120%">Selenium v4.6</span>

Selenium Manager는 Selenium을 즉시 실행할 수 있는 작업 환경을 제공한다. Chrome, Firefox 및 Edge 드라이버가 PATH에 없는 경우, Selenium Manager의 베타 1에서 해당 드라이버를 구성한다. 추가 구성이 필요 없다. 향후 Selenium Manager의 출시는 필요하다면 브라우저까지 다운로드할 것이다.

자세한 내용은 [Selenium Manager](https://www.selenium.dev/blog/2022/introducing-selenium-manager/) 블로그 공지사항을 참조하시오.

### 2. 드라이버 관리 소프트웨어
대부분의 컴퓨터에서는 브라우저를 자동으로 업데이트하지만 드라이버는 업데이트하지 않는다. 브라우저에 적합한 드라이버를 얻으려면 여러 타사 라이브러리를 사용해야 한다.

1. Python을 위한 WebDriver Manager를 import한다.

```python
from webdriver_manager.chrome import ChromeDriverManager
```

2. `install()`을 사용하여 관리자가 사용하는 위치를 가져와 서비스 클래스 인스턴스의 드라이버에 이를 전달한다.

```python
    service = ChromeService(executable_path=ChromeDriverManager().install())

    driver = webdriver.Chrome(service=service)
```

### 3. `PATH` 환경 변수
이 옵션을 사용하려면 먼저 드라이버를 수동으로 다운로드해야 한다 (링크는 [빠른 참조](#quick-reference) 섹션 참조).

이 옵션은 코드를 업데이트하지 않고도 드라이버의 위치를 변경할 수 있는 유연한 옵션이며, 각 컴퓨터에서 드라이버를 동일한 위치에 배치할 필요 없이 여러 컴퓨터에서 작동한다.

드라이버를 `PATH`에 이미 나열된 디렉터리에 배치하거나 디렉토리에 배치한 다음 `PATH`에 추가할 수 있다.

`PATH`에 이미 정의된 디렉터리를 보려면 터미널을 열고 다음을 실행하시오.

```bash
echo $PATH
```

드라이버 저정된 디렉토리가 나열된 디렉터리에 아직 없는 경우 저장된 디렉터리를 `PATH`에 추가할 수 있다.

```bash
echo 'export PATH=$PATH:/path/to/driver' >> ~/.bash_profile
source ~/.bash_profile
```

드라이버를 시작하여 정확하게 추가되었는지 테스트할 수 있다.

```bash
chromedriver
```

`PATH`가 위에서 올바르게 구성된 경우 드라이버 시작과 관련된 일부 정보가 출력된다.

```
Starting ChromeDriver 95.0.4638.54 (d31a821ec901f68d0d34ccdbaea45b4c86ce543e-refs/branch-heads/4638@{#871}) on port 9515
Only local connections are allowed.
Please see https://chromedriver.chromium.org/security-considerations for suggestions on keeping ChromeDriver safe.
ChromeDriver was started successfully.
```

`Ctrl+C`를 눌러 명령 프롬프트로 다시 돌아올 수 있다.

### 4. 하드 코딩된 위치
위의 3번째 방법과 마찬가지로 드라이버를 수동으로 다운로드해야 한다 (링크는 [빠른 참조](#quick-reference) 섹션 참조). 코드 자체에 위치를 지정하면 시스템에서 환경 변수를 파악할 필요가 없다는 장점이 있지만 코드의 유연성이 훨씬 떨어진다는 단점이 있다.

```pyton
from selenium.webdriver.chrome.service import Service
from selenium import webdriver

service = Service(executable_path="/path/to/chromedriver")
driver = webdriver.Chrome(service=service)
```

## 빠른 참조 <a id="quick-reference"></a>

| 브라우저 | 지원되는 OS | 관리자 | 다운로드 | 이슈 추척 |
|--------|----------|-------|--------|--------| 
| Chromium/Chrome | Windows/macOS/Linux | Google | [다운로드](https://chromedriver.chromium.org/downloads) | [이슈](https://bugs.chromium.org/p/chromedriver/issues/list) |
| Firefox | Windows/macOS/Linux | Mozilla | [다운로드](https://github.com/mozilla/geckodriver/releases) | [이슈](https://github.com/mozilla/geckodriver/issues) |
| Edge | Windows/macOS/Linux | Microsoft | [다운로드](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/) | [이슈](https://github.com/MicrosoftEdge/EdgeWebDriver/issues) |
| Internet Explorer | Windows | Selenium Project | [다운로드](https://www.selenium.dev/downloads) | [이슈](https://github.com/SeleniumHQ/selenium/labels/D-IE) |
| Safari | macOS Sierra 이상 | Apple | 내장 | [이슈](https://bugreport.apple.com/logon) |
> **Note**: Opera 드라이버는 더 이상 Selenium의 최신 기능과 함께 작동하지 않으며 현재 공식적으로 지원되지 않는다.
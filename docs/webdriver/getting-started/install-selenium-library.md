# Selenium 라이브러리 설치
<span style="font-size:120%">사용할 프로그래밍 언어를 위한 Selenium 라이브러리를 설정한다.</span>

먼저 자동화 프로젝트에 대한 Selenium 바인딩을 설치해야 한다. 라이브러리 설치 프로세스는 사용할 언어에 따라 다르다. [Selenium download page](https://www.selenium.dev/downloads/)에서 최신 버전을 사용하고 있는지 확인하시오.

## 프로그래밍 언어별 요구사항
각 Selenium 버전에 대해 지원되는 최소 Python 버전은 [PyPi](https://pypi.org/project/selenium/)의 `Supported Python Versions`에서 확인할 수 있다

Selenium을 설치하는 몇 가지 방법이 있다.

### Pip
```python
pip install selenium
```

### 다운로드
또는 [PyPI source archive](https://pypi.org/project/selenium/#files)(selenium-x.x.x.tar.gz)를 다운로드하고 *setup.py*을 사용하여 설치할 수 있다.

```python
python setup.py install
```

### 프로젝트에 요청
프로젝트에 사용하려면 `requirements.txt` 파일에 추가한다.

```
selenium==4.8.0
```

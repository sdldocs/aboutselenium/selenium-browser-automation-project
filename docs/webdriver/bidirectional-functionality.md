# 양방향 기능 (BiDirectional Functionality)
Selenium은 일반적으로 브라우저 자동화와 특정 테스트 모두에 유용한 양방향 기능을 사용하는 안정적인 크로스 브라우저 API를 제공하는 수단으로 WebDriver BiDirectional Protocol을 개발하였다. 이를 위해 브라우저 공급업체와 협력하고 있다. 지금까지 이 기능을 찾는 개발자들은 모든 좌절과 한계를 안고 CDP(Chrome DevTools Protocol)에 의존해야 했다.

엄격한 요청/응답 명령의 기존 WebDriver 모델은 WebSocket을 통해 사용자 에이전트에서 제어 소프트웨어로 이벤트를 스트리밍하는 기능으로 보완되어 브라우저 DOM의 이벤트 특성과 보다 잘 일치한다.

테스트를 브라우저의 특정 버전에 연결하는 것은 좋은 생각이 아니므로 Selenium 프로젝트는 가능한 한 WebDriver BiDi를 사용할 것을 권장한다.

사양이 작동하는 동안 브라우저 공급업체는 [WebDriver BiDirectional Protocol](https://w3c.github.io/webdriver-bidi/) 또한 구현하고 있다. [웹 플랫폼 테스트 대시보드](https://wpt.fyi/results/webdriver/tests/bidi?label=experimental&label=master&aligned&view=subtest)를 참조하여 브라우저 공급업체가 얼마나 잘 준수하고 있는지 확인하시오. Selenium은 브라우저 공급업체를 따라잡기 위해 노력하고 있으며 W3C BiDi API 구현을 시작했다. 목표는 API가 W3C를 준수하고 다른 언어 바인딩 간에 균일하도록 하는 것이다.

그러나 사양과 해당 Selenium 구현이 완료될 때까지 CDP가 제공하는 유용한 많은 기능이 있다. Selenium은 CDP를 사용하는 몇 가지 유용한 도우미 클래스를 제공한다.

---

## [양방향 API (CDP 구현)](bidirectional/bidi-api.md)

## [RemoteWebDriver 양방향 API (CDP 구현)](bidirectional/bidi-remotewebdriver.md)

## [Chrome DevTools](bidirectional/chrome-devtools.md)

## [양방향 API (W3C 표준)](bidirectional/bidirectional-w3c.md)
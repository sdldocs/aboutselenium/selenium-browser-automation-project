# 문제 해결 지원
<span style="font-size:120%">WebDriver 문제를 해결하는 방법</span>

Selenium 오류의 근본 원인은 항상 명확 하지는 않다.

1. Selenium과 관련된 가장 일반적인 오류는 동기화에서 발생한다. [대기 전략](waits.md)살펴보자. 동기화 전략인지 확실하지 않은 경우 문제가 발생할 때 일시적으로 큰 절전 모드를 하드 코딩해 볼 수 있으며, 명시적 대기를 추가하는 것이 도움이 되는지 알 수 있다.
2. 프로젝트에 보고되는 많은 오류는 실제로 Selenium이 명령을 보내기 위해 사용되는 드라이버의 문제로 인해 발생한다. 여러 브라우저에서 명령을 실행하여 드라이버 문제를 제외할 수 있다.
3. 작업 수행 방법에 대해 질문이 있는 경우 [지원 옵션](https://www.selenium.dev/support/)에서 지원을 받을 수 있는 방법을 확인해 보자.
4. Selenium 코드에 문제가 있다고 생각되면 GitHub에 [버그 보고서](https://github.com/SeleniumHQ/selenium/issues/new?assignees=&labels=I-defect%2Cneeds-triaging&template=bug-report.yml&title=%5B%F0%9F%90%9B+Bug%5D%3A+)를 제출한다.

---

### [일반 오류에 대한 이해](troubleshooting/errors.md)
Selenium 코드의 다양한 문제를 처리하는 방법.

### [Selenium 명령어 로깅](troubleshooting/logging.md)
Selenium 실행에 대한 정보 얻기

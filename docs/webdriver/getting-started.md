# 시작하기
<span style="font-size:120%">Selenium을 처음 사용하면 몇 가지 리소스를 이용하여 바로 속도를 높일 수 있다.</span>

Selenium은 WebDriver를 통해 시장의 모든 주요 브라우저의 자동화를 지원한다. WebDriver는 웹 브라우저의 동작을 제어하기 위한 언어 중립 인터페이스를 정의하는 API와 프로토콜이다. 드라이버라고 불리는 특정 WebDriver 구현으로 각 브라우저를 지원한다. 드라이버는 브라우저에 대한 위임을 담당하는 구성 요소이며 Selenium과 브라우저간의 통신을 처리한다.

이러한 분리는 브라우저 공급업체들이 그들의 브라우저에 대한 구현에 책임을 지도록 하기 위한 의식적인 노력의 일부이다. Selenium은 가능한 한 이러한 타사 드라이버를 사용하지만, 현실이 아닌 경우를 위해 프로젝트에서 유지 관리하는 자체 드라이버도 제공한다.

Selenium 프레임워크는 크로스 브라우저 및 크로스 플랫폼 자동화를 가능하여, 서로 다른 브라우저 백엔드를 투명하게 사용할 수 있도록 하는 사용자 대면 인터페이스를 통해 이 모든 부분을 연결한다.

Selenium 설정은 다른 상용 도구의 설정과는 상당히 다르다. Selenium 코드 작성을 시작하기 앞서 먼저 선택한 언어, 사용할 브라우저 및 해당 브라우저의 드라이버에 대한 언어 바인딩 라이브러리를 설치해야 한다.

> **Note**: 현재(2023년 2월) 버전에서는 우선 Python을 프로그래밍 언어로 사용하는 것으로 시작한다. 차후 필요에 따라 다른 언어를 추가하도록 할 것이다.

**<i>아래 링크를 따라 Selenium WebDriver를 사용하시오.</i>**

낮은 코드/녹음 및 재생 도구로 시작하려면 [Selenium IDE](../ide.md)를 확인하시오

작업이 완료되어 테스트를 확장하고 싶다면 [Selenium Grid](../grid.md)를 확인하시오.

---

### [Selenium 라이브러리 설치](getting-started/install-selenium-library.md)
사용할 프로그래밍 언어를 위한 Selenium 라이브러리를 설정한다.

### [브라우저 드라이버 설치](getting-started/install-browser-drivers.md)
브라우저를 자동화하도록 시스템을 설정한다.

### [첫 Selenium 스크립트 작성](getting-started/write-first-selenium-script.md)
Selenium 스크립트를 작성하기 위한 단계별 지침.

### [Selenium 4로 업그레이드](getting-started/upgrade-selenium4.md)
아직도 Selenium 3을 사용하고 있나요? 최신 버전으로 업그레이드에 도움을 준다!

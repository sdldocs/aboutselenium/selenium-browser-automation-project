WebDriver는 일반적으로 차단 API가 있다고 할 수 있다. WebDriver는 브라우저에 무엇을 해야 하는지 지시하는 프로세스를 벗어난 라이브러리이며 웹 플랫폼이 본질적으로 비동기적인 특성을 가지고 있기 때문에 WebDriver는 DOM의 활성 실시간 상태를 추적하지 않는다. 여기서 논의할 몇 가지 도전 과제가 있다.

경험상 Selenium과 WebDriver의 사용으로 인해 발생하는 대부분의 간헐적인 문제는 브라우저와 사용자의 지시 사이에서 발생하는 *race 조건*과 관련이 있다. 예를 들어, 사용자가 브라우저에 페이지로 이동하도록 지시한 다음 요소를 찾을 때 해당 요소 없는 오류가 발생할 수 있다.

다음 문서를 보자.

```html
<!doctype html>
<meta charset=utf-8>
<title>Race Condition Example</title>

<script>
  var initialised = false;
  window.addEventListener("load", function() {
    var newElement = document.createElement("p");
    newElement.textContent = "Hello from JavaScript!";
    document.body.appendChild(newElement);
    initialised = true;
  });
</script>
```

WebDriver 명령은 아래과 같이 단순해 보일 수 있다.

```Python
driver.navigate("file:///race_condition.html")
el = driver.find_element(By.TAG_NAME, "p")
assert el.text == "Hello from JavaScript!"
```

여기서 문제는 WebDriver에서 사용되는 기본 [페이지 로드 전략](drivers/browser-options.md#pageloadstrategy)이 호출에서 *navigate*로 반환하기 전에 `document.readyState`를 `"complete"`로 변경를 수신한다. `p` 요소는 문서 로드가 완료된 *다음* 추가되므로 이 WebDriver 스크립트는 간헐적으로 파행을 발생사킬 수 있다. 명시적으로 대기하거나 차단하지 않고 비동기적으로 트리거하는 요소나 이벤트에 대해 보장할 수 없기 때문에 간헐적으로 파행이 발생할 수 있다.

다행히 `WebElement.click`과 `WebElement.sendKeys`와 같은 [*WebElement*](web-elements.md) 인터페이스에서 사용할 수 있는 일반 명령 집합이 있다. 브라우저에서 명령이 완료될 때까지 함수 호출이 반환되지 않거나 콜백 스타일 언어로 콜백이 트리거되지 않는다는 점에서 동기화를 보장한다. 고급 사용자 상호 작용 API인 [*Keyboard*](actions-api/keyboard.md)와 [*Mouse*](actions-api/mouse.md)는 비동기 명령을 "명령하는 대로 실행"하기 위한 명시적으로 의도된 예외들이다.

대기 중인 작업은 다음 단계를 계속하기 전에 일정 시간을 보낸다.

브라우저와 WebDriver 스크립트 간의 경합 조건 문제를 해결하기 위해 대부분의 Selenium 클라이언트에 *대기* 패키지를 함께 배송한다. 대기를 사용할 때 일반적으로 [*명시적 대기*](#explicit-wait)라고 부르는 것을 사용한다.

## 명시적 대기 <a name="explicit-wait"></a>
Selenium 클라이언트는 명령적이고 절차적인 언어에 대해 명시적인 대기를 사용할 수 있다. 코드 실행을 중지하거나 사용자가 전달한 조건이 해결될 때까지 스레드를 중지할 수 있다. 대기 시간이 초과될 때까지 특정 빈도로 조건을 호출한다. 이것은 조건이 잘못된 값을 반환하는 할 때까지 계속 시도하고 대기한다는 것을 뜻한다.

명시적 대기를 사용하면 조건이 발생할 때까지 기다릴 수 있기 때문에 브라우저와 해당 DOM간의 상태와 WebDriver 스크립트를 동기화하는 데 적합하다.

이전의 버그를 수정하기 위해 스크립트에서 동적으로 추가된 요소가 DOM에 추가될 때까지 *findElement* 호출을 대기할 수 있다.

```Python
from selenium.webdriver.support.wait import WebDriverWait
def document_initialised(driver):
    return driver.execute_script("return initialised")

driver.navigate("file:///race_condition.html")
WebDriverWait(driver, timeout=10).until(document_initialised)
el = driver.find_element(By.TAG_NAME, "p")
assert el.text == "Hello from JavaScript!"
```

반환 값이 진실이 될 때까지 대기가 반복적으로 실행된다는 조건을 함수 참조로 전달한다. "trueful" 반환 값은 문자열, 숫자, 부울, 객체(*WebElement* 포함), 채워지지 않은 시퀀스 또는 리스트와 같이 해당 언어에서 부울 true로 평가되는 모든 값이다. 이는 *빈 리스트*는 거짓으로 평가된다는 것을 의미한다. 조건이 true이고 차단 대기가 중단되면 조건의 반환 값이 대기의 반환 값이 됩니다.

이 지식을 통해, 그리고 대기 유틸리티는 기본적으로 그러한 요소 오류를 무시하지 않기 때문에, 우리의 명령을 더 간결하게 리팩터링할 수 있다.

```Python
from selenium.webdriver.support.wait import WebDriverWait

driver.navigate("file:///race_condition.html")
el = WebDriverWait(driver, timeout=3).until(lambda d: d.find_element(By.TAG_NAME,"p"))
assert el.text == "Hello from JavaScript!"
```

이 예제에서는 익명 함수를 전달한다 (그러나 앞에서 설명한 것처럼 명시적으로 정의하여 재사용할 수도 있다). 우리의 상태에 전달되는 첫 번째이자 유일한 assert는 항상 우리의 드라이버 객체인 WebDriver에 대한 참조이다. 멀티 스레드 환경에서 외부의 드라이버에 대한 참조가 아니라 조건에 전달된 드라이버 참조에 대해 작동하도록 주의해야 한다.

요소를 찾을 수 없을 때 발생하는 요소 오류를 대기 중에 허용하지 않으므로 요소를 찾을 때까지 조건을 재시도하게 된다. 그런 다음 반환 값인 *WebElement*를 가져와 스크립트로 다시 전달한다.

조건이 실패하는 경우 (예: 조건에서 참인 반환 값에 도달하지 못하는 경우), 대기는 *timeout error*라는 오류/예외를 발생시킨다.

### 옵션
대기 조건은 필요에 맞게 커스터마이즈할 수 있다. 성공적인 조건에 도달하지 않을 경우 위약금이 낼 수 있기 때문에 기본 timeout의 끝까지 기다릴 필요가 없는 경우가 있다.

대기를 통해 인수를 전달하여 timeout를 재정의할 수 있다.

```Python
WebDriverWait(driver, timeout=3).until(some_condition)
```

### 기대 조건
DOM과 사용자의 명령을 동기화해야 하는 경우가 매우 흔하기 때문에 대부분의 클라이언트는 미리 정의된 *예상 조건* 집합도 제공한다. 이름에서 알 수 있듯이, 이러한 조건은 빈번한 대기 작업에 대해 미리 정의된 조건들이다.

다양한 언어 바인딩에서 사용할 수 있는 조건도 다양하지만 다음과 같은 몇 가지 목록이 전체가 아니다.

- 경고가 존재
- 요소가 존재
- 가시 요소
- 제목이 포함
- 제목은
- 진부한 요소
- 가시 텍스트

각 클라이언트 바인딩에 대한 API 설명서를 참조하여 예상 조건의 전체 목록을 찾을 수 있다.

- Java의 [org.openqa.selenium.support.ui.ExpectedConditions]() class
- Python’s [selenium.webdriver.support.expected_conditions]() class
- JavaScript’s [selenium-webdriver/lib/until]() module

## 암묵적 대기
*암묵적 대기*라고 불리는 [명시적 대기](#explicit-wait)와는 구별되는 두 번째 대기 유형이 있다. 암묵적으로 대기함으로써 WebDriver는 요소를 찾으려고 할 때 특정 기간 동안 DOM을 폴링한다. 이 기능은 웹 페이지의 특정 요소를 즉시 사용할 수 없고 로드하는 데 시간이 필요할 때 유용하다.

요소가 나타날 때까지 암묵적으로 대기하는 기능은 기본적으로 비활성화되어 있으므로 세션별로 수동으로 활성화해야 한다. [명시적 대기](#explicit-wait)와 암시적 대기를 혼합하면 의도하지 않은 결과, 즉 요소가 사용 가능하거나 조건이 참인 경우에도 최대 시간 동안 대기하게 된다.

> **Warning**: 암시적 대기와 명시적 대기를 함께 사용하지 마시오. 이렇게 하면 예기치 않은 대기 시간이 발생할 수 있다. 예를 들어 암시적 대기를 10초로 설정하고 명시적 대기를 15초로 설정하면 20초 후에 시간 초과가 발생할 수 있다.

암묵적인 대기는 WebDriver가 요소 또는 요소를 즉시 사용할 수 없는 경우 해당 요소를 찾으려고 할 때 특정 시간 동안 DOM을 폴링하도록 지시하는 것이다. 기본 설정은 0이다 (사용 안 함). 일단 설정되면 세션의 수명 동안 암묵적 대기가 설정된다.

```Python
driver = Firefox()
driver.implicitly_wait(10)
driver.get("http://somedomain/url_that_delays_loading")
my_dynamic_element = driver.find_element(By.ID, "myDynamicElement")
```

## FluentWait
FluentWait 인스턴스는 조건을 확인하는 빈도뿐만 아니라 조건을 기다리는 최대 시간을 정의한다.

사용자는 대기 중에 페이지에서 요소를 검색할 때 `NoSuchElementException`과 같이 특정 타입의 예외를 무시하도록 대기를 설정할 수 있다. 

```Python
driver = Firefox()
driver.get("http://somedomain/url_that_delays_loading")
wait = WebDriverWait(driver, timeout=10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException])
element = wait.until(EC.element_to_be_clickable((By.XPATH, "//div")))
```

# 브라우저 상호 작용

## 브라우저 정보 얻기

### 제목 얻기
브라우저에서 현재 페이지 제목을 읽을 수 있다.

```Python
driver.title
```

### 현재 URL 얻기
브라우저의 주소 표시창에서 다음을 사용하여 현재 URL을 읽을 수 있다.

```Python
driver.current_url
```

---

### [브라우저 내비게이션](interactions/navigation.md)

### [Javascript 알림, 프롬프트와 확인](interactions/alerts.md)

### [쿠키 작업](interactions/cookies.md)

### [IFrame과 frame 작업](interactions/frames.md)

### [원도우와 탭 작업](interactions/windows.md)

### [가상 인증자](interactions/virtual-authenticator.md)
웹 인증 모델을 나타낸다.

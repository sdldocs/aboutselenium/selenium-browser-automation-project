# 지원 기능
<span style="font-size:120%">지원 클래스는 선택적인 상위 수준 기능을 제공한다.</span>

셀레늄의 핵심 라이브러리는 낮은 수준과 비유연화(non-opinated)를 시도한다. 각 언어의 지원 클래스는 일부 동작을 단순화하는 데 사용될 수 있는 통상 상호 작용에 대한 유연한(opinated) 래퍼를 제공한다.

---

### [색상 작업](support-features/colors.md)

### [Select 리스트 요소 작업](support-features/select-lists.md)

### [ThreadGuard](support-features/threadguard.md)


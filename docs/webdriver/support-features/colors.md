# 색상 작업
때때로 개발자는 테스트의 일부로 어떤 것의 색상을 검증하기를 원할 수 있다. 문제는 웹의 색상 정의가 일정하지 않다. 색상의 HEX 표현을 색상의 RGB 표현과 비교하거나 색상의 RGBA 표현을 HSLA 표현과 비교하는 쉬운 방법이 있다면 좋지 않을까?

*Color* 클래스가 그 해결책이다.

먼저 클래스를 가져와야 한다.

```Python
from selenium.webdriver.support.color import Color
```

이제 색상 객체 생성을 시작할 수 있다. 모든 색상 객체는 색상을 문자열로 표현하여 생성해야 한다. 지원되는 색상 표현은 다음과 같다.

```Python
HEX_COLOUR = Color.from_string('#2F7ED8')
RGB_COLOUR = Color.from_string('rgb(255, 255, 255)')
RGB_COLOUR = Color.from_string('rgb(40%, 20%, 40%)')
RGBA_COLOUR = Color.from_string('rgba(255, 255, 255, 0.5)')
RGBA_COLOUR = Color.from_string('rgba(40%, 20%, 40%, 0.5)')
HSL_COLOUR = Color.from_string('hsl(100, 0%, 50%)')
HSLA_COLOUR = Color.from_string('hsla(100, 0%, 50%, 0.5)')
```

Color 클래스는 [http://www.w3.org/TR/css3-color/#https4](http://www.w3.org/TR/css3-color/#https4)에 지정된 모든 기본 색상 정의 또한 지원한다.

```Python
BLACK = Color.from_string('black')
CHOCOLATE = Color.from_string('chocolate')
HOTPINK = Color.from_string('hotpink')
```

요소에 색상이 설정되지 않은 경우 브라우저에서 "투명(transparent)" 색상 값을 반환할 수 있다. Color 클래스는 다음 기능도 지원한다.

```Python
TRANSPARENT = Color.from_string('transparent')
```

이제 모든 응답이 정확히 구문 분석되고 유효한 Color 객체로 변환된다는 것을 알고 있다면 요소를 정확하게 쿼리하여 색상/배경 색상을 가져올 수 있다.

```Python
login_button_colour = Color.from_string(driver.find_element(By.ID,'login').value_of_css_property('color'))

login_button_background_colour = Color.from_string(driver.find_element(By.ID,'login').value_of_css_property('background-color'))
```

다음 색상 객체를 직접 비교할 수 있다.

```Python
assert login_button_background_colour == HOTPINK
```

또는 색상을 다음 형식 중 하나로 변환하여 정적 유효성 검사를 수행할 수 있다.

```Python
assert login_button_background_colour.hex == '#ff69b4'
assert login_button_background_colour.rgba == 'rgba(255, 105, 180, 1)'
assert login_button_background_colour.rgb == 'rgb(255, 105, 180)'
```

색상은 더 이상 문제가 되지 않는다.

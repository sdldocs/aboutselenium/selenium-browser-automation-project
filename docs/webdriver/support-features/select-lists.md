# Select 리스트 요소 작업
<span style="font-size:120%">Select 리스트는 다른 요소에 비해 특별한 동작을 수행한다.</span>

이제 Select 객체는 `<select>` 요소와 상호 작용할 수 있는 일련의 명령을 제공한다.

Java 또는 .NET을 사용하는 경우, 코드에 지원 패키지가 적절하게 요구했는 지를 확인해야 한다. 아래 예제의 전체 코드를 GitHub에서 볼 수 있다.

이 클래스는 HTML 요소 `select`와 `option`에 대해서만 동작한다. `div` 또는 `li`를 사용하여 JavaScript 오버레이를 사용하여 드롭다운을 설계할 수 있으나, 이 클래스는 이들에게는 작동하지 않는다.

## 타입
Select 메소드는 작업 중인 `<select>` 요소의 타입에 따라 다르게 동작할 수 있다.

### 단일 select
옵션을 하나만 선택할 수 있는 표준 드롭다운 객체이다.

```Python
<select name="selectomatic">
    <option selected="selected" id="non_multi_option" value="one">One</option>
    <option value="two">Two</option>
    <option value="four">Four</option>
    <option value="still learning how to count, apparently">Still learning how to count, apparently</option>
</select>
```

### 다중 select
이 select 리스트을 사용하면 한 번에 두 개 이상의 옵션을 선택하거나 선택 취소할 수 있다. 이는 다중 속성을 가진 `<select>` 요소에만 적용된다.

```Python
<select name="multi" id="multi" multiple="multiple">
    <option selected="selected" value="eggs">Eggs</option>
    <option value="ham">Ham</option>
    <option selected="selected" value="sausages">Sausages</option>
    <option value="onion gravy">Onion gravy</option>
</select>
```

## 클래스 생성
먼저 `<select>` 요소를 찾은 다음 이 요소를 사용하여 `Select` 개체를 초기화한다. Selenium 4.5부터는 `<select>` 요소가 비활성화된 경우 `Select` 객체를 생성할 수 없다.

```Python
    select_element = driver.find_element(By.NAME, 'selectomatic')
    select = Select(select_element)
```

## 리스트 옵션
다음 두 가지 리스트를 얻을 수 있다.

### 모든 옵션
`<select>` 요소의 모든 옵션 리스트를 가져온다.

```Python
    option_list = select.options
```

### 선택된 옵션
`<select>` 요소에서 선택한 옵션 리스트를 가져온다. 표준 select 리스트의 경우 하나의 요소만 포함된 리스트가 되고 다중 select 리스트의 경우 0개 또는 여러 요소를 포함할 수 있다.

```Python
    selected_option_list = select.all_selected_options
```

## 선택 옵션
Select 클래스는 세 가지 방법으로 옵션을 선택할 수 있다. 다중 선택 타입의 select 목록 경우, 선택할 각 요소에 대해 이러한 방법을 반복할 수 있다.

### 텍스트
표시되는 텍스트를 기준으로 옵션 선택

```Python
    select.select_by_visible_text('Four')
```

### 값
값 애트리뷰트를 기준으로 옵션 선택

```Python
    select.select_by_value('two')
```

### 인덱스
리스트에서 위치에 따라 옵션 선택

```Python
    select.select_by_index(3)
```

### 비활성화된 옵션

<span style="color: green">**Selenium 4,5**</span>

비활성화된 애트리뷰트인 옵션을 선택할 수 없다.

```html
    <select name="single_disabled">
      <option id="sinlge_disabled_1" value="enabled">Enabled</option>
      <option id="sinlge_disabled_2" value="disabled" disabled="disabled">Disabled</option>
    </select>
```

```Python
    with pytest.raises(NotImplementedError):
        select.select_by_value('disabled')
```

## 선택 해제 옵션
다중 선택 타입의 선택 리스트만 옵션 선택을 취소할 수 있다. 선택할 각 요소에 대해 이러한 방법을 반복할 수 있다.

```Python
    select.deselect_by_value('eggs')
```

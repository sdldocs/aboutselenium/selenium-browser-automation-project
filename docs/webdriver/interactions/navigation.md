# 브라우저 내비게이션

## 내비게이트
브라우저를 시작한 후 가장 먼저 하고 싶은 일은 웹사이트를 여는 것이다. 이 작업은 한 줄로 수행할 수 있다.

```Python
driver.get("https://selenium.dev")
```

## 뒤로
브라우저의 뒤로 버튼을 누른다.

```Python
driver.back()
```

## 앞으로
브라우저의 앞으로 버튼을 누른다.

```Python
driver.forward()
```

## 새로 고침
현재 페이지를 새로 고친다.

```Python
driver.refresh()
```


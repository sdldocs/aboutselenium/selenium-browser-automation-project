# IFrame과 frame 작업
Frame은 이제 동일한 도메인에 있는 여러 문서에서 사이트 레이아웃을 만드는 데 사용되지 않는다. HTML5 이전 웹앱으로 작업하지 않는 한 작업을 수행할 수 없다. iframes는 완전히 다른 도메인의 문서를 삽입할 수 있도록 해주며, 여전히 일반적으로 사용된다.

frame 또는 iframe 작업이 필요한 경우, WebDriver를 사용하면 동일한 방법으로 작업할 수 있다. iframe 내의 버튼을 생각해 보자. 브라우저 개발 도구를 사용하여 요소를 검사하면 다음 사항을 볼 수 있다.

```html
<div id="modal">
  <iframe id="buttonframe" name="myframe"  src="https://seleniumhq.github.io">
   <button>Click here</button>
 </iframe>
</div>
```

iframe이 아니라면 다음과 같은 방법으로 버튼을 클릭할 수 있다.

```Python
    # This Wont work
driver.find_element(By.TAG_NAME, 'button').click()
```

그러나 iframe 외부에 버튼이 없으면 *no such element error*가 발생할 수 있다. 이는 Selenium은 최상위 문서의 요소만 인식하기 때문에 발생한다. 버튼과 상호 작용하려면 먼저 창을 전환하는 방법과 유사한 방식으로 frame을 전환해야 한다. WebDriver는 frame으로 전환하는 세 가지 방법을 제공한다.

## 웹요소 사용
WebElement를 사용하여 전환하는 것이 가장 유연한 옵션이다. 원하는 선택자(selector)를 사용하여 frame을 찾은 후 그 frame으로 전환할 수 니다.

```Python
    # Store iframe web element
iframe = driver.find_element(By.CSS_SELECTOR, "#modal > iframe")

    # switch to selected iframe
driver.switch_to.frame(iframe)

    # Now click on button
driver.find_element(By.TAG_NAME, 'button').click()
```

## 이름 또는 ID 사용
frame 또는 iframe에 ID 또는 이름 매트리뷰트가 있는 경우 이 애츠리뷰트을 대신 사용할 수 있다. 페이지에서 이름 또는 ID가 고유하지 않은 경우, 처음 발견된 frame 또는 iframe으로 전환된다.

```Python
    # Switch frame by id
driver.switch_to.frame('buttonframe')

    # Now, Click on the button
driver.find_element(By.TAG_NAME, 'button').click()
```

## 인덱스 사용
JavaScript에서 *window.frames*을 사용하여 질의할 수 있는 등 frame의 인덱스를 사용할 수도 있다.

```Python
    # switching to second iframe based on index
iframe = driver.find_elements(By.TAG_NAME,'iframe')[1]

    # switch to selected iframe
driver.switch_to.frame(iframe)
```

## frame에서 떠나기
iframe 또는 frameset에서 떠나면 아래와 같이 기본 콘텐츠로 다시 전환된다.

```Python
    # switch back to default content
driver.switch_to.default_content()
```
  
# JavaScript 알림, 프롬프트와 확인
WebDriver는 JavaScript가 제공하는 세 가지 타입의 네이티브 팝업 메시지를 사용하기 위한 API를 제공한다. 이러한 팝업은 브라우저에 의해 스타일이 지정되며 제한된 커스터마이제이션을 제공한다.

## 알림
가장 간단한 것은 알림이다. 커스텀 메시지를 표시하는 알림과 대부분의 브라우저에서 "OK"로 출력되는 "OK"로 알림을 해제하는 단일 단추입니다. 또한 대부분의 브라우저에서 닫기 버튼을 눌러 해제할 수 있지만, 이 경우 항상 OK 버튼과 동일한 작업을 수행한다. 알림 예제를 참조하시오.

WebDriver는 팝업에서 텍스트를 가져오고 이러한 알림을 승인하거나 해제할 수 있다.

```Python
# Click the link to activate the alert
driver.find_element(By.LINK_TEXT, "See an example alert").click()

# Wait for the alert to be displayed and store it in a variable
alert = wait.until(expected_conditions.alert_is_present())

# Store the alert text in a variable
text = alert.text

# Press the OK button
alert.accept()
```

## 확인
확인 박스는 사용자가 메시지를 취소하도록 선택할 수 있다는 점을 제외하면 알림과 유사하다. 확인 예을 참조하시오.

이 예에서는 알림을 저장하는 다른 방법도 보여 준다.

```Python
# Click the link to activate the alert
driver.find_element(By.LINK_TEXT, "See a sample confirm").click()

# Wait for the alert to be displayed
wait.until(expected_conditions.alert_is_present())

# Store the alert in a variable for reuse
alert = driver.switch_to.alert

# Store the alert text in a variable
text = alert.text

# Press the Cancel button
alert.dismiss()
```

## 프롬프트
프롬프트에는 텍스트 입력가 포함된다는 점을 제외하고 확인 박스와 유사하다. 폼 요소에서 작업하는 것과 마찬가지로 WebDriver의 키 보내기를 사용하여 응답을 작성할 수 있다. 이렇게 하면 자리 표시자 텍스트가 완전히 바뀐다. 취소 버튼을 누르면 텍스트를 전송하지 않는다. 프롬프트 예를 참조하시오.

```Python
# Click the link to activate the alert
driver.find_element(By.LINK_TEXT, "See a sample prompt").click()

# Wait for the alert to be displayed
wait.until(expected_conditions.alert_is_present())

# Store the alert in a variable for reuse
alert = Alert(driver)

# Type your message
alert.send_keys("Selenium")

# Press the OK button
alert.accept()
```

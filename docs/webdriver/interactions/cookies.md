# 쿠키 작업
쿠키는 웹 사이트에서 전송되어 컴퓨터에 저장되는 작은 데이터 조각이다. 쿠키는 주로 사용자를 인식하고 저장된 정보를 로드하는 데 사용된다.

WebDriver API는 다음과 같은 내장 메소드로 쿠키와 상호 작용할 수 있는 방법을 제공한다.

## 쿠키 추가
현재 브라우징 컨텍스트에 쿠키를 추가하는 데 사용된다. 쿠키 추가는 정의된 시리얼라이즈 가능한 JSON 객체 집합만 허용한다. [다음](https://www.w3.org/TR/webdriver1/#cookies)은 허용된 JSON 키 값 목록에 대한 링크이다

우선 쿠키가 유효할 수 있는 도메인에 있어야 한다. 사이트와 상호 작용하기 전에 쿠키를 미리 설정하려고 하는데 홈 페이지가 크거나 로드하는 데 시간이 오래 걸리는 경우 사이트에서 더 작은 페이지를 찾는 것이 대안이다 (일반적으로 404 페이지는 작다. 예: [http://example.com/some404page](http://example.com/some404page))

```Python
from selenium import webdriver

driver = webdriver.Chrome()

driver.get("http://www.example.com")

# Adds the cookie into current browser context
driver.add_cookie({"name": "key", "value": "value"})
```

## 명명된 쿠키 얻기
연결된 모든 쿠키 중 쿠키 이름과 일치하는 직렬화된 쿠키 데이터를 반환한다.

```Python
from selenium import webdriver

driver = webdriver.Chrome()

# Navigate to url
driver.get("http://www.example.com")

# Adds the cookie into current browser context
driver.add_cookie({"name": "foo", "value": "bar"})

# Get cookie details with named cookie 'foo'
print(driver.get_cookie("foo"))
```

## 모든 쿠키 얻기
현재 브라우징 컨텍스트에 대한 '직렬화가 된 쿠키 데이터'를 반환한다. 브라우저를 더 이상 사용할 수 없다면 오류를 반환한다.

```Python
from selenium import webdriver

driver = webdriver.Chrome()

# Navigate to url
driver.get("http://www.example.com")

driver.add_cookie({"name": "test1", "value": "cookie1"})
driver.add_cookie({"name": "test2", "value": "cookie2"})

# Get all available cookies
print(driver.get_cookies())
```

## 쿠키 삭제
제공된 쿠키 이름과 일치하는 쿠키 데이터를 삭제한다.

```Python
from selenium import webdriver
driver = webdriver.Chrome()

# Navigate to url
driver.get("http://www.example.com")
driver.add_cookie({"name": "test1", "value": "cookie1"})
driver.add_cookie({"name": "test2", "value": "cookie2"})

# Delete a cookie with name 'test1'
driver.delete_cookie("test1")
```

## 모든 쿠키 삭제
현재 브라우징 컨텍스트의 모든 쿠키를 삭제한다.

```Python
from selenium import webdriver
driver = webdriver.Chrome()

# Navigate to url
driver.get("http://www.example.com")
driver.add_cookie({"name": "test1", "value": "cookie1"})
driver.add_cookie({"name": "test2", "value": "cookie2"})

#  Deletes all cookies
driver.delete_all_cookies()
```

## 동일 사이트 쿠키 애트리뷰트
사용자는 서드 파티 사이트에서 시작한 요청과 함께 쿠키가 전송되는지 여부를 제어하도록 브라우저에 지시할 수 있다. CSRF(Cross-Site Request Forgery) 공격을 막기 위해 도입됐다.

Same-Site cookie 특성은 명령으로 두 개의 매개 변수를 허용한다.

## Strict:
sameSite 속성이 **Strict**로 설정된 경우 서드 파티 웹 사이트에서 시작한 요청과 함께 쿠키가 전송되지 않는다.

## Lax:
쿠키 sameSite 속성을 **Lax**로 설정하면, 쿠키가 서드 파티 웹 사이트에서 시작한 GET 요청과 함께 전송된다.

> **Note**: 현재 이 기능은 크롬(80+ 버전), 파이어폭스(79+ 버전)에 탑재되어 있으며 Selenium 4 이상 버전에서 작동한다.

```Python
from selenium import webdriver

driver = webdriver.Chrome()

driver.get("http://www.example.com")
# Adds the cookie into current browser context with sameSite 'Strict' (or) 'Lax'
driver.add_cookie({"name": "foo", "value": "value", 'sameSite': 'Strict'})
driver.add_cookie({"name": "foo1", "value": "value", 'sameSite': 'Lax'})
cookie1 = driver.get_cookie('foo')
cookie2 = driver.get_cookie('foo1')
print(cookie1)
print(cookie2)
```

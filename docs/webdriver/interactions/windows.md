# 원도우와 탭 작업

## 윈도우와 탭

### 윈도우 핸들 얻기
WebDriver는 윈도우과 탭을 구분하지 않는다. 사이트에서 새 탭이나 윈도우를 열면 Selenium에서 윈도우 핸들을 사용하여 작업할 수 있다. 각 윈도우에는 단일 세션에서 지속되는 고유 식별자를 갖고 있다. 아래와 같이 현재 윈도우의 윈도우 핸들을 가져올 수 있다.

```Python
driver.current_window_handle
```

### 윈도우 또는 탭으로 전환
[새 윈도우](https://seleniumhq.github.io/)에서 열리는 링크를 클릭하면 새 윈도우 또는 탭이 화면에서 활성화되지만 WebDriver는 운영 체제에서 활성화된 창을 알 수 없다. 새 원도우에서 작업하려면 새 윈도우로 전환해야 한다. 탭이나 윈도우가 두 개만 열려 있고 어느 윈도우에서 시작하는지 알고 있다면 제거 과정에서 WebDriver가 볼 수 있는 윈도우나 탭을 모두 반복하여 원래 윈도우가 아닌 윈도우로 전환할 수 있다.

그러나 Selenium 4는 새 탭(또는) 새 윈도우을 만들고 자동으로 윈도우로 전환하는 새로운 api [NewWindow](https://www.selenium.dev/documentation/webdriver/interactions/windows/#create-new-window-or-new-tab-and-switch)를 제공한다.

```Python
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

with webdriver.Firefox() as driver:
    # Open URL
    driver.get("https://seleniumhq.github.io")

    # Setup wait for later
    wait = WebDriverWait(driver, 10)

    # Store the ID of the original window
    original_window = driver.current_window_handle

    # Check we don't have other windows open already
    assert len(driver.window_handles) == 1

    # Click the link which opens in a new window
    driver.find_element(By.LINK_TEXT, "new window").click()

    # Wait for the new window or tab
    wait.until(EC.number_of_windows_to_be(2))

    # Loop through until we find a new window handle
    for window_handle in driver.window_handles:
        if window_handle != original_window:
            driver.switch_to.window(window_handle)
            break

    # Wait for the new tab to finish loading content
    wait.until(EC.title_is("SeleniumHQ Browser Automation"))
```

### 새 윈도우 또는 새 탭 생성 및 전환
새 윈도우 (또는) 탭을 만들고 화면에 새 윈도우 또는 탭을 활성화 한다. 새 윈도우 (또는) 탭으로 작업을 전환할 필요가 없다. 새 윈도우가 아닌 새 윈도우 (또는) 탭이 세 개 이상 연 경우 WebDriver가 볼 수 있는 윈도우 또는 탭을 모두 방문하여 원래 윈도우가 아닌 윈도우로 전환할 수 있다.

> **Note**: 이 기능은 Selenium 4 이상 버전에서 동작한다.

```Python
    # Opens a new tab and switches to new tab
driver.switch_to.new_window('tab')

    # Opens a new window and switches to new window
driver.switch_to.new_window('window')
```

### 원도우 또는 탭 닫기
윈도우 또는 탭 작업을 띁내고 브라우저에서 마지막으로 열려 있는 윈도우 또는 탭이 아닌 경우, 윈도우를 닫고 이전에 사용하던 윈도우로 다시 전환해야 한다. 이전 섹션의 코드 샘플을 따랐다고 가정하면 이전 윈도우 핸들은 변수에 저장되어 있다. 이를 종합하면 다음과 같은 이점을 얻을 수 있다.

```Python
    #Close the tab or window
driver.close()

    #Switch back to the old tab or window
driver.switch_to.window(original_window)
```

윈도우를 닫은 후 다른 윈도우 핸들로 전환하지 않으면 WebDriver가 지금 닫은 페이지에서 실행되고 **No Such Window Exception**가 발생된다. 실행을 계속하려면 정확한 윈도우 핸들로 다시 전환해야 한다.

### 세션 끝에 브라우저에서 나가기
브라우저 세션을 마치려면 닫기 대신 quit를 호출해야 한다.

```Python
driver.quit()
```

- quit는 
    - 해당 WebDriver 세션과 연결된 모든 윈도우와 탭을 닫는다
    - 브라우저 프로세스를 닫는다
    - 백그라운드 드라이버 프로세스를 닫는다
    - (Seleniium Grid를 사용하는 경우) 브라우저가 더 이상 사용되지 않으므로 다른 세션에서 사용할 수 있음을 Selenium Grid에 알린다

quit 호출이 실패하면 시스템에서 추가 백그라운드 프로세스와 포트가 실행 상태로 남아 있어 나중에 문제가 발생할 수 있다.

일부 테스트 프레임워크는 테스트 종료 시 연결하여 해체할 수 있는 메서드와 주석을 제공한다.

```Python
    # unittest teardown
    # https://docs.python.org/3/library/unittest.html?highlight=teardown#unittest.TestCase.tearDown
def tearDown(self):
    self.driver.quit()
```

테스트 컨텍스트에서 WebDriver를 실행하지 않는 경우 대부분의 언어에서 제공하는 `try/finally`를 사용하여 WebDriver 세션을 정리하는 것을 고려할 수 있다.

```Python
try:
    #WebDriver code here...
finally:
    driver.quit()
```

Python의 WebDriver는 이제 python 컨텍스트 매니저를 지원하며, `with` 키워드를 사용하면 실행이 끝날 때 자동으로 드라이버를 종료할 수 있다.

```Python
with webdriver.Firefox() as driver:
  # WebDriver code here...

# WebDriver will automatically quit after indentation
```

## 윈도우 관리
화면 해상도는 웹 애플리케이션 프로그램이 렌더링하는 방식에 영향을 줄 수 있으므로 WebDriver는 브라우저 윈도우를 이동하고 크기를 조절하는 메커니즘을 제공한다.

### 윈도우 크기 얻기
브라우저 창의 크기를 픽셀 단위로 가져온다.

```Python
    # Access each dimension individually
width = driver.get_window_size().get("width")
height = driver.get_window_size().get("height")

    # Or store the dimensions and query them later
size = driver.get_window_size()
width1 = size.get("width")
height1 = size.get("height")
```

### 윈도우 크기 설정
창을 복원하고 창 크기를 설정한다.

```Python
driver.set_window_size(1024, 768)
```

### 원도우 위치 얻기
브라우저 윈조우의 왼쪽 상단 좌표를 가져온다.

```Python
    # Access each dimension individually
x = driver.get_window_position().get('x')
y = driver.get_window_position().get('y')

    # Or store the dimensions and query them later
position = driver.get_window_position()
x1 = position.get('x')
y1 = position.get('y')
```

## 윈도우 위치 설정
창을 선택한 위치로 이동시킨다.

```Python
    # Move the window to the top left of the primary monitor
driver.set_window_position(0, 0)
```

### 윈도우 최대화
윈도우를 확대한다. 대부분의 운영 체제에서 운영 체제의 메뉴와 도구 모음을 차단하지 않고 창이 화면을 채운다.

```Python
driver.maximize_window()
```

### 윈도우 최소화
현재 브라우징 컨텍스트의 윈도우을 최소화한다. 이 명령의 정확한 동작은 개별 윈도우 관리자에 따라 다르다.

윈조우 최소화는 일반적으로 윈도우를 시스템 트레이에 숨긴다.

> **Note**: 이 기능은 Selenium 4 이상 버전에서 동작한다.

```Python
driver.minimize_window()
```

### 전체 화면 윈도우
대부분의 브라우저에서 F11을 누르는 것과 유사하게 전체 화면을 채운다.

```Python
driver.fullscreen_window()
```

### TakeScreenshot
현재 브라우징 컨텍스트의 스크린샷을 캡처하는 데 사용된다. WebDriver 엔드포인트 [스크린샷](https://www.w3.org/TR/webdriver/#dfn-take-screenshot)은 Base64 형식으로 인코딩된 스크린샷을 반환한다.

```Python
from selenium import webdriver

driver = webdriver.Chrome()

driver.get("http://www.example.com")

    # Returns and base64 encoded string into image
driver.save_screenshot('./image.png')

driver.quit()
```

### TakeElementScreenshot
현재 브라우징 컨텍스트에 대한 요소의 스크린샷을 캡처하는 데 사용된다. WebDriver 엔드포인트 스크린샷은 Base64 형식으로 인코딩된 스크린샷을 반환한다.

```Python
from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()

driver.get("http://www.example.com")

ele = driver.find_element(By.CSS_SELECTOR, 'h1')

    # Returns and base64 encoded string into image
ele.screenshot('./image.png')

driver.quit()
```

### Execute 스크립트
선택한 프레임 또는 윈도우의 현재 컨텍스트에서 JavaScript 코드를 실행한다.

```Python
    # Stores the header element
header = driver.find_element(By.CSS_SELECTOR, "h1")

    # Executing JavaScript to capture innerText of header element
driver.execute_script('return arguments[0].innerText', header)
```

### 프린트 페이지
브라우저 내에서 현재 페이지를 인쇄한다.

**Note**: 이를 위해서 Chrome 브라우저가 헤드리스 모드여야 한다.

```Python
    from selenium.webdriver.common.print_page_options import PrintOptions

    print_options = PrintOptions()
    print_options.page_ranges = ['1-2']

    driver.get("printPage.html")

    base64code = driver.print_page(print_options)
```

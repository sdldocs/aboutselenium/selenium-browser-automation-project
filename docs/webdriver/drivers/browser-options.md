# 브라우저 옵션
이는 모든 브라우저의 공통 기능이다.

Selenium 3에서는 원하는 Capabilities 클래스를 사용하여 세션에서 capabilities를 정의했다. Selenium 4부터는 브라우저 옵션 클래스를 사용해야 한다. 원격 드라이버 세션의 경우 사용할 브라우저를 결정하기 위해 브라우저 옵션 인스턴스가 필요하다.

이러한 옵션은 기능에 대한 w3c 사양에 기술되어 있다.

각 브라우저에는 사양에 정의된 옵션 외에도 정의할 수 있는 사용자 지정 옵션이 있다.

## browserName
지정된 세션의 `browserName`을 설정하는 데 이 기능을 사용한다. 지정한 브라우저가 원격 엔드에 설치되어 있지 않으면 세션을 생성하지 못한다.

## browserVersion
이 기능은 선택 사항이며, 원격 엔드에서 사용 가능한 브라우저 버전을 설정하는 데 사용된다. 예를 들어 Chrome 버전 80만 설치된 시스템에서 버전 75를 요청하면 세션 생성을 생성하지 못한다.

## pageLoadStrategy <a name="pageloadstrategy"></a>
세 가지 페이지 로드 전략을 사용할 수 있다.

페이지 로드 전략은 아래 표와 같이 [document.readyState](https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState)를 질의한다.

| 전략 | Ready 상태 | Notes |
|-----|-----------|-------|
| normal | complete | 기본적으로 사용되며, 모든 리소스가 다운로드될 때까지 기다린다 |
| eager | interactive | DOM 액세스가 준비되었지만 이미지와 같은 다른 리소스가 여전히 로드되고 있을 수 있다 |
| none | Any | 웹 드라이버를 전혀 차단하지 않음 |

문서의 document.readyState 속성은 현재 문서의 로드 상태를 나타낸다.

URL을 통해 새 페이지로 이동할 때 기본적으로 WebDriver는 문서 준비가 완료 상태가 될 때까지 탐색 메소드(예: driver.navigate().get())를 완료하지 않는다. 이는 특히 준비 상태가 완료된 후 특히 동적으로 콘텐츠를 로드하기 위해 자바스크립트를 사용하는 Single Page Application과 같은 사이트의 경우 *페이지 로드가 반드시 완료되었음을 의미하지는 않는다*. 또한 이 동작은 요소를 클릭하거나 양식을 제출한 결과로 발생하는 탐색에는 적용되지 않는다.

자동화에 중요하지 않은 자산(예: 이미지, CSS, js)을 다운로드하여 페이지를 로드하는 데 시간이 오래 걸리는 경우 세션 속도를 높이기 위해 기본 매개 변수인 `normal`에서 `eager` 또는 `none`으로 변경할 수 있다. 이 값은 전체 세션에 적용되므로 [대기 전략](../waits.md)의 취약성을 최소화하기에 충분한지 확인하여야 한다.

### normal(default)
WebDriver는 [load](https://developer.mozilla.org/en-US/docs/Web/API/Window/load_event) 이벤트 요청이 반환될 때까지 기다린다.

```Python
from selenium import webdriver 
from selenium.webdriver.chrome.options import Options 

options = Options() 
options.page_load_strategy = 'normal' 
driver = webdriver.Chrome(options=options) 
driver.get("http://www.google.com") 
driver.quit()
```

### eager
WebDriver는 [DOMContentLoaded](https://developer.mozilla.org/en-US/docs/Web/API/Document/DOMContentLoaded_event) 이벤트 요청이 반환될 때까지 기다린다.

```Python
from selenium import webdriver 
from selenium.webdriver.chrome.options import Options 

options = Options() 
options.page_load_strategy = 'eager' 
driver = webdriver.Chrome(options=options) 
driver.get("http://www.google.com") 
driver.quit()
```

### none
WebDriver는 초기 페이지가 다운로드될 때까지만 기다린다.

```Python
from selenium import webdriver 
from selenium.webdriver.chrome.options import Options 

options = Options() 
options.page_load_strategy = 'none' 
driver = webdriver.Chrome(options=options) 
driver.get("http://www.google.com") 
driver.quit()
```

## platformName
원격의 운영 체제를 식별하며 `platformName`을 가져오면 OS 명을 반환한다.

클라우드 기반 공급자에서 `platformName`을 설정하면 원격에 OS를 설정한다.

## acceptInsecureCerts
이 기능은 세션 중에 탐색하는 동안 유효하지 않은 `TLS Certificate`가 사용되는지 여부를 확인한다.

기능이 `false`로 설정된 경우 탐색에서 도메인 인증서 문제가 발생하면 [insecure certificate error](https://developer.mozilla.org/en-US/docs/Web/WebDriver/Errors/InsecureCertificate)가 반환된다. `true`로 설정하면 잘못된 인증서를 브라우저가 신뢰하게 된다.

기본적으로 이 기능은 모든 자체 서명 인증서를 신뢰한다. 일단 설정되면 `acceptInsecureCerts` 기능은 전체 세션에 영향을 미친다.

## timeouts
WebDriver 세션은 특정 세션 제한 시간 간격으로 적용되며, 이 기간 동안 사용자는 브라우저에서 스크립트를 실행하거나 정보를 검색하는 동작을 제어할 수 있다.

각 세션 시간 초과는 아래에 설명된 대로 서로 다른 시간 초과의 조합으로 구성된다.

### 스크립트 타임아웃
현재 브라우징 컨텍스트에서 실행 스크립트를 중단할 시기를 지정한다. WebDriver에서 새 세션을 만들 때 기본 제한 시간 **30,000**이 적용된다.

### 페이지 로드 타임아웃
현재 브라우징 컨텍스트에서 웹 페이지를 로드하는 시간 간격을 지정한다. 기본 제한 시간 **300,000**은 WebDriver에 의해 새 세션이 생성될 때 적용된다. 페이지 로드가 지정/기본 시간 범위를 제한하는 경우 *TimeoutException*으로 스크립트를 중지한다.

### 암묵적 대기 타임아웃
요소를 찾을 때 암묵적 요소 위치 전략을 기다리는 시간을 지정한다. WebDriver에서 새 세션을 만들 때 기본 제한 시간 **0**이 적용된다.

## unhandledPromptBehavior
현재 세션의 `user prompt handler` 상태를 지정한다. 기본은 **dismiss and notify state**이다.

### 사용자 프롬프트 처리기
이는 사용자 프롬프트가 원격에서 발생할 때 수행해야 하는 작업을 정의한다.`unhandledPromptBehavior` 기능이 이를 정의하며 다음과 같은 상태들이 존재한다.

- dismiss
- accept
- dismiss and notify
- accept and notify
- ignore

## setWindowRect
원격에서 [resizing and repositioning commands](https://w3c.github.io/webdriver/#resizing-and-positioning-windows)의 지원 가능 여부를 나타낸다.

## proxy
프록시 서버는 클라이언트와 서버 간의 요청을 위한 중개자 역할을 한다. 간단히 말해서, 트래픽은 요청한 주소로 이동하는 동안 프록시 서버를 통과하고 다시 돌아간다.

Selenium을 사용하는 자동화 스크립트를 위한 프록시 서버는 다음과 같은 경우에 유용할 수 있다.

- 네트워크 트래픽 캡처
- 웹 사이트에서 수행한 백엔드 호출 모조
- 복잡한 네트워크 토폴로지 또는 엄격한 기업 제한/정책 하에서 필요한 웹 사이트 접근

회사 환경에서 브라우저가 URL에 연결하지 못하는 경우 대부분 환경에서 접근할 프록시가 필요하기 때문이다.

Selenium WebDriver는 프록시 설정을 제공한다.

```Python
from selenium import webdriver

PROXY = "<HOST:PORT>"
webdriver.DesiredCapabilities.FIREFOX['proxy'] = {
    "httpProxy": PROXY,
    "ftpProxy": PROXY,
    "sslProxy": PROXY,
    "proxyType": "MANUAL",
}

with webdriver.Firefox() as driver:
    driver.get("https://selenium.dev")
```

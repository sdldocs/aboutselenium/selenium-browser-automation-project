# 원격 webDriver
로컬에서 사용하는 것과 동일하게 WebDriver를 원격으로 사용할 수 있다. 주요 차이점은 원격 webDriver가 별도의 컴퓨터에서 테스트를 수행할 수 있도록 구성되어야 한다는 것이다.

원격 webDriver는 클라이언트와 서버의 두 부분으로 구성된다. 클라이언트는 여러분의 webDriver 테스트이고 서버는 단순히 자바 서블릿이며, 현대의 JEE 앱 서버에 호스팅될 수 있다.

원격 WebDriver 클라이언트를 실행하려면 먼저 RemoteWebDriver에 연결해야 한다. 테스트를 실행하는 서버의 주소로 URL을 지정하여 이 작업을 수행한다. 사용자 구성을 설정하기 위해 원하는 기능을 설정한다. 아래는 Firefox에서 테스트를 수행하는 원격 웹 서버인 `www.example.com`을 가리키는 원격 WebDriver 객체를 인스턴스화한 예이다.

```Python
from selenium import webdriver

firefox_options = webdriver.FirefoxOptions()
driver = webdriver.Remote(
    command_executor='http://www.example.com',
    options=firefox_options
)
driver.get("http://www.google.com")
driver.quit() 
```
사용자가 테스트 구성을 추가로 지정하기 위해 다른 원하는 기능을 추가할 수 있다.

## 브라우저 옵션
예를 들어, Chrome 버전 67을 사용하여 Windows XP에서 Chrome을 실행한다고 가정한다.

```Python
from selenium import webdriver

chrome_options = webdriver.ChromeOptions()
chrome_options.set_capability("browserVersion", "67")
chrome_options.set_capability("platformName", "Windows XP")
driver = webdriver.Remote(
    command_executor='http://www.example.com',
    options=chrome_options
)
driver.get("http://www.google.com")
driver.quit()  
```

## 로컬 파일 탐지기
로컬 파일 탐지기를 사용하면 클라이언트 컴퓨터에서 원격 서버로 파일을 전송할 수 있다. 예를 들어 테스트에서 웹 응용 프로그램에 파일을 업로드해야 하는 경우 원격 WebDriver는 실행 중에 로컬 컴퓨터에서 원격 웹 서버로 파일을 자동으로 전송할 수 있다. 이렇게 하면 테스트를 실행하는 원격 컴퓨터에서 파일을 업로드할 수 있다. 기본적으로 실행되지 않으며 다음과 같은 방법으로 실행할 수 있다.

```Python
from selenium.webdriver.remote.file_detector import LocalFileDetector

driver.file_detector = LocalFileDetector()
```

위의 코드가 정의되면 다음과 같은 방법으로 테스트에 파일을 업로드할 수 있다.

```Python
driver.get("http://sso.dev.saucelabs.com/test/guinea-file-upload")

driver.find_element(By.ID, "myfile").send_keys("/Users/sso/the/local/path/to/darkbulb.jpg")
```

## 클라이언트 요청 추적
이 기능은 Java 클라이언트 바인딩(Beta 이후)에서만 사용할 수 있다. 원격 WebDriver 클라이언트는 요청을 Selenium Grid 서버로 전송하고, 서버는 요청을 WebDriver로 전달한다. HTTP 요청을 종단 간으로 추적하려면 서버와 클라이언트 사이드에서 추적하도록 설정해야 한다. 양쪽 끝에는 시각화 프레임워크를 가리키는 추적 내보내기 설정이 있어야 한다. 기본적으로 추적은 클라이언트와 서버 모두에 대해 사용 가능하다. 시각화 프레임워크 Jager UI와 Selenium Grid 4를 설정하려면 원하는 버전의 [Tracing Setup](https://github.com/SeleniumHQ/selenium/blob/selenium-4.0.0-beta-1/java/server/src/org/openqa/selenium/grid/commands/tracing.txt)을 참조하시오.

클라이언트 사이드 설정의 경우 아래 단계를 수행하시오.

### 요청된 종속성 추가
추적 익스포터를 위한 외부 라이브러리 설치는 Maven을 이용해 할 수 있다. 프로젝트 pom.xml에 `opentemetry-exporter-jaeger`와 `grpc-netty` 종속성을 추가한다.

```xml
  <dependency>
      <groupId>io.opentelemetry</groupId>
      <artifactId>opentelemetry-exporter-jaeger</artifactId>
      <version>1.0.0</version>
    </dependency>
    <dependency>
      <groupId>io.grpc</groupId>
      <artifactId>grpc-netty</artifactId>
      <version>1.35.0</version>
    </dependency>
```

클라이언트를 실행하는 동안 필요한 시스템 속성 추가/전달한다.

```Java
System.setProperty("otel.traces.exporter", "jaeger");
System.setProperty("otel.exporter.jaeger.endpoint", "http://localhost:14250");
System.setProperty("otel.resource.attributes", "service.name=selenium-java-client");

ImmutableCapabilities capabilities = new ImmutableCapabilities("browserName", "chrome");

WebDriver driver = new RemoteWebDriver(new URL("http://www.example.com"), capabilities);

driver.get("http://www.google.com");

driver.quit();
```

원하는 Selenium 버전에 필요한 외부 종속성 버전에 대한 자세한 내용은 Tracing Setup](https://github.com/SeleniumHQ/selenium/blob/selenium-4.0.0-beta-1/java/server/src/org/openqa/selenium/grid/commands/tracing.txt)을 참조하시오.

자세한 내용은 다음 사이트를 참조하시오:

- [OpenTelemetry](https://opentelemetry.io)
- [OpenTelemetry 구성](https://github.com/open-telemetry/opentelemetry-java/tree/main/sdk-extensions/autoconfigure)
- [jaeger](https://www.jaegertracing.io)
- [Selenium Grid Observability](https://www.selenium.dev/documentation/grid/advanced_features/observability/)

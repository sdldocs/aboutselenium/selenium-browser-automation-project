# Web elements
<span style="font-size:120%">DOM에서 요소 객체를 식별하고 작업한다.</span>

대부분 사람들의 Selenium 코드는 웹 요소에 대한 작업을 포함하고 있다.

---

### [파일 업로드](elements/file-upload.md)

### [Locator 전략](elements/locators.md)
DOM에서 하나 이상의 특정 요소를 식별하는 방법.

### [웹 요소 찾기](elements/finders.md)
제공된 locator 값을 기준으로 요소 찾기.

### [웹 요소와 상호 작용](elements/interactions.md)
form 컨트롤을 조작하기 위한 고급 명령 집합.

### [웹 요소에 대한 정보](elements/information.md)
요소에 대해 배울 수 있는 내용.

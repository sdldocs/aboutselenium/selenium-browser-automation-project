# Chrome DevTools

> **Note**: Selenium 4는 CDP(Chrome DevTools Protocol)에 직접 액세스할 수 있지만 대신 WebDriver BiDi API를 사용하는 것이 바람직 하다.

많은 브라우저들은 개발자들이 웹 앱을 디버깅하고 페이지의 성능을 실험하는 데 사용할 수 있는 브라우저와 통합된 도구인 "DevTools"를 제공한다. Google Chrome's DevTools는 Chrome DevTools Protocol(또는 줄여서 CDP)이라고 부르는 프로토콜을 사용한다. 이름에서 알 수 있듯이, 이는 테스트용으로 설계된 것도 아니고 안정적인 API를 갖도록 설계된 것도 아니기 때문에 기능은 브라우저 버전에 크게 의존한다.

WebDriver BiDi는 W3C WebDriver 프로토콜의 차세대 제품으로 모든 브라우저에서 구현되는 안정적인 API를 제공하는 것을 목표로 하고 있으나 아직 완성된 것은 아니다. 그때까지 Selenium은 CDP를 구현한 브라우저(예: Google Chrome, Microsoft Edge, Firefox)에 대한 액세스를 제공하여 흥미로운 방법으로 테스트를 향상시킬 수 있다. 이 기능을 사용하여 수행할 수 있는 몇 가지 예는 다음과 같다.

## 지리적 위치 에뮬레이션
일부 애플리케이션 프로그램은 위치에따라 다른 특징과 기능을 가지고 있다. Selenium을 사용하여 브라우저에서 지리적 위치를 에뮬레이트하기 어렵기 때문에 이러한 애플리케이션 프로그램을 자동화하는 것은 어렵다. 하지만 Devtools의 도움으로 우리는 쉽게 그것들을 모방할 수 있다. 아래 코드는 이를 보여준다.

```Python
from selenium import webdriver 
from selenium.webdriver.chrome.service import Service 
def geoLocationTest(): 
  driver = webdriver.Chrome() 
  Map_coordinates = dict({ "latitude": 41.8781, "longitude": -87.6298, "accuracy": 100 }) driver.execute_cdp_cmd("Emulation.setGeolocationOverride", Map_coordinates) 
  driver.get("")
```

## Remote WebDrive를 사용한 지리적 위치 에뮬레이션

```Python
from selenium import webdriver
#Replace the version to match the Chrome version
import selenium.webdriver.common.devtools.v93 as devtools

async def geoLocationTest():
    chrome_options = webdriver.ChromeOptions()
    driver = webdriver.Remote(
        command_executor='<grid-url>',
        options=chrome_options
    )

    async with driver.bidi_connection() as session:
        cdpSession = session.session
        await cdpSession.execute(devtools.emulation.set_geolocation_override(latitude=41.8781,longitude=-87.6298,accuracy=100))
    driver.get("https://my-location.org/")
    driver.quit()
```

## 장치 모드 재정의
Selenium과 CDP를 통합하여 사용하면 현재 장치 모드를 오버라이드하고 새로운 모드를 시뮬레이션할 수 있다. Width, height, mobile 및 deviceScaleFactor는 필수 매개 변수이다. 선택적 매개변수로는 scale, screenWidth, screenHeight, positionX, positionY, dontSetVisible, screenOrientation, viewport 및 displayFeature이 있다.

```Python
  from selenium import webdriver
  
  driver = webdriver.Chrome()
  // iPhone 11 Pro dimensions
  set_device_metrics_override = dict({
                  "width": 375,
                  "height": 812,
                  "deviceScaleFactor": 50,
                  "mobile": True
              })
  driver.execute_cdp_cmd('Emulation.setDeviceMetricsOverride', set_device_metrics_override)
  driver.get("<your site url>")
  ```

## 성능 지표 수집
애플리케이션을 방문하는 동안 다양한 성능 메트릭을 수집한다.

```Python
from selenium import webdriver

driver = webdriver.Chrome()

driver.get('https://www.duckduckgo.com')
driver.execute_cdp_cmd('Performance.enable', {})
t = driver.execute_cdp_cmd('Performance.getMetrics', {})
print(t)
driver.quit()
```

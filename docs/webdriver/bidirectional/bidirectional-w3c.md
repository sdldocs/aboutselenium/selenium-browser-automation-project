# 양방향 API (W3C 표준)
다음 API 목록은 [WebDriver BiDirectional Protocol](https://w3c.github.io/webdriver-bidi/)이 성장하고 브라우저 공급업체가 동일한 것을 구현함에 따라 증가할 것이다. 또한 Selenium은 내부적으로 W3C BiDi 프로토콜 API의 조합을 사용하는 실제 사용 사례를 지원하기 위해 노력할 것이다.

원하는 추가 기능이 있다면, [기능 요청](https://github.com/SeleniumHQ/selenium/issues/new?assignees=&labels=&template=feature.md)을 요청하십시오.

---

## 브라우징 컨텍스트
이 섹션에서는 컨텍스트 명령 검색과 관련된 API에 대해 설명한다.

### 새 윈도우 열기
새 창에 새 브라우징 컨텍스트를 생성한다.

```Python
    BrowsingContext browsingContext = new BrowsingContext(driver, WindowType.WINDOW);
```

### 새 탬 열기
새 탭에 새 브라우징 컨텍스트를 생성한다.

```Python
    BrowsingContext browsingContext = new BrowsingContext(driver, WindowType.TAB);
```

### 기존 윈도우 핸들 사용
명령을 실행할 기존 탭/창에 대한 브라우징 컨텍스트를 생성한다.

```Python
    String id = driver.getWindowHandle();
    BrowsingContext browsingContext = new BrowsingContext(driver, id);
```

### 참조 브라우징 컨텍스트로 윈도우 열기
참조 브라우징 컨텍스트는 [최상위 브라우징 컨텍스트](https://html.spec.whatwg.org/multipage/document-sequences.html#top-level-browsing-context)이다. API는 새 창을 만드는 데 사용되는 참조 브라우징 컨텍스트를 전달할 수 있다. 운영 체제에 따라 구현은 다르다.

```Python
    BrowsingContext
            browsingContext =
            new BrowsingContext(driver, WindowType.WINDOW, driver.getWindowHandle());
```

### 참조 브라우징 컨텍스트로 탬 열기
참조 브라우징 컨텍스트는 [최상위 브라우징 컨텍스트](https://html.spec.whatwg.org/multipage/document-sequences.html#top-level-browsing-context)이다. API는 새 탭을 만드는 데 사용되는 참조 브라우징 컨텍스트를 전달할 수 있다. 운영 체제에 따라 구현은 다르다.

```Python
    BrowsingContext
            browsingContext =
            new BrowsingContext(driver, WindowType.TAB, driver.getWindowHandle());
```

### URL로 이동

```Python
    BrowsingContext browsingContext = new BrowsingContext(driver, WindowType.TAB);

    NavigationResult info = browsingContext.navigate("https://www.selenium.dev/selenium/web/bidi/logEntryAdded.html");

    Assertions.assertNotNull(browsingContext.getId());
    Assertions.assertNull(info.getNavigationId());
    Assertions.assertTrue(info.getUrl().contains("/bidi/logEntryAdded.html"));
```

### 준비 상태가 있는 URL로 이동

```Python
    BrowsingContext browsingContext = new BrowsingContext(driver, WindowType.TAB);

    NavigationResult info = browsingContext.navigate("https://www.selenium.dev/selenium/web/bidi/logEntryAdded.html",
            ReadinessState.COMPLETE);

    Assertions.assertNotNull(browsingContext.getId());
    Assertions.assertNull(info.getNavigationId());
    Assertions.assertTrue(info.getUrl().contains("/bidi/logEntryAdded.html"));
```

### 브라우징 컨텍스트 트리 가져오기

상위 브라우징 컨텍스트를 포함하여 상위 브라우징 컨텍스트에서 하위 모든 브라우징 컨텍스트의 트리를 제공한다.

```Python
    String referenceContextId = driver.getWindowHandle();
    BrowsingContext parentWindow = new BrowsingContext(driver, referenceContextId);

    parentWindow.navigate("https://www.selenium.dev/selenium/web/iframes.html", ReadinessState.COMPLETE);

    List<BrowsingContextInfo> contextInfoList = parentWindow.getTree();

    Assertions.assertEquals(1, contextInfoList.size());
    BrowsingContextInfo info = contextInfoList.get(0);
    Assertions.assertEquals(1, info.getChildren().size());
    Assertions.assertEquals(referenceContextId, info.getId());
    Assertions.assertTrue(info.getChildren().get(0).getUrl().contains("formPage.html"));
```

### 깊이로 브라우징 컨텍스트 트리 가져오기
전달된 깊이 값까지 상위 브라우징 컨텍스트를 포함하여 상위 브라우징 컨텍스트에서 하위 모든 브라우징 컨텍스트 트리를 제공한다.

```Python
    String referenceContextId = driver.getWindowHandle();
    BrowsingContext parentWindow = new BrowsingContext(driver, referenceContextId);

    parentWindow.navigate("https://www.selenium.dev/selenium/web/iframes.html", ReadinessState.COMPLETE);

    List<BrowsingContextInfo> contextInfoList = parentWindow.getTree(0);

    Assertions.assertEquals(1, contextInfoList.size());
    BrowsingContextInfo info = contextInfoList.get(0);
    Assertions.assertNull(info.getChildren()); // since depth is 0
    Assertions.assertEquals(referenceContextId, info.getId());
```

### 모든 최상위 브라우징 컨텍스트 가져오기

```Python
        BrowsingContext window1 = new BrowsingContext(driver, driver.getWindowHandle());
        BrowsingContext window2 = new BrowsingContext(driver, WindowType.WINDOW);

        List<BrowsingContextInfo> contextInfoList = window1.getTopLevelContexts();

        Assertions.assertEquals(2, contextInfoList.size());
```

### 탭/윈도우 닫기

```Python
    BrowsingContext window1 = new BrowsingContext(driver, WindowType.WINDOW);
    BrowsingContext window2 = new BrowsingContext(driver, WindowType.WINDOW);

    window2.close();

    Assertions.assertThrows(BiDiException.class, window2::getTree);
```

## 로그
여기에서는 로깅과 관련된 API를 설명한다.

### `console.log` 이벤트 청취
`console.log` 이벤트를 수신하고 콜백을 등록하여 이벤트를 처리한다.

```Python
    try (LogInspector logInspector = new LogInspector(driver)) {
        CompletableFuture<ConsoleLogEntry> future = new CompletableFuture<>();
        logInspector.onConsoleLog(future::complete);

        driver.get("https://www.selenium.dev/selenium/web/bidi/logEntryAdded.html");
        driver.findElement(By.id("consoleLog")).click();

        ConsoleLogEntry logEntry = future.get(5, TimeUnit.SECONDS);

        Assertions.assertEquals("Hello, world!", logEntry.getText());
        Assertions.assertNull(logEntry.getRealm());
        Assertions.assertEquals(1, logEntry.getArgs().size());
        Assertions.assertEquals("console", logEntry.getType());
        Assertions.assertEquals("log", logEntry.getMethod());
        Assertions.assertNull(logEntry.getStackTrace());
    }
```

### JS 이벤트 청취
JS 예외 사항을 듣고 콜백을 등록하여 예외 세부 정보를 처리한다.

```Python
    try (LogInspector logInspector = new LogInspector(driver)) {
        CompletableFuture<JavascriptLogEntry> future = new CompletableFuture<>();
        logInspector.onJavaScriptException(future::complete);

        driver.get("https://www.selenium.dev/selenium/web/bidi/logEntryAdded.html");
        driver.findElement(By.id("jsException")).click();

        JavascriptLogEntry logEntry = future.get(5, TimeUnit.SECONDS);

        Assertions.assertEquals("Error: Not working", logEntry.getText());
        Assertions.assertEquals("javascript", logEntry.getType());
    }
```

### JS 로그 청취
모든 레벨의 JS 로그를 수신하고 콜백을 등록하여 로그를 처리한다.

```Python
    try (LogInspector logInspector = new LogInspector(driver)) {
        CompletableFuture<JavascriptLogEntry> future = new CompletableFuture<>();
        logInspector.onJavaScriptLog(future::complete);

        driver.get("https://www.selenium.dev/selenium/web/bidi/logEntryAdded.html");
        driver.findElement(By.id("jsException")).click();

        JavascriptLogEntry logEntry = future.get(5, TimeUnit.SECONDS);

        Assertions.assertEquals("Error: Not working", logEntry.getText());
        Assertions.assertEquals("javascript", logEntry.getType());
        Assertions.assertEquals(LogLevel.ERROR, logEntry.getLevel());
    }
```

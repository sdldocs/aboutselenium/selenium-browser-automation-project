# 양방향 API (CDP 구현)
다음 API 목록은 Selenium 프로젝트가 실제 사용 사례를 지원함에 따라 늘어날 것이다. 추가 기능을 보고 싶은 경우 찾아 보고, [기능 요청](https://github.com/SeleniumHQ/selenium/issues/new?assignees=&labels=&template=feature.md)에 올리세오.

## 기본 인증 등록
일부 애플리케이션 프로그램은 페이지를 보호하기 위해 브라우저 인증을 사용한다. Selenium을 사용하면 인증이 필요할 때마다 기본 인증 자격 증명의 입력을 자동화할 수 있다.

```
예 추가 예정
```

## 돌연변이 관찰
돌연변이 관찰은 DOM의 특정 요소에 DOM 돌연변이가 있을 때 WebDriver BiDi를 통해 이벤트를 캡처하는 기능이다.

```Python
from selenium import webdriver 
from selenium.webdriver.common.by import By 
from selenium.webdriver.support.wait import WebDriverWait 

driver = webdriver.Chrome() 
async with driver.log.mutation_events() as event: 
    pages.load("dynamic.html") 
    driver.find_element(By.ID, "reveal").click() 
WebDriverWait(driver, 5).until(EC.visibility_of(driver.find_element(By.ID, "revealed"))) 
assert event["attribute_name"] == "style" 
assert event["current_value"] == "" 
assert event["old_value"] == "display:none;"
```

## `console.log` 이벤트 청취
`console.log` 이벤트를 수신하고 콜백을 등록하여 이벤트를 처리한다.

```Python
import trio
from selenium import webdriver
from selenium.webdriver.common.log import Log

async def printConsoleLogs():
  chrome_options = webdriver.ChromeOptions()
  driver = webdriver.Chrome()
  driver.get("http://www.google.com")

  async with driver.bidi_connection() as session:
      log = Log(driver, session)
      from selenium.webdriver.common.bidi.console import Console
      async with log.add_listener(Console.ALL) as messages:
          driver.execute_script("console.log('I love cheese')")
      print(messages["message"])

  driver.quit()

trio.run(printConsoleLogs)
```

## JS 이벤트 청취
JS 예외 사항을 듣고 콜백을 등록하여 예외 세부 정보를 처리한다.

```Python
async def catchJSException():
  chrome_options = webdriver.ChromeOptions()
  driver = webdriver.Chrome()

  async with driver.bidi_connection() as session:
      driver.get("<your site url>")
      log = Log(driver, session)
      async with log.add_js_error_listener() as messages:
          # Operation on the website that throws an JS error
      print(messages)

  driver.quit()
```

## 네트워크 차단
브라우저에 들어오는 네트워크 이벤트를 캡처하고 이를 조작하려면 다음 예를 사용하시오.

```
특정 비동기과 동기 명령을 혼합할 수 없기 때문에 현재 Python에서 사용할 수 없음
```

# RemoteWebDriver 양방향 API (CDP 구현)
다음 예제는 [Remote WebDriver](../drivers/remote-webdriver.md)로 BiDi API를 활용하는 방법을 보이고 있다.

## 기본 인증 등록
일부 애플리케이션 프로그램은 페이지를 보호하기 위해 브라우저 인증을 사용한다. Selenium을 사용하면 인증이 필요할 때마다 기본 인증 자격 증명의 입력을 자동화할 수 있다.

```
Python 예 추가 필요
```

## 돌연변이 관찰
돌연변이 관찰은 DOM의 특정 요소에 DOM 돌연변이가 있을 때 WebDriver BiDi를 통해 이벤트를 캡처하는 기능이다.

```
Python 예 추가 필요
```

## `console.log` 이벤트 청취
`console.log` 이벤트를 수신하고 콜백을 등록하여 이벤트를 처리한다.

```
Python 예 추가 필요
```

## JS 예외를 발생시키는 작업

```
Python 예 추가 필요
```

## 네트워크 차단
브라우저에 들어오는 네트워크 이벤트를 캡처하고 이를 조작하려면 다음 예를 사용하시오.

```
Python 예 추가 필요
```

# 웹 요소에 대한 정보
<span style="font-size:120%">요소에 대해 배울 수 있는 내용.</span>

특정 요소에 대해 질의할 수 있는 많은 세부 정보가 있다.

## Is Displayed
이 메소드는 연결된 요소가 웹 페이지에 표시되는지 확인하는 데 사용된다. `Boolean` 값을 반환한다. 연결된 요소가 현재 브라우징 컨텍스트에 표시되면 True이고, 그렇지 않으면 false를 반환한다.

이 기능은 [언급되어](https://w3c.github.io/webdriver/#element-displayedness) 있지만 [모든 잠재적 조건을 다루는 것이 불가능](https://www.youtube.com/watch?v=LAD_XPGP_kk)하기 때문에 w3c 사양에서 정의하고 있지 않다. 이와 같이 Selenium은 드라이버가 직접 이 기능을 구현할 것이라고 기대할 수 없으며, 이제는 큰 자바스크립트 함수를 직접 실행하는 것에 의존한다. 이 함수는 값을 반환하기 위해 트리에서 요소의 특성과 관계에 대한 많은 근사치를 만든다.

```Python
# Navigate to the url
driver.get("https://www.selenium.dev/selenium/web/inputs.html")

# Get boolean value for is element display
is_email_visible = driver.find_element(By.NAME, "email_input").is_displayed()
```

```
    코드 예를 추가 예정
```

## Is Enabled
이 메소드는 웹 페이지에서 연결된 요소가 활성화되어 있는지 또는 비활성화되어 있는지 확인하는 데 사용된다. 부울 값을 반환합니다. 연결된 요소가 현재 브라우징 컨텍스트에서 활성화된 경우 **True**이고, 그렇지 않은 경우 **False**를 반환한다.

```Python
    # Navigate to url
driver.get("http://www.google.com")

    # Returns true if element is enabled else returns false
value = driver.find_element(By.NAME, 'btnK').is_enabled()
```

## Is Selected
이 메서드는 참조된 요소가 선택되었는지 여부를 결정한다. 이 메소드는 Check boxes, radio buttons, 입력 요소 및 옵션 요소에서 널리 사용된다.

부울 값을 반환한다. 현재 브라우징 컨텍스트에서 참조된 요소를 선택한 경우 **True**이고, 그렇지 않은 경우 **False**를 반환한다.

```Python
    # Navigate to url
driver.get("https://the-internet.herokuapp.com/checkboxes")

    # Returns true if element is checked else returns false
value = driver.find_element(By.CSS_SELECTOR, "input[type='checkbox']:first-of-type").is_selected()
```

## 태그 명
현재 브라우징 컨텍스트에 포커스가 있는 참조된 요소의 태그 이름을 가져오는 데 사용된다.

```Python
    # Navigate to url
driver.get("https://www.example.com")

    # Returns TagName of the element
attr = driver.find_element(By.CSS_SELECTOR, "h1").tag_name
```

## 크기와 위치
참조된 요소의 치수와 좌표를 가져오는 데 사용된다.

가져온 데이터 본문에는 다음과 같은 세부 정보가 있다.

- 요소의 왼쪽 상단 모서리의 X축 위치
- 요소의 왼쪽 상단 모서리의 y축 위치
- 요소의 높이
- 요소의 너비

```Python
    # Navigate to url
driver.get("https://www.example.com")

    # Returns height, width, x and y coordinates referenced element
res = driver.find_element(By.CSS_SELECTOR, "h1").rect
```

## CSS 값 얻기
현재 브라우징 컨텍스트에서 요소의 지정된 계산 스타일 속성 값을 검색한다.

```Python
    # Navigate to Url
driver.get('https://www.example.com')

    # Retrieves the computed style property 'color' of linktext
cssValue = driver.find_element(By.LINK_TEXT, "More information...").value_of_css_property('color')
```

## 텍스트 내용
지정한 요소의 렌더링된 텍스트를 검색한다.

```Python
    # Navigate to url
driver.get("https://www.example.com")

    # Retrieves the text of the element
text = driver.find_element(By.CSS_SELECTOR, "h1").text
```

## 애트리뷰트와 속성 가져오기
DOM 애트리뷰트와 연결된 런타임 값을 가져온다. 요소의 DOM 애트리뷰트 또는 속성과 관련된 데이터를 반환한다.

```Python
# Navigate to the url
driver.get("https://www.selenium.dev/selenium/web/inputs.html")

# Identify the email text box
email_txt = driver.find_element(By.NAME, "email_input")

# Fetch the value property associated with the textbox
value_info = email_txt.get_attribute("value")
```

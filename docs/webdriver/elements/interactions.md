# 웹 요소와 상호 작용
<span style="font-size:120%">form 컨트롤을 조작하기 위한 고급 명령 집합.</span>

요소에서 실행할 수 있는 기본 명령은 5개 뿐이다.

- [클릭](https://w3c.github.io/webdriver/#element-click) (모든 요소에 적용)
- [키 보내기](https://w3c.github.io/webdriver/#element-send-keys) (텍스트 필드와 내용 편집 가능 요소에만 적용)
- [지우기](https://w3c.github.io/webdriver/#element-send-keys) (텍스트 필드와 내용 편집 가능 요소에만 적용)
- 제출 (폼 요소에만 적용)
- 선택 ([리스트 요소 선택](../support-features/select-lists.md) 참조)

## 추가 검증
이러한 방법은 사용자의 경험을 면밀히 모방하도록 설계되어 있으므로 [Actions API](../actions-api.md)와 달리 지정된 액션을 시도하기 전에 두 가지 작업을 수행하려고 시도한다.

1. 요소가 뷰포트 외부에 있다고 판단되면 [요소를 뷰로 스크롤한다](https://w3c.github.io/webdriver/#dfn-scrolls-into-view), 특히 요소의 하단을 뷰포트의 하단과 정렬한다.
2. 작업을 수행하기 전에 요소가 [상호 작용할 수 있는지](https://w3c.github.io/webdriver/#interactability) 확인한다. 이는 스크롤에 실패했거나 요소가 표시되지 않았음을 의미할 수 있다. 요소가 페이지에 표시되는지 여부를 결정하는 것은 [웹 드라이버 사양에서 직접 정의하기](https://w3c.github.io/webdriver/#element-displayedness)가 너무 어려웠기 때문에 Selenium은 요소가 표시되지 않도록 하는 것을 검사하는 자바스크립트 원자와 함께 실행 명령을 전송한다. 요소가 뷰포트에 없거나, 표시되지 않거나, [키보드와 상호 작용](https://w3c.github.io/webdriver/#dfn-keyboard-interactable)할 수 없거나, [포인터와 상호 작용](https://w3c.github.io/webdriver/#dfn-pointer-interactable)할 수 없는 것으로 판단되면 [요소가 상호 작용할 수 없다](https://w3c.github.io/webdriver/#dfn-element-not-interactable)는 오류를 반환한다.

## 클릭
[요소 클릭 명령](https://w3c.github.io/webdriver/#dfn-element-click)은 [요소의 중앙](https://w3c.github.io/webdriver/#dfn-center-point)에서 실행된다. 어떤 이유로 원소의 중심이 [가려지면](https://w3c.github.io/webdriver/#dfn-obscuring) Selenium은 [element click intercepted](https://w3c.github.io/webdriver/#dfn-element-click-intercepted) 오류를 반환한다.

## 키 보내기
[요소 키 보내기 명령]()은 제공된 키를 [편집 가능한]() 요소에 입력한다. 일반적으로, 요소는 `text` 타입을 가진 폼의 입력 요소이거나 `content-editable` 속성을 가진 요소임을 의미한다. 편집할 수 없는 경우 [invalid element state](https://w3c.github.io/webdriver/#dfn-invalid-element-state) 오류가 반환됩니다.

[이](https://www.w3.org/TR/webdriver/#keyboard-actions)는 WebDriver가 지원하는 키 입력 목록이다.

```Python
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
driver = webdriver.Firefox()

    # Navigate to url
driver.get("http://www.google.com")

    # Enter "webdriver" text and perform "ENTER" keyboard action
driver.find_element(By.NAME, "q").send_keys("webdriver" + Keys.ENTER)
```

## 지우기
[요소 지우기 명령](https://w3c.github.io/webdriver/#dfn-element-clear)은 요소의 내용을 재설정한다. 이를 위해서 요소를 [편집](https://w3c.github.io/webdriver/#dfn-editable)하고 [재설정](https://w3c.github.io/webdriver/#dfn-resettable-elements)할 수 있어야 한다. 일반적으로, 요소는 `text` 타입을 가진 폼의 입력 요소이거나 `content-editable` 속성을 가진 요소임을 의미한다. 이러한 조건을 충족하지 않으면 [invalid element state](https://w3c.github.io/webdriver/#dfn-invalid-element-state) 오류가 반환된다.

```Python
from selenium import webdriver
from selenium.webdriver.common.by import By
driver = webdriver.Chrome()

    # Navigate to url
driver.get("http://www.google.com")
    # Store 'SearchInput' element
SearchInput = driver.find_element(By.NAME, "q")
SearchInput.send_keys("selenium")
    # Clears the entered text
SearchInput.clear()
```

## 제출
Selenium 4에서 이는 더 이상 별도의 엔드포인트와 스크립트를 실행하여 기능한 함수로 구현되지 않는다. 따라서 이 방법을 사용하지 말고 해당 양식 제출 버튼을 클릭하는 것을 추천한다.

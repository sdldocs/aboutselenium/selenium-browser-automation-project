# 파일 업로드
파일 업로드 대화상자는 입력 요소가 파일 타입일 때 Selenium을 사용하여 처리할 수 있다. 그 예는  [https://the-internet.herokuapp.com/upload](https://the-internet.herokuapp.com/upload) 웹 페이지에서 찾을 수 있다. 업로드해야 하는 파일이 필요하다. 프로그래밍 언어로 파일을 업로드하는 코드는 다음과 같다.

```Python
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
driver.implicitly_wait(10)
driver.get("https://the-internet.herokuapp.com/upload");
driver.find_element(By.ID,"file-upload").send_keys("selenium-snapshot.jpg")
driver.find_element(By.ID,"file-submit").submit()
if(driver.page_source.find("File Uploaded!")):
    print("file upload success")
else:
    print("file upload not successful")
driver.quit()
```

위의 예제 코드는 Selenium을 사용하여 파일을 업로드하는 방법을 이해하는 데 도움이 된다.

# 웹 요소 찾기
<span style="font-size:120%">제공된 locator 값을 기준으로 요소 찾기.</span>

Selenium을 사용하는 가장 근본적인 측면 중 하나는 작업할 수 있는 원소 참조를 얻는 것이다. Selenium은 원소를 고유하게 식별하기 위한 다양한 내장 [로케이터 전략](locators.md)을 제공한다. 고급 시나리오에서 로케이터를 사용하는 여러 방법이 있다. 다음 HTML 스니펫을 뒤에 계속되는 설명을 위해 사용한다.

```html
<ol id="vegetables">
 <li class="potatoes">…
 <li class="onions">…
 <li class="tomatoes"><span>Tomato is a Vegetable</span>…
</ol>
<ul id="fruits">
  <li class="bananas">…
  <li class="apples">…
  <li class="tomatoes"><span>Tomato is a Fruit</span>…
</ul>
```

## 첫 일치 요소
많은 로케이터가 페이지의 여러 요소와 일치할 수 있다. singular find_element 메서드는 주어진 컨텍스트 내에서 찾은 첫 번째 요소에 대한 참조를 반환한다.

### 전체 DOM 평가
find_element 메서드가 드라이버 인스턴스에서 호출되면 제공된 로케이터와 일치하는 DOM의 첫 번째 요소에 대한 참조를 반환한다. 이 값을 저장하여 향후 요소 작업에 사용할 수 있다. 위의 예제 HTML에서 "tomatoes"라는 클래스 이름을 가진 두 개의 요소가 있으므로 이 메서드는 "vegetable" 리스트 내 요소를 반환한다.

```Python
vegetable = driver.find_element(By.CLASS_NAME, "tomatoes")
```

### DOM의 부분 집합 평가
전체 DOM에서 고유한 로케이터를 찾는 대신, 검색 범위를 다른 위치 요소의 범위로 좁히는 것이 종종 유용하다. 위의 예에서는 "tomatoes"라는 클래스 이름을 가진 두 개의 요소가 있으며, 두 번째 요소에 대한 참조를 얻는 것은 조금 더 어렵다.

한 가지 해결 방법은 원하지 않는 요소의 상위 요소가 아니고 원하는 요소의 상위 요소인 고유 속성을 가진 요소를 찾은 다음 해당 객체에서 find_element를 호출하는 것이다.

```Python
fruits = driver.find_element(By.ID, "fruits")
fruit = fruits.find_element(By.CLASS_NAME,"tomatoes")
```

### 최적화된 로케이터
중첩 룩업은 브라우저에 두 개의 개별 명령을 실행해야 하므로 효과적인 위치 전략이 아닐 수 있다.

성능을 약간 향상시키기 위해 CSS 또는 XPath를 사용하여 단일 명령에서 이 요소를 찾을 수 있다. [권장 테스트 관행](../../test-practices/encouraged.md) 섹션에서 [로케이터 전략 제안](../../test-practices/encouraged/locators.md)을 참조하시오.

이 예에서는 CSS Selector를 사용한다.

```Python
fruit = driver.find_element(By.CSS_SELECTOR,"#fruits .tomatoes")
```

## 모든 일치 요소
첫 번째 요소뿐만 아니라 로케이터와 일치하는 모든 요소에 대한 참조를 가져와야 하는 몇몇 사용 사례가 있다. find_elements 메소드는 요소 참조들의 컬렉션을 반환한다. 일치하는 항목이 없으면 빈 목록을 반환한다. 이 경우 모든 과일과 채소 리스트 항목에 대한 참조가 컬렉션으로 반환된다.

```Python
plants = driver.find_elements(By.TAG_NAME, "li")
```

### 요소 얻기
종종 요소 집합을 얻지만 특정 요소로 작업하려 한다면 컬렉션 상에서 반복하여 원하는 요소를 식별해야 한다.

```Python
from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Firefox()

    # Navigate to Url
driver.get("https://www.example.com")

    # Get all the elements available with tag name 'p'
elements = driver.find_elements(By.TAG_NAME, 'p')

for e in elements:
    print(e.text)
```

## 요소로부터 요소 찾기
상위 요소의 컨텍스트 내에서 일치하는 하위 WebElements 목록을 찾는 데 사용한다. 이를 위해 상위 WebElement는 하위 요소를 액세스하기 위해 'findElements'로 연결한다.

```Python
from selenium import webdriver
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get("https://www.example.com")

    # Get element with tag name 'div'
element = driver.find_element(By.TAG_NAME, 'div')

    # Get all the elements available with tag name 'p'
elements = element.find_elements(By.TAG_NAME, 'p')
for e in elements:
    print(e.text)
```

## 활성 요소 얻기
현재 검색 컨텍스트에 초점을 맞춘 DOM 요소를 추적하거나 찾는 데 사용한다.

```Python
  from selenium import webdriver
  from selenium.webdriver.common.by import By

  driver = webdriver.Chrome()
  driver.get("https://www.google.com")
  driver.find_element(By.CSS_SELECTOR, '[name="q"]').send_keys("webElement")

    # Get attribute of current active element
  attr = driver.switch_to.active_element.get_attribute("title")
  print(attr)
```

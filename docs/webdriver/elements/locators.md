# Locator 전략
<span style="font-size:120%">DOM에서 하나 이상의 특정 요소를 식별하는 방법.</span>

로케이터는 페이지에서 요소를 식별하는 방법이다. 이는 [Finding element](finders.md) 메서드에 전달된 인수이다.

탐색 방법과 별도로 로케이터를 선언하는 시기와 이유를 포함하여 [로케이터](../../test-practices/encouraged/locators.md)의 팁에 대한 [권장 테스트 관행](../../test-practices/encouraged.md)을 확인하시오

## 기존 로케이터
Selenium은 WebDriver에서 다음과 같은 8가지 전통적인 위치 전략을 지원한다.

| **로케이터** | **설명** |
|------------|---------|
| 클래스 이름 |  클래스 이름에 검색 값이 포함된 요소를 찾는다 (복합 클래스 이름은 허용되지 않음) |
| css 선택자 | CSS 선택자와 일치하는 요소를 찾는다 |
| id | ID 애트리뷰트가 검색 값과 일치하는 요소를 찾는다 |
| 이음 | NAME 애트리뷰트가 검색 값과 일치하는 요소를 찾는다 |
| 링크 텍스트 | 표시되는 텍스트가 검색 값과 일치하는 앵커 요소를 찾는다 |
| 부분 링크 텍스트 | 표시되는 텍스트에 검색 값이 포함된 앵커 요소를 찾는다. 여러 요소가 일치하는 경우 첫 번째 요소만 선택된다. |
| 태그 이름 | 태그 이름이 검색 값과 일치하는 요소를 찾는다 |
| xpath | XPath 식과 일치하는 요소를 찾는다 |

```
    코드 예를 추가 예정
```

## 상대 로케이터
**Selenium 4**는 상대 위치추적기(이전에는 Friendly 위치추적기라고 불림)를 도입한다. 이러한 로케이터는 원하는 요소에 대한 로케이터를 구성하는 것이 쉽지 않지만, 쉽게 구성된 로케이터가 있는 요소와 관련하여 요소가 어디에 있는지 공간적으로 설명하기 쉬운 경우에 유용하다.

### 작동 방식
셀레늄은 자바스크립트 함수 [`getBoundingClientRect()`](https://developer.mozilla.org/en-US/docs/Web/API/Element/getBoundingClientRect)를 사용하여 페이지에서 요소의 크기와 위치를 결정하며, 이 정보를 사용하여 인접 요소를 찾을 수 있다.

상대 로케이터 방법은 이전에 위치한 요소 참조 또는 다른 로케이터의 원점에 대한 인수로 취할 수 있다. 이 예에서는 로케이터만 사용하지만 마지막 방법에서 로케이터를 요소 객체와 교환하면 동일하게 작동한다.

상대 로케이터를 이해하기 위해 아래의 예를 고려해 보자.

![relative locators](../../images/relative_locators.png)

### 가용 상대 로케이터

#### Above
이메일 텍스트 필드 요소가 어떤 이유에서 인지 쉽게 식별할 수 없지만 암호 텍스트 필드 요소가 있다면, 암호 요소 "위"에 있는 "입력" 요소라는 사실을 사용하여 암호 텍스트 필드 요소를 찾을 수 있다.

```Python
email_locator = locate_with(By.TAG_NAME, "input").above({By.ID: "password"})
```

#### Below
암호 텍스트 필드 요소가 어떤 이유에서 인지 쉽게 식별할 수 없지만 이메일 텍스트 필드 요소가 있다면, 이메일 요소 "아래"의 "입력" 요소라는 사실을 사용하여 이메일 텍스트 필드 요소를 찾을 수 있다.

```Python
password_locator = locate_with(By.TAG_NAME, "input").above({By.ID: "email"})
```

#### Left of
어떤 이유로 취소 버튼을 쉽게 식별할 수 없지만 제출 버튼 요소가 있다면, 그것이 제출 요소의 "왼쪽"에 있는 "버튼" 요소라는 사실을 사용하여 취소 버튼 요소를 찾을 수 있다.

```Python
cancel_locator = locate_with(By.TAG_NAME, "button").to_left_of({By.ID: "submit"})
```

#### Right of
어떤 이유로 제출 버튼을 쉽게 식별할 수 없지만 취소 버튼 요소가 있다면, 취소 버튼 요소의 "오른쪽"에 있는 "버튼" 요소라는 사실을 사용하여 제출 버튼 요소를 찾을 수 있다.

```Python
submit_locator = locate_with(By.TAG_NAME, "button").to_right_of({By.ID: "cancel"})
```

#### Near
상대적 위치가 명확하지 않거나 창 크기에 따라 달라지는 경우 근거리 방법을 사용하여 제공된 로케이터에서 최대 50px 떨어져 있는 요소를 식별할 수 있다. 이를 위한 한 가지 좋은 사용 사례는 쉽게 구축된 로케이터가 없는 폼 요소와 관련된 입력 레이블 요소를 함께 사용하는 것이다.

```Python
email_locator = locate_with(By.TAG_NAME, "input").near({By.ID: "lbl-email"})
```

#### 상대 로케이터 체이닝
필요한 경우 로케이터를 체인으로 연결할 수도 있다. 때로는 요소가 한 요소의 위/아래와 다른 요소의 오른쪽/왼쪽 모두에 있는 것으로 가장 쉽게 식별된다.

```Python
submit_locator = locate_with(By.TAG_NAME, "button").below({By.ID: "email"}).to_right_of({By.ID: "cancel"})
```

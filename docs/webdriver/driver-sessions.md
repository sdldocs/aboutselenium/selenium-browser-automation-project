# 드리아버 세션
세션 시작 및 중지는 브라우저를 열고 닫기 위한 것이다.

## 세션 생성
새 세션 생성은 W3C 명령의 [New Session](https://w3c.github.io/webdriver/#new-session)에 해당된다

새 Driver 클래스 객체를 초기화하여 세션이 자동으로 생성된다.

각 언어는 다음 클래스 중 하나(또는 동등한 클래스)의 인수로 세션을 생성할 수 있다.

- 원하는 세션 타입을 기술하는 [옵션](drivers/browser-options.md)으로 기본값은 로컬이 사용되지만 원격을 위하여는 필요하다
- 어떤 형태의 [CommandExecutor](drivers/command-executors.md) (언어마다 구현 내용이 다름)
- [Listeners](drivers/command-listeners.md))

### 로컬 드라이버
로컬 드라이버를 시작하기 위한 주요 고유 인수에는 로컬 시스템에서 필요한 드라이버 서비스를 시작하기 위한한 정보가 포함된다.

- [서비스](drivers/browser-service.md) 객체는 로컬 드라이버에만 적용되며 브라우저 드라이버에 대한 정보를 제공한다

### 원격 드리이버
원격 드라이버를 시작하기 위한 주요 고유 인수에는 코드를 실행할 위치에 대한 정보가 포함된다. [원격 드라이버 섹션](drivers/remote-webdriver.md)에서 세부 정보을 볼 수 있다.

## 세션 종료
세션을 종료는 W3C 명령의 [세션 삭제](https://w3c.github.io/webdriver/#delete-session)에 해당된다.

> **Important Note**: `quit` 메소드는 `close` method와 다르므로 세션을 종료하려면 항상 `quit`를 사용하는 것이 좋다

---

### [브라우저 옵션](drivers/browser-options.md)
이는 모든 브라우저의 공통 기능이다.

### [명령 실행기](drivers/command-executors.md)

### [명령 리스터](drivers/command-listeners.md)

### [브라우저 서비스](drivers/browser-service.md)

### [원격 webDriver](drivers/remote-webdriver.md)

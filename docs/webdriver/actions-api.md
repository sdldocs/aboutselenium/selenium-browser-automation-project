# Action API
<span style="font-size:120%">웹 브라우저에 가상화된 장치 입력 작업을 제공하기 위한 낮은 수준의 인터페이스이다.</span>

고급 [요소 상호 작용](elements/interactions.md)외에도 [Action API](https://w3c.github.io/webdriver/#dfn-actions)는 지정된 입력 장치가 수행할 수 있는 작업을 세부적으로 제어한다. Selenium은 키 입력용 키보드, 포인터 입력용  마우스, 펜 또는 터치 장치, 휠 입력용 스크롤 휠 장치 등 3종류의 입력 소스를 위한 인터페이스를 제공한다 (Selenium 4.2에서 도입됨). Selenium을 사용하면 특정 입력에 할당된 개별 작업 명령을 구성하고 이를 함께 연결하고 관련 수행 방법을 호출하여 이를 모두 한 번에 실행할 수 있다.

## Action 구축기
기존 JSON Wire Protocol에서 새로운 W3C WebDriver Protocol로 이동하면서 낮은 수준의 작업 블록이 특히 상세해졌다. 매우 강력하지만 각 입력 장치는 여러 가지 방법으로 사용할 수 있으며 둘 이상의 장치를 관리해야 하는 경우 사용자는 해당 장치 간의 적절한 동기화를 보장해야 한다.

다행히도 낮은 수준의 명령을 직접 사용하는 방법을 배울 필요는 없다. 왜냐하면 사용자에게 하고 싶은 거의 모든 것이 낮은 수준의 명령을 결합하는 편리한 방법을 제공하기 때문입니다. 이들은 모두 [키보드](actions_api/keyboard/), [마우스](actions_api/mouse/), [펜](actions_api/pen/) 및 [휠](actions_api/wheel/) 페이지로 문서화되어 있다.

## Pause
포인터 이동 및 휠 스크롤을 사용하면 작업 기간을 설정할 수 있지만 작업이 명확히 작동하려면 작업 동안 잠시 대기만 하면 되는 경우도 있다.

```Python
    clickable = driver.find_element(By.ID, "clickable")
    ActionChains(driver)\
        .move_to_element(clickable)\
        .pause(1)\
        .click_and_hold()\
        .pause(1)\
        .send_keys("abc")\
        .perform()
```

## 모든 Action 해제
주의해야 할 점은 드라이버가 세션 전체에 걸쳐 모든 입력 항목의 상태를 기억한다는 것이다. 작업 클래스의 새 인스턴스를 생성하더라도 누른 키와 포인터의 위치는 이전에 수행한 작업의 상태로 남아 있는다.

현재 누른 모든 키와 포인터 버튼을 해제하는 특별한 방법이 있다. 이 메서드는 perform 메서드로는 실행되지 않기 때문에 각 언어에서 다르게 구현된다.

```Python
    ActionBuilder(driver).clear_actions()
```

---

### [키보드 actions](actions-api/keyboard.md)
웹 페이지와 상호 작용하기 위한 키 입력 장치를 나타낸다.

### [마우스 actions](actions-api/mouse.md)
웹 페이지와 상호 작용하기 위한 포인터 장치를 나타낸다.

### [펜 actions](actions-api/pen.md)
웹 페이지와 상호 작용하기 위한 펜 스타일러스 종류의 포인터 입력 장치를 나타낸다.

### [스크롤 휠 actions](actions-api/wheel.md)
웹 페이지와 상호 작용하기 위한 스크롤 휠 입력 장치를 나타낸다.

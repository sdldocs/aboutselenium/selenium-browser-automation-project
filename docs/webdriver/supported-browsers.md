# 지원하는 브라우저
각 브라우저에는 사용자 지정 능력과 고유한 기능을 갖고 있다.

---

## [Chrome 특유의 기능성](browsers/chrome.md)
Google Chrome 브라우저의 능력과 기능이다.

## [Edge 특유의 기능성](browsers/edge.md)
Microsoft Edge 브라우저의 능력과 기능이다.

## [Firefox 특유의 기능성](browsers/firefox.md)
Mozilla Firefox 브라우저의 능력과 기능이다.

## [IE 특유의 기능성](browsers/ie.md)
Microsoft Internet Explorer 브라우저의 능력과 기능이다.

## [Safari 특유의 기능성](browsers/safari.md)
Apple Safari 브라우저의 능력과 기능이다.

# Chrome 특유의 기능성
<span style="font-size:120%">이는 Google Chrome 브라우저의 능력과 기능이다.</span>

기본적으로 Selenium 4는 Chrome v75 이상과 호환된다. Chrome 브라우저 버전과 chromedriver 버전은 주요 버전과 일치해야 한다.

## 옵션
모든 브라우저에 공통된 기능은 [브라우저 옵션](../drivers/browser-options.md) 페이지에 설명되어 있다.

Chrome 고유의 기능은 Google의 [Capabilities & Chrome Options](https://chromedriver.chromium.org/capabilities) 페이지에서 확인할 수 있다

기본적으로 정의된 옵션을 사용하여 Chrome 세션을 시작하는 방법은 다음과 같다.

```Python
    options = ChromeOptions()
    driver = webdriver.Chrome(options=options)
```

다음은 다양한 기능들에 대한 일반적인 몇몇 사용 사례이다

### 인자
`args` 매개 변수는 브라우저를 시작할 때 사용되는 [명령어 스위치](https://peter.sh/experiments/chromium-command-line-switches/) 목록에 대한 것이다. 일반적으로 사용되는 인수에는 `--start-messages`와 `--headless=new`가 포함된다.

옵션에 인수 추가

```Python
    chrome_options = ChromeOptions()
    chrome_options.add_argument("--headless=new")
```

### 지정된 곳에서 브라우저 실행
`binary` 매개 변수는 사용할 브라우저의 대체 위치 경로를 갖는다. 이 매개 변수를 사용하면 Chrome 드라이버를 사용하여 다양한 Chronium 기반 브라우저를 구동할 수 있다.

옵션에 브라우저 위치 추가

```
    코드 예를 추가 예정
```

### 확장 추가
확장 매개 변수는 crx 파일을 허용한다

옵션에 확장 추가

```
코드 예를 추가 예정
```

### 브라우저를 연 상태로 유지
`detach` 매개 변수를 참(true)로 설정하면 드라이버 프로세스가 종료된 후에도 브라우저가 계속 열려 있다.

옵션에 binary 추가

```Python
    chrome_options = ChromeOptions()
    chrome_options.add_experimental_option("detach", True)
```

### 인수 제외
Chrome은 다양한 인수를 추가할 수 있다. 이러한 인수를 추가하지 않으려면 `excludeSwitch`로 전달하시오. 일반적인 예는 팝업 차단기를 다시 켜는 것이다.

옵션에 대한 excluded 인수 설정

```Python
    chrome_options = ChromeOptions()
    chrome_options.add_experimental_option('excludeSwitches', ['enable-automation'])
```

## 캐스팅
공유 탭을 포함한 Chrome Cast 장치를 구동할 수 있다.

```
코드 예를 추가 예정
```

## 네트워크 조건
다양한 네트워크 조건을 시뮬레이션할 수 있다.

```
코드 예를 추가 예정
```

## 로그

```
코드 예를 추가 예정
```

## 허가

```
코드 예를 추가 예정
```

## DevTools
Chrome DevTools 사용에 대한 자세한 내용은 [Chrome DevTools](../bidirectional/chrome-devtools.md) 섹션을 참조하시오

# IE 특유의 기능성
<span style="font-size:120%">이는 Internet Explorer 브라우저의 능력과 기능이다.</span>

2022년 6월 현재 Selenium은 공식적으로 더 이상 독립형 Internet Explorer를 지원하지 않는다. Internet Explorer 드라이버는 "IE Compatibility Mode"에서 Microsoft Edge 실행을 계속 지원한다.

## 특별 고려사항
Selenium 프로젝트에서 IE 드라이버를 직접 유지 관리하는 유일한 드라이버이다. 32비트와 64비트 버전의 Internet Explorer용 binary 파일을 사용할 수 있지만 64비트 드라이버에는 몇 가지 [알려진 제한 사항](https://jimevansmusic.blogspot.co.uk/2014/09/screenshots-sendkeys-and-sixty-four.html)이 있다. 따라서 32비트 드라이버를 사용하는 것을 추천한다.

Internet Explorer 사용에 대한 자세한 내용은 [IE 드라이버 서버 페이지](https://www.selenium.dev/documentation/ie_driver_server/)에서 확인할 수 있습니다

## 옵션
기본 옵션을 사용하여 Internet Explorer 호환 모드에서 Microsoft Edge 브라우저를 시작하는 방법은 다음과 같다.

```Python
    options = InternetExplorerOptions()
    driver = webdriver.Ie(options=options)
```

Internet Explorer 드라이버 v4.5.0 기준

- Windows 11의 시스템처럼 IE가 설치되어 있지 않은 경우, 위의 두 매개 변수를 사용할 필요가 없다. IE 드라이버는 Edge를 사용하고 자동으로 Edge를 찾는다.
- IE와 Edge가 모두 시스템에 설치되어 있는 경우 Edge에 연결만 설정하면 IE 드라이버가 자동으로 시스템에서 Edge를 찾는다.

Internet Explorer 드라이버 v4.7.0 기준

- IE 모드의 Edge에 대해 더 이상 줌 수준 무시를 설정할 필요가 없다

다음은 다양한 기능을 가진 몇 가지 일반적인 사용 사례들이다

### fileUploadDialogTimeout
일부 환경에서는 파일 업로드 대화 상자를 열 때 Internet Explorer가 timeout될 수 있다. IEDriver의 기본 timeout은 1000ms이지만 fileUploadDialogTimeout 기능을 사용하여 timeout을 늘릴 수 있다.

```Python
from selenium import webdriver

options = webdriver.IeOptions()
options.file_upload_dialog_timeout = 2000
driver = webdriver.Ie(options=options)

driver.get("http://www.google.com")

driver.quit()
```

### ensureCleanSession
`true`로 설정하면 이 기능은 수동으로 또는 드라이버에 의해 시작된 인스턴스를 포함하여 Internet Explorer의 모든 실행 중인 인스턴스에 대한 캐시, 브라우저 기록 및 쿠키를 삭제한다. 기본적으로 `false`로 설정된다.

이 기능을 사용하면 드라이버가 IE 브라우저를 시작하기 전에 캐시가 지워질 때까지 기다리므로 브라우저를 시작이 느릴 수 있다.

이 기능은 부울 값을 매개 변수로 사용할 수 있다.

```Python
from selenium import webdriver

options = webdriver.IeOptions()
options.ensure_clean_session = True
driver = webdriver.Ie(options=options)

driver.get("http://www.google.com")

driver.quit()
```

### ignoreZoomSetting 

Internet Explorer 드라이버는 브라우저 확대/축소 수준을 100%로 예상한다. 그렇지 않으면 드라이버가 예외를 발생시킨다. *ignoreZoom*을 `true`로 설정하여 이 기본 동작을 사용하지 않도록 설정할 수 있다. 

이 기능은 부울 값을 매개 변수로 사용한다.

```Python
from selenium import webdriver

options = webdriver.IeOptions()
options.ignore_zoom_level = True
driver = webdriver.Ie(options=options)

driver.get("http://www.google.com")

driver.quit()
```

### ignoreProtectedModeSettings
새 IE 세션을 시작하는 동안 *Protected Mode* 검사를 건너뛸지 여부이다.

설정되어 있지 않고 모든 영역에서 *Protected Mode* 설정이 동일하지 않은 경우 드라이버는 예외를 발생시킨다.

기능을 `true`로 설정하면 테스트가 불안정해지거나 응답하지 않거나 브라우저가 중단될 수 있습니다. 그러나 이는 여전히 차선의 선택이며, 첫 번째 선택은 *항상* 각 영역의 보호 모드 설정을 실제로 수동으로 설정하는 것이다. 사용자가 이 속성을 사용하는 경우 "최선의 지원"만 제공된다.

이 기능은 부울 값을 매개 변수로 사용한다.

```Python
from selenium import webdriver

options = webdriver.IeOptions()
options.ignore_protected_mode_settings = True
driver = webdriver.Ie(options=options)

driver.get("http://www.google.com")

driver.quit()
```

### silent
`true`로 설정하면 이 기능은 IEDriver 서버의 진단을 출력하지 않는다.

이 기능은 부울 값을 매개 변수로 사용한다.

```Python
from selenium import webdriver

options = webdriver.IeOptions()
options.set_capability("silent", True)
driver = webdriver.Ie(options=options)

driver.get("http://www.google.com")

driver.quit()
```

### 명령어 옵션
Internet Explorer는 브라우저의 문제를 해결하고 구성할 수 있도록 몇 가지 명령어 옵션이 포함되어 있다.

지원되는 몇 가지 명령어 옵션에 대해 설명한다

- *private*: 개인 브라우징 모드에서 IE를 시작할 때 사용합니다. 이 기능은 IE 8 이상 버전에서 작동한다.
- *k*: Internet Explorer를 키오스크 모드로 시작합니다. 브라우저는 주소 표시줄, 탐색 단추 또는 상태 표시줄을 표시하지 않는 최대화된 창에서 열린다.

- *extoff*: add-on이 없는 모드에서 IE를 시작한다. 이 옵션은 특히 브라우저 add-on의 문제를 해결하는 데 사용된다. IE 7 이상 버전에서 작동한다.

> **Note**: 명령어 인수가 작동하려면 **forceCreateProcessApi**를 순서대로 활성화해야 한다.

```Python
from selenium import webdriver

options = webdriver.IeOptions()
options.add_argument('-private')
options.force_create_process_api = True
driver = webdriver.Ie(options=options)

driver.get("http://www.google.com")

driver.quit()
```

### forceCreatePropcessApi
CreateProcess API를 사용하여 Internet Explorer를 강제로 실행한다. 기본값은 `false`이다.

IE 8 이상의 경우 이 옵션을 사용하려면 "TabProcGrowth" 레지스트리 값을 0으로 설정해야 한다.

```Python
from selenium import webdriver

options = webdriver.IeOptions()
options.force_create_process_api = True
driver = webdriver.Ie(options=options)

driver.get("http://www.google.com")

driver.quit()
```

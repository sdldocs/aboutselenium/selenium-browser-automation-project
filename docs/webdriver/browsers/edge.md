# Edge 특유의 기능성
<span style="font-size:120%">이는 Microsoft Edge 브라우저의 능력과 기능이다.</span>

Microsoft Edge는 Chronium을 사용하여 구현되었으며, v79 초기 버전의 지원이 포함되어 있다. Chrome과 마찬가지로 Edge 드라이버의 주 버전 번호는 Edge 브라우저의 주 버전과 일치해야 한다.

[Chrome 페이지](chrome.md)에 있는 모든 기능과 옵션은 Edge에서도 작동한다.

## 옵션
기본적으로 정의된 옵션을 사용하여 Edge 세션을 시작하는 방법은 다음과 같다.

```Python
    options = EdgeOptions()
    driver = webdriver.Edge(options=options)
```

다음은 다양한 기능들에 대한 일반적인 몇몇 사용 사례이다

### 인자
`args` 매개 변수는 브라우저를 시작할 때 사용되는 [명령어 스위치](https://peter.sh/experiments/chromium-command-line-switches/) 목록에 대한 것이다. 일반적으로 사용되는 인수에는 `--start-messages`와 `--headless=new`가 포함된다.

옵션에 인수 추가

```Python
    options = EdgeOptions()
    options.add_argument("--headless=new")
```

## Internet Explorer 호환 모드
Microsoft Edge는 Internet Explorer Driver 클래스를 Microsoft Edge와 함께 사용하여 "Internet Explorer Compatibility Mode"에서 구동할 수 있다. 자세한 내용은 [Internet Explorer 페이지](ie.md)를 참조하시오.

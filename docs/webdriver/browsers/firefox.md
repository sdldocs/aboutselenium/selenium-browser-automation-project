# Firefox 특유의 기능성
<span style="font-size:120%">이는 Mozilla Firefox 브라우저의 능력과 기능이다.</span>

Selenium 4에는 Firefox 78 이상이 필요하다. 항상 최신 버전의 geckodriver를 사용하는 것을 추천한다.

## 옵션
모든 브라우저에 공통된 기능은 [브라우저 옵션](../drivers/browser-options.md) 페이지에 설명되어 있다.

Chrome 고유의 기능은 Mozilla의 [firefoxOptions](https://developer.mozilla.org/en-US/docs/Web/WebDriver/Capabilities/firefoxOptions) 페이지에서 확인할 수 있다

기본적으로 정의된 옵션을 사용하여 Firefox 세션을 시작하는 방법은 다음과 같다.

```Python
    options = FirefoxOptions()
    driver = webdriver.Firefox(options=options)
```

다음은 다양한 기능들에 대한 일반적인 몇몇 사용 사례이다

### 인자
`args` 매개 변수는 브라우저를 시작할 때 사용되는 [명령어 스위치](https://peter.sh/experiments/chromium-command-line-switches/) 목록에 대한 것이다. 일반적으로 사용되는 인수에는 `--headless`와 `"-profile"`, `"/path/to/profile"`이 포함된다.

옵션에 인수 추가

```Python
    options=Options() 
    options.add_argument("-profile") 
    options.add_argument("/path/to/profile")
```

### 지정된 곳에서 브라우저 실행
`binary` 매개 변수는 사용할 브라우저의 대체 위치 경로를 갖는다. 이 매개 변수를 사용하면 Chrome 드라이버를 사용하여 다양한 Chronium 기반 브라우저를 구동할 수 있다.

옵션에 브라우저 위치 추가

```
    코드 예를 추가 예정
```

### 프로필
Firefox 프로필을 사용하는 여러 방법이 있다

```Python
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

options=Options()
firefox_profile = FirefoxProfile()
firefox_profile.set_preference("javascript.enabled", False)
options.profile = firefox_profile
```

### Add-ons
Chrome과 달리 Firefox 확장은 기능의 일부로 추가되지 않고 드라이버를 시작한 후 생성된다.

#### 설치
[Mozilla Addon 페이지](https://addons.mozilla.org/en-US/firefox/)에서 얻을 수 있는 서명된 xpi 파일

```Python
    path = os.path.abspath("tests/extensions/webextensions-selenium-example.xpi")
    driver.install_addon(path)
```

#### 제거
add-on을 제거하려면 그 ID를 알아야 한다. add-on 설치 시 반환 값에서 id를 얻을 수 있다.

```Python
    path = os.path.abspath("tests/extensions/webextensions-selenium-example.xpi")
    id = driver.install_addon(path)
    driver.uninstall_addon(id)
```

#### 서명되지 않은 설치
완료되지 않았거나 퍼블리시되지 않은 확장을 사용하여 작업할 경우 서명되지 않을 수 있다. 따라서 "임시"로만 설치할 수 있다 이 작업은 zip 파일 또는 디렉토리를 전달하여 수행할 수 있다. 다음은 디렉토리 전달의 예입니다:

```Python
    path = os.path.abspath("tests/extensions/webextensions-selenium-example/")
    driver.install_addon(path, temporary=True)
```

### 페이지 전체 스크린샷

```
코드 예를 추가 예정
```

### 컨텍스트

```
코드 예를 추가 예정
```

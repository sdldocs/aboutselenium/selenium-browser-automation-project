# Safari 특유의 기능성
<span style="font-size:120%">이는 Apple Safari 브라우저의 능력과 기능이다.</span>

Chrome과 Firefox 드라이버와 달리 Safari 드라이버는 운영 체제와 함께 설치된다. Safari에서 자동화를 사용하려면 터미널에서 다음 명령을 실행한다.

```shell
safaridriver --enable
```

## 옵션
모든 브라우저에 공통된 기능은 [옵션](../drivers/browser-options.md) 페이지에 설명되어 있다.

Safari 고유의 기능은 Apple의 [WebDriver for Safari](https://developer.apple.com/documentation/webkit/about_webdriver_for_safari#2957227) 페이지에서 확인할 수 있다

기본적으로 정의된 옵션을 사용하여 Safari 세션을 시작하할 수 있다.

```Python
    options = SafariOptions()
    driver = webdriver.Safari(options=options)
```

### Mobile
iOS에서 Safari를 자동화하는 사람들은 [Appium 프로젝트](https://appium.io/)를 주목해야 한다.

## Safari 기술 예고
Apple은 브라우저의 개발 버전을 제공한다. 이 버전을 사용하려면 [Safari Technology Preview](https://developer.apple.com/safari/technology-preview/)를 방문하시오.

```
    코드 예를 추가 예정
```

# 마우스 actions
<span style="font-size:120%">웹 페이지와 상호 작용하기 위한 포인터 장치를 나타낸다.</span>

마우스를 사용하여 수행할 수 있는 작업은 버튼을 누르고, 누른 버튼을 놓고, 마우스를 이동하는 세 가지뿐이다. Selenium은 이러한 작용을 가장 일반적인 방법으로 결합하는 편리한 방법을 제공한다.

## 클릭 그리고 누르기 (Click and hold)
이 메소드는 마우스를 요소의 중심으로 이동하는 것과 마우스 왼쪽 버튼을 누르는 것을 결합한 것이다. 이는 특정 요소의 초점을 맞추는 데 유용하다.

```Python
    clickable = driver.find_element(By.ID, "clickable")
    ActionChains(driver)\
        .click_and_hold(clickable)\
        .perform()
```

## 클릭 그리고 놓기 (Click and release)
이 메소드는 요소의 중심으로 이동하는 것과 마우스 왼쪽 버튼을 눌렀다가 놓는 것을 결합한 것이다. 이를 "클릭"이라고도 한다.

```Python
    clickable = driver.find_element(By.ID, "click")
    ActionChains(driver)\
        .click(clickable)\
        .perform()
```

## 대체(alternate) 버튼 클릭
마우스에는 총 5개의 정의된 버튼이 있다.

- 0 — 왼쪽 버튼(기본값)
- 1 — 중간 버튼(현재 지원되지 않음)
- 2 — 오른쪽 버튼
- 3 — X1(뒤로) 버튼
- 4 — X2(앞으로) 버튼

### 콘텍스트 클릭
이 메소드는 요소의 중심으로 이동하는 것과 마우스 오른쪽 버튼(버튼 2)을 눌렀다가 놓는 것을 결합한 것이다. 이를 "우클릭"이라고도 한다.

```Python
    clickable = driver.find_element(By.ID, "clickable")
    ActionChains(driver)\
        .context_click(clickable)\
        .perform()
```

### 뒤로 클릭
이것에 대한 편리한 방법은 따로 없으며, 마우스 버튼 3을 눌렀다 놓는 것뿐이다.

**Selenium v4.2**
```Python
    action = ActionBuilder(driver)
    action.pointer_action.pointer_down(MouseButton.BACK)
    action.pointer_action.pointer_up(MouseButton.BACK)
    action.perform()
```

### 앞으로 클릭
이것에 대한 편리한 방법은 따로 없으며, 마우스 버튼 4을 눌렀다 놓는 것뿐이다.

**Selenium v4.2**
```Python
    action = ActionBuilder(driver)
    action.pointer_action.pointer_down(MouseButton.FORWARD)
    action.pointer_action.pointer_up(MouseButton.FORWARD)
    action.perform()
```

## 더블 클릭
이 메소드는 요소의 중심으로 이동하는 것과 마우스 왼쪽 버튼을 두 번 눌렀다가 놓는 것을 결합한 것이다.

```Python
    clickable = driver.find_element(By.ID, "clickable")
    ActionChains(driver)\
        .double_click(clickable)\
        .perform()
```

## 요소로 이동
이 메서드는 마우스를 요소의 뷰 내 중앙으로 이동한다. 이것을 "호버링"이라고도 한다. 이때 요소가 뷰포트에 있어야 한다. 그렇지 않으면 오류를 일으킨다.

```Python
    hoverable = driver.find_element(By.ID, "hover")
    ActionChains(driver)\
        .move_to_element(hoverable)\
        .perform()
```

## 오프셋으로 이동
이 메소드는 먼저 지정된 원점으로 마우스를 이동한 다음 제공된 오프셋의 픽셀 수만큼 이동한다. 마우스 위치가 뷰포트에 있어야 한다. 그렇지 않으면 오류가 발생한다.

### 요소로부터 오프셋
이 메소드는 마우스를 요소의 in-view 중앙점으로 이동한 다음 제공된 오프셋을 기준으로 이동한다.

```Python
    mouse_tracker = driver.find_element(By.ID, "mouse-tracker")
    ActionChains(driver)\
        .move_to_element_with_offset(mouse_tracker, 8, 0)\
        .perform()
```

### 뷰포트로부터 오프셋
이 메서드는 현재 뷰포트의 왼쪽 상단 끝에서 제공된 오프셋만큼 마우스를 이동한다.

```Python
    action = ActionBuilder(driver)
    action.pointer_action.move_to_location(8, 0)
    action.perform()
```

### 현 위치로부터 오프셋
이 메소드는 사용자가 제공한 오프셋에 의해 마우스를 현재 위치에서 이동한다. 마우스를 이전에 이동하지 않은 경우 위치는 뷰포트의 왼쪽 상단 끝이다. 페이지를 스크롤할 때 포인터 위치는 변경되지 않는다.

첫 번째 인수 X가 양수일 때 오른쪽으로 이동하고 두 번째 인수 Y는 양수일 때 아래로 이동한다. 따라서 `MoveByOffset(30, -10)`은 현재 마우스 위치에서 오른쪽으로 30 이동하고 위쪽으로 10 이동한다.

```Python
    ActionChains(driver)\
        .move_by_offset( 13, 15)\
        .perform()
```

## 요소에서 드래그앤 드롭 (drag and drop)
이 메소드는 먼저 소스 요소를 클릭하여 누르고 대상 요소의 위치로 이동한 다음 마우스를 놓는다.

```Python
    draggable = driver.find_element(By.ID, "draggable")
    droppable = driver.find_element(By.ID, "droppable")
    ActionChains(driver)\
        .drag_and_drop(draggable, droppable)\
        .perform()
```

## 오프셋으로 드래그앤 드롭
이 메소드는 먼저 소스 요소를 클릭한 다음 유지하고 지정된 오프셋으로 이동한 다음 마우스를 놓는다.

```Python
    draggable = driver.find_element(By.ID, "draggable")
    start = draggable.location
    finish = driver.find_element(By.ID, "droppable").location
    ActionChains(driver)\
        .drag_and_drop_by_offset(draggable, finish['x'] - start['x'], finish['y'] - start['y'])\
        .perform()
```

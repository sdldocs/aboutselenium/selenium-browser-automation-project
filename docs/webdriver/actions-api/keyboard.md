# 키보드 actions
<span style="font-size:120%">웹 페이지와 상호 작용하기 위한 키 입력 장치를 나타낸다.</span>

키보드에서 수행할 수 있는 작업은 키를 누른 상태와 누른 키를 놓는 두 가지뿐이다. 각 키보드 키에는 ASCII 문자를 지원하는 것 외에도 지정된 순서로 누르거나 놓은 상태를 나타낼 수 있다.

## 키

> [Python Keys class](https://github.com/SeleniumHQ/selenium/blob/selenium-4.2.0/py/selenium/webdriver/common/keys.py#L23)를 사용

## 키 누르기(down)

```Python
    ActionChains(driver)\
        .key_down(Keys.SHIFT)\
        .send_keys("abc")\
        .perform()
```

## 키 놓기(up)

```Python
    ActionChains(driver)\
        .key_down(Keys.SHIFT)\
        .send_keys("a")\
        .key_up(Keys.SHIFT)\
        .send_keys("b")\
        .perform()
```

## 키 보내기
이는 keyDown와 keyUp 명령을 하나의 작업으로 결합하는 Actions API의 편리한 방법이다. 이 명령을 실행하는 것은 요소 메서드를 사용하는 것과 약간 다르지만 주로 다른 작업 도중에 여러 문자를 입력해야 할 때 사용된다.

### 활성(active) 요소

```Python
    ActionChains(driver)\
        .send_keys("abc")\
        .perform()
```

### 지정(designated) 요소

```Python
    text_input = driver.find_element(By.ID, "textInput")
    ActionChains(driver)\
        .send_keys_to_element(text_input, "abc")\
        .perform()
```

## 복사와 붙여넣기 (Copy and Paste)
다음은 위의 모든 방법을 사용하여 복사/붙여넣기 작업을 수행하는 예이다. 이 작업에 사용할 키는 Mac OS인지 여부에 따라 다르다. 아래 코드는 `SeleniumSelenium!` 텍스트로 끝난다. 

```Python
    cmd_ctrl = Keys.COMMAND if sys.platform == 'darwin' else Keys.CONTROL

    ActionChains(driver)\
        .send_keys("Selenium!")\
        .send_keys(Keys.ARROW_LEFT)\
        .key_down(Keys.SHIFT)\
        .send_keys(Keys.ARROW_UP)\
        .key_up(Keys.SHIFT)\
        .key_down(cmd_ctrl)\
        .send_keys("xvv")\
        .key_up(cmd_ctrl)\
        .perform()
```

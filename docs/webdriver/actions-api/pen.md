# 펜 actions
<span style="font-size:120%">웹 페이지와 상호 작용하기 위한 펜 스타일러스 종류의 포인터 입력 장치를 나타낸다.</span>

**Chronium Only**

펜(Pen)은 마우스와 같은 동작을 하는 포인터 입력의 일종이지만 스타일러스 고유의 이벤트 속성을 가질 수 있다. 마우스에는 5개의 버튼이 있지만 펜에는 3개의 버튼 상태가 있다.

0 — 컨택(contact)를 누른다 (기본값, 왼쪽 클릭에 해당)
2 — 배럴(barrel) 버튼 (우클릭과 동일)
5 — 지우개(eraser) 버튼 (현재 드라이버에서 지원되지 않음)

## 펜 사용

**Selenium v4.2**

```Python
    pointer_area = driver.find_element(By.ID, "pointerArea")
    pen_input = PointerInput(POINTER_PEN, "default pen")
    action = ActionBuilder(driver, mouse=pen_input)
    action.pointer_action\
        .move_to(pointer_area)\
        .pointer_down()\
        .move_by(2, 2)\
        .pointer_up()
    action.perform()
```

## 포인터 이벤트 애트리뷰트 추가

**Selenium v4.2**

```Python
    pointer_area = driver.find_element(By.ID, "pointerArea")
    pen_input = PointerInput(POINTER_PEN, "default pen")
    action = ActionBuilder(driver, mouse=pen_input)
    action.pointer_action\
        .move_to(pointer_area)\
        .pointer_down()\
        .move_by(2, 2, tilt_x=-72, tilt_y=9, twist=86)\
        .pointer_up(0)
    action.perform()
```

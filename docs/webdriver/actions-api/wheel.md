# 스크롤 휠 actions
<span style="font-size:120%">웹 페이지와 상호 작용하기 위한 스크롤 휠 입력 장치를 나타낸다.</span>

**Selenium v4.2**

**Chronium Only**

한 페이지에서 5가지 스크롤하는 시나리오가 있다.

## 요소로 스크롤
가장 일반적인 시나리오이다. 기존의 클릭 및 키 보내기 방법과 달리 작업 클래스는 자동으로 대상 요소를 뷰로 스크롤하지 않으므로 요소가 뷰포트 내에 아직 없는 경우 이 방법을 사용해야 한다.

이 메서드는 웹 요소를 유일한 인수로 사용한다.

요소가 현재 뷰 화면 위에 있는지 또는 아래에 있는지 여부에 관계없이 뷰포트가 스크롤되어 요소의 하단이 화면 하단에 표시된다.

```Python
    iframe = driver.find_element(By.TAG_NAME, "iframe")
    ActionChains(driver)\
        .scroll_to_element(iframe)\
        .perform()
```

## 지정된 만큼 스크롤
이는 스크롤에 대한 두 번째로 일반적인 시나리오이다. 오른쪽 및 아래쪽 방향으로 스크롤할 양을 델타 x와 델타 y 값으로 전달한다. 음수 값은 각각 왼쪽과 위쪽을 나타낸다.

```Python
    footer = driver.find_element(By.TAG_NAME, "footer")
    delta_y = footer.rect['y']
    ActionChains(driver)\
        .scroll_by_amount(0, delta_y)\
        .perform()
```

## 지정된 만큼 요소로부터 스크롤
이 시나리오는 위의 두 가지 방법을 효과적으로 결합한 것이다.

"Scroll From" 메서드를 사용하여 이를 실행한다. 이 메서드에는 3개의 인수가 사용된다. 첫 번째는 원점을 나타내며, 두 번째는 델타 x와 델타 y 값이다.

요소가 뷰포트를 벗어나면 화면 하단으로 스크롤된 다음 제공된 델타 x 및 델타 y값으로 페이지가 스크롤된다.

```Python
    iframe = driver.find_element(By.TAG_NAME, "iframe")
    scroll_origin = ScrollOrigin.from_element(iframe)
    ActionChains(driver)\
        .scroll_from_origin(scroll_origin, 0, 200)\
        .perform()
```

## 오프셋으로 요소로부터 스크롤
이 시나리오는 화면의 일부만 스크롤해야 하고 뷰포트 외부에 있을 때 사용된다. 또는 뷰포트 내부에 있으며 화면에서 스크롤해야 하는 부분은 특정 요소에서 떨어진 알려진 오프셋이다.

이는 "Scroll From" 메소드를 다시 사용하며, 요소를 지정하는 것 외에도 스크롤의 원점을 나타내기 위해 오프셋을 지정한다. 오프셋은 제공된 요소의 중심에서 계산된다.

요소가 뷰포트 밖에 있는 경우 먼저 화면 하단으로 스크롤한 다음 요소 중심의 좌표에 오프셋을 추가하여 스크롤 원점을 결정하고 마지막으로 제공된 델타 x 및 델타 y값으로 페이지를 스크롤한다.

요소의 중심으로부터 간격 띄우기가 뷰포트 외부에 있는 경우 예외가 발생한다.

```Python
    footer = driver.find_element(By.TAG_NAME, "footer")
    scroll_origin = ScrollOrigin.from_element(footer, 0, -50)
    ActionChains(driver)\
        .scroll_from_origin(scroll_origin, 0, 200)\
        .perform()
```

## 지정된 만큼 원점(요소)의 오프셋으로부터 스크롤
이 시나리오는 화면의 일부만 스크롤해야 하고 이미 뷰포트 내부에 있는 경우에 사용된다.

이는 "Scroll From" 메소드를 다시 사용하지만 요소 대신 뷰포트가 지정된다. 현재 뷰포트의 왼쪽 상단 끝에서 오프셋을 지정한다. 원점이 결정되면 페이지는 제공된 델타 x 및 델타 y 값으로 스크롤된다.

뷰포트의 왼쪽 상단 끝에서 오프셋이 화면 밖이면 예외가 발생한다.

```Python
    ActionChains(driver)\
        .scroll_from_origin(scroll_origin, 0, 200)\
        .perform()
```

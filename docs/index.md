Selenium은 웹 브라우저의 자동화를 가능하게 하고 지원하는 다양한 도구와 라이브러리를 위한 포괄적인 프로젝트이다.

브라우저와 사용자 상호 작용을 에뮬레이트하는 확장 기능, 브라우저 할당 크기 조정을 위한 배포 서버 및 모든 주요 웹 브라우저에 대해 호환 코드를 작성할 수 있는 [W3C WebDriver 사양](https://www.w3.org/TR/webdriver/) 구현을 위한 인프라를 제공한다.

수천 시간의 자신의 시간을 투자한 자원봉사자들이 이 프로젝트를 가능하게 하였고, 소스 코드를 누구나 자유롭게 사용하고, 즐기고, 개선할 수 있도록 만들었다.

Selenium은 웹 플랫폼의 자동화에 대한 공개 토론을 진행하기 위해 브라우저 공급업체, 엔지니어와 애호가들이 모인다. 프로젝트는 커뮤니티를 가르치고 육성하기 위해 [연례 회의](https://seleniumconf.com/)를 개최한다.

Selenium의 핵심은 많은 브라우저에서 상호 교환적으로 실행할 수 있는 명령어 집합을 작성하기 위한 인터페이스인 WebDriver이다. 모든 것을 설치한 후 몇 줄의 코드만으로 브라우저 안에 들어 갈 수 있다. 보다 포괄적인 예를 첫 번째 Selenium 스크립트에서 보여 준다.

```python
from selenium import webdriver


driver = webdriver.Chrome()

driver.get("http://selenium.dev")

driver.quit()
```

[개요](overview.md)를 참조하여 다양한 프로젝트 구성 요소를 확인하고, 여러분에게 셀레늄이 적합한 도구인지 결정하시오.

[시작](webdriver.md#getting_started)하기를 계속 진행하여 Selenium을 설치하고 테스트 자동화 도구로 성공적으로 사용하는 방법을 익히고, 이와 같은 간단한 테스트를 확장하여 여러 브라우저, 여러 다양한 운영 체제의 대규모 분산 환경에서 실행하는 방법을 이해해야 한다.

- [Selenium Overview](overview.md) - Selenium이 여러분을 위한 것인지, 다양한 프로젝트 구성 요소의 개요를 참조하쎄오.
- [WebDriver](webdriver.md) - WebDriver는 기본적으로 브라우저를 구동하며, 브라우저에 대해 자세히 살펴본다.
- [Grid](grid.md) - 여러 컴퓨터에서 병렬로 테스트를 실행할 필요가 있나? 그럼, 그리드를 살펴보아야 한다.
- [Selenium IDE](ide.md) - Selenium IDE는 사용자의 동작을 기록하고 재생하는 브라우저 확장이다.
- [테스트 실습](test-practices.md) - Selenium 프로젝트에서 사용되는 테스트에 대한 몇 가지 지침 및 권장 사항에 대한 설명.
- [레거시](legacy.md) - Selenium의 레거시 구성 요소와 관련된 문서. 사용되지 않는 구성 요소를 사용하기 위함이 아니라 역사적인 이유로 순수하게 유지된다.

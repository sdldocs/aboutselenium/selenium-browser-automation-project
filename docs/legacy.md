# Legacy
<span style="font-size:120%">Selenium의 레거시 구성 요소와 관련된 문서. 사용되지 않는 구성 요소를 사용하기 위함이 아니라 역사적인 이유로 순수하게 유지된다.</span>

<span style="color:red">현재 최신 버전인 Selenium 4만을 대상으로 편역하므로 여기에서는 참조만을 제공한다.</span>

---

### [Selenium RC(Selenium 1)](https://www.selenium.dev/documentation/legacy/selenium_1/)
Selenium의 오리지널 버전

### [Selenium 2](https://www.selenium.dev/documentation/legacy/selenium_2/)
Selenium 2는 WebDriver 코드로 구현된 Selenium 1을 다시 작성한 것이다.

### [Selenium 3](https://www.selenium.dev/documentation/legacy/selenium_3/)
Selenium 3는 Selenium RC 코드 없이 WebDriver를 구현한 것이다. 이후 W3C WebDriver 사양을 구현하는 Selenium 4로 대체되었다.

### [Legacy Selenium IDE](https://www.selenium.dev/documentation/legacy/selenium_ide/)
Selenium IDE는 원래 녹음과 재생을 위한 파이어폭스의 확장이었다.

### [JSON Wire Protocol Specification](https://www.selenium.dev/documentation/legacy/json_wire_protocol/)
[W3C 사양](https://w3c.github.io/webdriver/)의 선구자였던 현재는 구식인 오픈 소스 프로토콜의 엔드포인트와 페이로드이다.

### [Legacy Selenium Desired Capabilities](https://www.selenium.dev/documentation/legacy/desired_capabilities/)
이러한 기능은 기존 JSON Wire Protocol과 함께 동작했었다.

### [Legacy developer dicumentation](https://www.selenium.dev/documentation/legacy/developers/)
Selenium 개발자에게 관심있는 정보
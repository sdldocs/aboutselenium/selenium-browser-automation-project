<span style="font-size:120%">네이티브로 브라우저를 구동하는 WebDriver에 대해 자세히 알아본다.</span>

WebDriver는 사용자가 로컬에서 또는 Selenium 서버를 사용하는 원격 컴퓨터에서 브라우저를 네이티브로 구동하므로 브라우저 자동화 측면에서 비약적인 발전을 의미한다.

Selenium WebDriver는 언어 바인딩과 개별 브라우저 제어 코드의 구현을 모두 가리킨다. 이를 일반적으로 *WebDriver**라고 한다.

Selenium WebDriver는 W3C 권장 사항을 따르고 있다.

- WebDriver는 단순하고 더 간결한 프로그래밍 인터페이스로 설계되었다.
- WebDriver는 컴팩트한 객체 지향 API이다.
- 브라우저를 효과적으로 구동한다.

---

### [시작하기](webdriver/getting-started.md)
Selenium을 처음 사용하면 몇 가지 리소스를 이용하여 바로 속도를 높일 수 있다.

### [드리아버 세션](webdriver/driver-sessions.md)

### [지원하는 브라우저](webdriver/supported-browsers.md)

### [대기](webdriver/waits.md)

### [웹 요소](webdriver/web-elements.md)
DOM에서 요소 객체를 식별하고 작업한다.

### [브라우저 상호 작용](webdriver/browser-interactions.md)

### [Actions API](webdriver/actions-api.md)
웹 브라우저에 가상화된 장치 입력 작업을 제공하기 위한 낮은 수준의 인터페이스이다.

### [양방향 기능](webdriver/bidirectional-functionality.md)

### [지원 기능](webdriver/support-features.md)
지원 클래스는 선택적인 상위 수준 기능을 제공한다.

### [문제 해결 지원](webdriver/troubleshooting-assistance.md)
WebDriver 문제를 해결하는 방법

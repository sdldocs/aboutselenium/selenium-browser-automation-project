# 피해야 하는 사항 (Discouraged behavior)
<span style="font-size:120%">Selenium에서 브라우저를 자동화할 때 피해야 할 사항.</span>

---

### [Captchas](discouraged/captchas.md)

### [파일 다운로드](discouraged/file-downloads)

### [HTTP 회신 코드](discouraged/file-downloads.md)

### [Gmail, email과 Facebook logins](discouraged/gmail-email-facebook-logins.md)

### [테스트 종속성](discouraged/test-dependency.md)

### [성능 테스팅](discouraged/performance-testing.md)

### [링크 스파이더링](discouraged/link-spidering.md)

### [2단계 인증](discouraged/two-factor-authentication.md)

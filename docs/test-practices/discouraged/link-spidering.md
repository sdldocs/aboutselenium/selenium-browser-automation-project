# 링크 스파이더링 (Link spidering)
WebDriver를 사용하여 링크를 스파이더하는 것을 권장하지 않는다. 할 수 없어서가 아니라, WebDriver가 이를 위한 가장 이상적인 도구가 아니라는 것이 분명하기 때문이다. WebDriver는 시작하는 데 시간이 필요하며 테스트 작성 방법에 따라 단지 페이지로 이동하여 DOM을 통과하는 데만 몇 초, 최대 1분 정도 걸릴 수 있다.

WebDriver를 사용하는 대신 `curl` 명령을 실행하거나 `BeautifulSoup`과 같은 라이브러리를 사용하여 시간을 크게 절약할 수 있다. 이러한 방법은 브라우저를 만들고 페이지로 이동하는 데 의존하지 않기 때문이다. 이 작업에서 WebDriver를 사용하지 않음으로써 많은 시간을 절약할 수 있다.

# HTTP 회신 코드
Selenium RC의 일부 브라우저 설정에서 Selenium은 브라우저와 자동화되는 사이트 간의 프록시 역할을 했다. 이는 Selenium을 통과하는 모든 브라우저 트래픽을 캡처하거나 조작할 수 있음을 의미한다. `captureNetworkTraffic()` 메서드는 HTTP 응답 코드를 포함하여 자동화되는 사이트와 브라우저 사이의 모든 네트워크 트래픽을 캡처하는 것을 목적으로 한다.

Selenium WebDriver는 브라우저 자동화에 대한 완전히 다른 접근 방식으로, 사용자처럼 행동하는 것을 선호한다. 이는 WebDriver를 사용하여 테스트를 작성하는 방식으로 나타난다. 자동화된 기능 테스트에서 상태 코드를 확인하는 것은 테스트 실패에 대한 특별히 중요한 세부 사항이 아니며, 그 이전의 단계가 더 중요하다.

브라우저는 항상 HTTP 상태 코드를 갖는다. 예를 들어 404 또는 500 오류 페이지를 생각해 보자. 이러한 오류 페이지 중 하나가 발생할 때 "fail fast"하는 간단한 방법은 페이지 로드 후 신뢰할 수 있는 지점(예: `<h1>` 태그)의 페이지 제목이나 내용을 확인하는 것이다. 페이지 객체 모델을 사용하는 경우, 클래스 생성자 또는 페이지 로드가 예상되는 유사한 근처에 이 검사를 포함할 수 있다. 때때로 HTTP 코드가 브라우저의 오류 페이지에 표시될 수 있으며 WebDriver를 사용하여 이를 읽고 디버깅 출력을 개선할 수 있다.

웹 페이지 자체를 확인하는 것은 웹 사이트에 대한 사용자 관점을 표현하고 주장하는 WebDriver의 이상적인 관행과 같다.

HTTP 상태 코드를 캡처하는 고급 솔루션은 프록시를 사용하여 셀레늄 RC의 동작을 복제하는 것이다. WebDriver API는 브라우저의 프록시를 설정할 수 있는 기능을 제공하며, 웹 서버에서 송수신되는 요청의 내용을 프로그래밍 방식으로 조작할 수 있는 다수의 프록시가 있다. 프록시를 사용하면 리디렉션 응답 코드에 대한 응답 방법을 결정할 수 있다. 또한 모든 브라우저가 WebDriver에서 응답 코드를 사용할 수 있는 것은 아니므로 프록시 사용을 선택하면 모든 브라우저에서 작동하는 솔루션을 가질 수 있다.

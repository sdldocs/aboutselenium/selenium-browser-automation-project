# Gmail, email과 Facebook logins
여러 가지 이유로, WebDriver를 사용하여 Gmail과 Facebook과 같은 사이트에 로그인하는 것은 권장하지 않는다. 계정을 종료할 위험이 있는 이러한 사이트에 대한 사용 조건을 반하는 것 외에는 속도가 느리고 신뢰할 수 없다.

이메일 제공자가 제공하는 API를 사용하거나 페이스북의 경우 테스트 계정, 친구 등을 만들기 위한 API를 공개하는 개발자 도구 서비스를 사용하는 것을 권장한다. 비록 API를 사용하는 것이 약간의 추가적인 노력으로 보일 수 있지만, 속도, 신뢰성, 그리고 안정성으로 노력에 대한 보상을 받을 수 있을 것이다. 웹 페이지 및 HTML 로케이터가 자주 변경되면 테스트 프레임워크를 업데이트해야 하는 반면 API를 변경할 가능성은 없다.

테스트의 어느 지점에서나 WebDriver를 사용하여 타사 사이트에 로그인하면 테스트가 길어지므로 테스트에 실패할 위험이 높아진다. 일반적인 경험에 비추어 볼 때 긴 시험은 더 취약하고 신뢰할 수 없다.

[W3C를 준수하는](https://w3c.github.io/webdriver/webdriver-spec.html) WebDriver 구현은 서비스 거부 공격을 완화할 수 있도록 `navigator` 객체를 `WebDriver` 속성으로 주석을 달기도 한다.

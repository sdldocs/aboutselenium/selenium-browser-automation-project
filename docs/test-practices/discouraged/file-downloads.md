# 파일 다운로드
Selenium의 제어 하에 있는 브라우저와의 링크를 클릭하면 다운로드를 시작할 수 있지만, API는 다운로드 진행 상황을 노출시키지 않으므로 다운로드된 파일을 테스트하기에 이상적이지 않다. 파일을 다운로드하는 것은 웹 플랫폼과의 사용자 상호 작용을 에뮬레이트하는 중요한 측면으로 간주되지 않기 때문이다. 대신 Selenium(및 필요한 쿠키)을 사용하여 링크를 찾고 [libcurl](https://curl.haxx.se/libcurl/)과 같은 HTTP 요청 라이브러리에 전달한다.

[HtmlUnit 드라이버](https://github.com/SeleniumHQ/htmlunit-driver)는 [AttachmentHandler](https://htmlunit.sourceforge.io/apidocs/com/gargoylesoftware/htmlunit/attachment/AttachmentHandler.html) 인터페이스를 구현하여 첨부 파일을 입력 스트림으로 액세스하여 이를 다운로드할 수 있다. 그런 다음 AttachmentHandler를 [HtmlUnit](https://htmlunit.sourceforge.io/) Web Client에 추가할 수 있다.

# 테스트 자동화 개요
먼저, 정말로 브라우저를 사용할 필요가 있는지 스스로에게 물어보는 것으로 시작하세요. 복잡한 웹 애플리케이션을 사용하는 경우 브라우저를 열고 실제로 테스트해야 할 가능성이 있다.

그러나 Selenium 테스트와 같은 기능적 최종 사용자 테스트를 실행하는 데에는 많은 비용이 든다. 또한 효율적으로 운영되기 위해서는 일반적으로 상당한 인프라가 필요하다. 단위 테스트와 같은 더 가벼운 테스트 방법을 사용하거나 더 낮은 수준의 테스트 방법을 사용하여 테스트할 수 있는지 항상 생각해 보는 것이 좋다.

웹 브라우저 테스트 사업을 하고 있고 Selenium 환경에서 테스트를 시작할 준비가 되면 일반적으로 다음 세 단계를 조합하여 수행한다.

- 데이터 설정
- 개별 작업 집합 수행
- 결과 평가

이러한 단계를 가능한 한 짧게 유지할 수 있다. 대부분 경우 한두 번의 작업으로 충분할 수도 있다. 브라우저 자동화는 "잘못된" 것으로 유명하지만, 실제로 사용자가 자주 너무 많이 요구하기 때문이다. 뒤에서 테스트에서 명백한 간헐적 문제, 특히 브라우저와 웹 드라이버 간의 [경쟁 조건을 극복](../webdriver/waits.md)하는 방법을 완화하는 데 사용할 수 있는 기술로 돌아갈 것이다.

대안이 전혀 없는 경우에만 테스트를 짧게 유지하고 웹 브라우저를 사용하면 최소한의 플레이크(flake)로 많은 테스트를 수행할 수 있다.

Selenium 테스트의 뚜렷한 장점은 사용자의 관점에서 백엔드에서 프런트엔드에 이르기까지 애플리케이션의 모든 구성 요소를 테스트할 수 있는 고유한 기능이다. 즉, 기능 테스트를 실행하는 데는 비용이 많이 들 수 있지만, 한 번에 많은 비즈니스 크리티컬한 부분을 포함할 수 있다.

## 테스트 요구사항
앞서 언급했듯이 Selenium 테스트는 실행하는 데 비용이 많이 들 수 있다. 테스트를 실행하는 브라우저에 따라 달라질 수 있지만, 역사적으로 브라우저의 동작은 매우 다양하여 종종 여러 브라우저와 교차 테스트하는 것이 목표였다.

Selenium을 사용하면 여러 운영 체제상의 여러 브라우저에 대해 동일한 명령을 실행할 수 있지만, 가능한 모든 브라우저, 서로 다른 버전, 그리고 이들이 실행하는 많은 운영 체제를 열거하는 것은 순식간에 중요하지 않은 작업이 될 것이다.

## 예로 시작하자
Larry는 사용자들이 그들의 unicorn을 주문할 수 있는 웹사이트를 만들었다.

일반적인 워크플로우(우리가 "happy path"라고 부르는 것)는 다음과 같다.

- 계정 만들기
- 유니콘 구성
- 쇼핑 카트에 추가
- 체크아웃 및 결제
- 유니콘에 대한 피드백을 제공

위 모든 작업을 수행하기 위해 하나의 웅장한 Selenium 스크립트를 작성하고자 할 수 있다. **유혹에 빠지지 마세요!** 그렇게 하면 a) 시간이 오래 걸리고, b) 페이지 렌더링 타이밍 문제와 관련된 몇 가지 일반적인 문제가 발생하며, c) 실패할 경우 무엇이 잘못되었는지 진단하는 간결하고 "눈에 보이는" 방법이 제공되지 않는다.

이 시나리오를 테스트할 때 바람직한 전략은 이 시나리오를 일련의 독립적이고 신속한 테스트로 분류하는 것이며, 각 테스트는 존재해야 할 "이유"가 하나씩 있어야 한다.

두 번째 단계인 unicorn 구성을 테스트해 보자. 다음 작업을 수행한다.

- 계정 만들기
- 유니콘 구성

이 단계 이후 나머지 부분을 생략한다. 이 단계를 완료한 후 나머지 워크플로우를 다른 소규모 개별 테스트 사례에서 테스트한다.

시작하려면 계정을 만들어야 한다. 여기서는 다음과 같은 몇 가지 옵션을 선택할 수 있다.

- 기존 계정을 사용?
- 새 계정을 생성하?
- 구성을 시작하기 전 이러한 사용자의 특별한 속성을 고려?

이 질문에 대한 답변 방법에 관계없이, 해결책은 테스트의 "데이터 설정" 부분에 포함시키는 것이다. Larry가 사용자 계정을 만들고 업데이트할 수 있는 API를 노출한 경우 이 질문에 답하기 위해 이 API를 사용해야 한다. 가능한 경우 사용자가 이미 로그인할 수 있는 자격 증명을 가진 후에만 브라우저를 시작할 수 있다.

각 워크플로우에 대한 각 테스트가 사용자 계정 생성으로 시작되는 경우 각 테스트 실행에 몇 초가 추가된다. API를 호출하고 데이터베이스와 대화하는 것은 브라우저를 열고, 올바른 페이지로 이동하고, 양식이 제출되도록 클릭하고 기다리는 등의 비용이 많이 드는 프로세스를 필요로 하지 않는 빠르고 자동으로 진행할 수 있는 작업이다.

이상적으로는 브라우저가 시작되기 전에 실행되는 한 줄의 코드에서 이 설정 단계를 해결할 수 있다.

```Python
# Create a user who has read-only permissions--they can configure a unicorn,
# but they do not have payment information set up, nor do they have
# administrative privileges. At the time the user is created, its email
# address and password are randomly generated--you don't even need to
# know them.
user = user_factory.create_common_user() #This method is defined elsewhere.

# Log in as this user.
# Logging in on this site takes you to your personal "My Account" page, so the
# AccountPage object is returned by the loginAs method, allowing you to then
# perform actions from the AccountPage.
account_page = login_as(user.get_email(), user.get_password())
```

상상할 수 있듯이 `UserFactory`를 확장하여 `createAdminUser()`와 `createUserWithPayment()`와 같은 메서드를 제공할 수 있다. 요점은, 이 두 줄의 코드가 이 테스트의 궁극적인 목적인 유니콘 구성에서 여러분을 집중할 수 있도록 하는 것이다.

[Page Object 모델](encouraged/page-object-models.md)의 복잡성은 이 장 나중에 설명하겠지만, 여기서 개념을 소개한다.

테스트는 사이트의 페이지 컨텍스트 내에서 사용자의 관점에서 수행되는 작업으로 구성되어야 한다. 이러한 페이지는 객체로 저장되며, 여기에 웹 페이지가 구성되는 방법과 작업이 수행되는 방법에 대한 특정 정보가 포함된다. 이러한 정보 중 테스터가 사용할 수 있는 것은 거의 없다.

어떤 유니콘을 원하세요? 핑크색을 원하실 수도 있지만 꼭 그런 것은 아니다. 보라색은 최근에 꽤 인기가 많다. 그녀는 선글라스가 필요한가? 별 문신? 이러한 선택은 어렵지만 테스터로서 가장 중요한 관심사이다. 주문 처리 센터에서 유니콘을 적절한 사람에게 정확히 보내도록 해야 한다. 이렇게 시작한다.

이 단락의 어느 곳에서도 단추, 필드, 드롭다운, 라디오 단추 또는 웹 양식에 대해 설명하지 않는다. **여러분의 시험도 마찬가지이다!** 사용자가 문제를 해결하려는 것처럼 코드를 작성하려고 해야 한다. 다음은 이를 수행하는 한 가지 방법이다 (앞의 예에서 계속).

```Python
# The Unicorn is a top-level Object--it has attributes, which are set here.
# This only stores the values; it does not fill out any web forms or interact
# with the browser in any way.
sparkles = Unicorn("Sparkles", UnicornColors.PURPLE, UnicornAccessories.SUNGLASSES, UnicornAdornments.STAR_TATTOOS)

# Since we're already "on" the account page, we have to use it to get to the
# actual place where you configure unicorns. Calling the "Add Unicorn" method
# takes us there.
add_unicorn_page = account_page.add_unicorn()

# Now that we're on the AddUnicornPage, we will pass the "sparkles" object to
# its createUnicorn() method. This method will take Sparkles' attributes,
# fill out the form, and click submit.
unicorn_confirmation_page = add_unicorn_page.create_unicorn(sparkles)
```

이제 유니콘을 구성했으므로 실제로 작동하는지 확인하는 3단계로 이동해야 한다.

```Python
# The exists() method from UnicornConfirmationPage will take the Sparkles
# object--a specification of the attributes you want to see, and compare
# them with the fields on the page.
assert unicorn_confirmation_page.exists(sparkles), "Sparkles should have been created, with all attributes intact"
```

테스터는 여전히 버튼, 로케이터, 브라우저 컨트롤이 없는 이 코드에서 유니콘에 대해 이야기하는 것 외에는 아무것도 하지 않았다. 이 애플리케이션 *모델링* 방법을 사용하면 Larry가 다음 주에 더 이상 Ruby-on-Rails를 좋아하지 않아 Fortran 프론트엔드를 사용하여 전체 사이트를 최신 Haskell 바인딩으로 다시 구현하기로 결정하더라도 이러한 테스트 수준 명령을 그대로 유지할 수 있다.

페이지 객체는 사이트 재설계를 준수하기 위해 약간의 유지관리가 필요하지만 이러한 테스트는 동일하게 유지된다. 이 기본 설계를 사용하여 가능한 최소한의 브라우저 핸들링 단계로 워크플로우를 계속 진행할 수 있다. 다음 워크플로우에는 쇼핑 카트에 유니콘을 추가하는 작업을 포한다. 카트가 정확한 상태를 유지하는지 확인하기 위해 이 테스트를 여러 번 반복해야 할 수도 있다. 시작하기 전에 카트에 유니콘이 하나 이상 있는가? 쇼핑 카트에 몇 개가 들어갈 수 있나? 동일한 이름 과/또는 기능을 사용하여 둘 이상을 생성하면 오류가 발생하는가? 기존 것만 유지할 것인가 아니면 다른 것을 추가할 것인가?

워크플로우를 이동할 때마다 계정을 만들고 사용자로 로그인하며 유니콘을 구성할 필요는 없다. 이상적으로는 API 또는 데이터베이스를 통해 계정을 생성하고 유니콘을 사전 구성할 수도 있다. 그런 다음 사용자로 로그인하고 Sparkles를 찾아 카트에 추가하기만 하면 된다.

## 자동화?
자동화는 항상 바람직한 것인가? 테스트 사례 자동화는 언제 결정해야 할까?

테스트 사례를 자동화하는 것이 항상 바람직하지는 않다. 수동 테스트가 더 적합할 수 있는 경우도 있다. 예를 들어, 애플리케이션의 사용자 인터페이스가 가까운 미래에 상당히 변경될 것이라면, 어쨌든 자동화는 다시 작성될 수 있다. 또한 때때로 테스트 자동화를 구축하기에 시간이 없을 수 있다. 단기적으로는 수동 시험이 더 효과적일 수 있다. 애플리케이션의 마감 시간이 매우 촉박하고 현재 사용 가능한 테스트 자동화가 없는 경우, 해당 기간 내에 테스트를 완료해야 하는 경우 수동 테스트가 최선의 솔루션이다.

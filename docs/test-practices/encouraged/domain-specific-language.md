# 특정 도에인 언어 (Doamin Specific Language)
특정 도메인 언어(DSL)는 사용자에게 문제를 해결하는 표현 수단을 제공하는 시스템이다. 이를 통해 사용자는 단순히 프로그래머가 말하는 것을 넘어 자신의 조건에 따라 시스템과 상호 작용할 수 있다.

일반적으로 사용자는 사이트 모습에 신경 쓰지 않는다. 그들은 장식, 애니메이션, 그래픽에 대해 관심을 두지 않는다. 그들은 시스템을 사용하여 신입사원들이 최소한의 노력으로 그 과정을 마치도록 하고 싶어한다. 그들은 알래스카 여행을 예약하고 싶어한다. 그들은 unicorn을 구성하고 할인된 가격에 구입하고 싶어한다. 테스터로서의 일은 이 사고방식을 "획득"하는 데 가능한 한 근접하는 것이다. 이러한 점을 염두에 두고 (릴리스 전 사용자의 유일한 프록시인) 테스트 스크립트가 사용자를 대신하여 사용자가 작업 중인 애플리케이션을 "모델링"하기 시작한다.

목표는 어디서나 사용할 수 있는 언어를 사용하는 것이다. "이 테이블에 데이터 로드" 또는 "세 번째 열 클릭"을 참조하기보다는 "새 계정 만들기" 또는 "표시된 결과를 이름으로 정렬"과 같은 언어를 사용할 수 있어야 한다

Selenium에서 DSL은 통상 개발자와 (사용자, 제품 소유자, 비즈니스 인텔리전스 전문가 등) 이해관계자 간의 보고서를 가능하게 하는 API를 단순하고 읽기 쉽게 작성된 메소드로 대표된다. 

## 이점

- **Readable**: 비즈니스 이해 관계자는 이를 이해할 수 있다
- **Writable**: 쓰기 쉽고 불필요한 중복을 방지한다.
- **Extensible**: 계약 및 기존 기능을 위반하지 않고 (합리적으로) 기능을 추가할 수 있다.
- **Maintainable**: 테스트 사례에서 구현 세부 정보를 제외함으로써 AUT* 변경에 대비할 수 있다.

## 참고 문헌

- 에릭 에반스이 저술한 도메인 기반 디자인에 대한 좋은 책, [http://www.amazon.com/exec/obidos/ASIN/0321125215/domainlanguag-20](http://www.amazon.com/exec/obidos/ASIN/0321125215/domainlanguag-20)
- 시작하기 위해서 온라인에서 다운로드할 수 있는 유용한 작은 책, [http://www.infoq.com/minibooks/domain-driven-design-quickly](http://www.infoq.com/minibooks/domain-driven-design-quickly)

## Java

<span style="color:red">이하 Java 코드는 Python 코드로 교체될 계획이다.</span>

다음은 Java에서 합리적인 DSL 방법의 예이다. 간결하도록 DSL이 `driver` 객체가 미리 정의되어 있고 메소드를 사용 가능하다고 가정한다.

```Java
/**
 * Takes a username and password, fills out the fields, and clicks "login".
 * @return An instance of the AccountPage
 */
public AccountPage loginAsUser(String username, String password) {
  WebElement loginField = driver.findElement(By.id("loginField"));
  loginField.clear();
  loginField.sendKeys(username);

  // Fill out the password field. The locator we're using is "By.id", and we should
  // have it defined elsewhere in the class.
  WebElement passwordField = driver.findElement(By.id("password"));
  passwordField.clear();
  passwordField.sendKeys(password);

  // Click the login button, which happens to have the id "submit".
  driver.findElement(By.id("submit")).click();

  // Create and return a new instance of the AccountPage (via the built-in Selenium
  // PageFactory).
  return PageFactory.newInstance(AccountPage.class);
}
```

이 방법은 테스트 코드에서 입력 필드, 버튼, 클릭 및 심지어 페이지의 개념을 완전히 추상화한다. 이 접근법을 사용하여 테스터는 이 방법을 호출하기만 하면 된다. 이를 통해 유지관리 이점을 얻을 수 있다. 로그인 필드가 변경된 경우 테스트가 아닌 이 방법만 변경하면 된다.

```Java
public void loginTest() {
    loginAsUser("cbrown", "cl0wn3");

    // Now that we're logged in, do some other stuff--since we used a DSL to support
    // our testers, it's as easy as choosing from available methods.
    do.something();
    do.somethingElse();
    Assert.assertTrue("Something should have been done!", something.wasDone());

    // Note that we still haven't referred to a button or web control anywhere in this
    // script...
}
```

반복된다: 우리의 주요 목표 중 하나는 **UI의 문제가 아니라 당면한 문제**를 해결할 수 있는 API를 작성하는 것이어야 한다. UI는 사용자의 부차적인 관심사이다. 테스터는 UI에 신경 쓰지 않고 단지 작업을 완료하고 싶을 뿐이다. 테스트 스크립트는 사용자가 하고 싶은 것과 알고 싶은 것의 세탁 목록처럼 읽을 수 있어야 한다. 테스트는 UI가 사용자에게 요구하는 방식과 관련이 없어야 한다.

**\*AUT**: 테스트 중인 애플리케이션 프로그램

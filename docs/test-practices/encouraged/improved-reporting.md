# 개선된 리포팅 (Improved reporting)
Selenium은 실행 중인 테스트 케이스의 상태를 보고하도록 설계되지 않았다. 단위 테스트 프레임워크의 내장된 보고 기능을 활용하는 것이 바람직한 것이다. 대부분의 단위 테스트 프레임워크에는 xUnit 또는 HTML 형식의 보고서를 생성할 수 있다. Jenkins, Travis, Bamb 등과 같은 CI(Continuous Integration) 서버로 결과를 가져오는 데 널리 xUnit 보고서가 사용된다. 다음은 여러 언어에 대한 보고서 출력에 대한 자세한 정보를 위한 링크이다.

- [NUnit 3 Console Runner](https://github.com/nunit/docs/wiki/Console-Runner)
- [NUnit 3 Console Command Line](https://github.com/nunit/docs/wiki/Console-Command-Line)
- [xUnit getting test results in TeamCity](https://xunit.net/docs/getting-test-results-in-teamcity)
- [xUnit getting test results in CruiseControl.NET](https://xunit.net/docs/getting-test-results-in-ccnet)
- [xUnit getting test results in Azure DevOps](https://xunit.net/docs/getting-test-results-in-azure-devops)

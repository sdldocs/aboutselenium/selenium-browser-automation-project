# Page obejct 모델
웹 앱의 UI에는 테스트가 상호 작용하는 영역이 있다. 페이지 객체는 테스트 코드 내에서 객체로만 모델링한다. 이는 중복되는 코드의 양을 줄이고 UI가 변경될 경우 수정 사항을 한 곳에서만 적용하면 된다는 것을 의미한다.

페이지 객체는 테스트 유지보수를 향상시키고 코드 중복을 줄이기 위해 테스트 자동화에서 널리 사용되는 디자인 패턴이다. 페이지 객체는 AUT의 페이지에 대한 인터페이스 역할을 하는 객체 지향 클래스이다. 이후 테스트는 페이지의 UI와 상호 작용해야 할 때마다 이 페이지 객체 클래스의 메서드를 사용한다. 페이지에 대한 UI가 변경되면 테스트 자체의 변경이 필요 없고 페이지 객체 내의 코드만 변경하면 된다는 이점이 있다. 이후에는 새 UI를 지원하기 위한 모든 변경 사항이 한 곳에 배치된다.

## 장점

- 테스트 코드와 페이지별 코드 로케이터(또는 UI 맵을 사용하는 경우 로케이터 사용)와 레이아웃 처럼 명확한 구분이 있다.
- 페이지에서 제공하는 서비스 또는 작업을 위하여 테스트 전체에 분산되어 있기 보다는 이들이 단일 저장소에 저장되어 있다.

두 경우 모두 UI 변경으로 인해 필요한 수정을 한 곳에서 수행할 수 있다. 이 '테스트 디자인 패턴'이 널리 사용되고 있기 때문에 이 기술에 대한 유용한 정보는 수많은 블로그에서 찾을 수 있다. 더 많은 것을 알고 싶은 독자들은 이 주제에 대한 블로그를 인터넷에서 검색하도록 권장한다. 많은 사람들이 이 디자인 패턴에 대해 글을 썼으며 이 사용 설명서의 범위를 벗어나는 유용한 팁을 제공할 수 있다. 시작하기 위해 간단한 예제를 사용하여 페이지 객체를 설명하고자 한다.

## 예
먼저 페이지 객체를 사용하지 않는 테스트 자동화의 전형적인 예를 생각해 본다.

<span style="color:red">이하 Java 코드는 Python 코드로 교체될 계획이다.</span>

```Java
/***
 * Tests login feature
 */
public class Login {

  public void testLogin() {
    // fill login data on sign-in page
    driver.findElement(By.name("user_name")).sendKeys("userName");
    driver.findElement(By.name("password")).sendKeys("my supersecret password");
    driver.findElement(By.name("sign-in")).click();

    // verify h1 tag is "Hello userName" after login
    driver.findElement(By.tagName("h1")).isDisplayed();
    assertThat(driver.findElement(By.tagName("h1")).getText(), is("Hello userName"));
  }
}
```

이 접근법에는 두 가지 문제가 있다.

- 테스트 방법과 AUT의 로케이터(이 예에서는 ID)는 분리되어 있지 않으며, 둘 다 단일 방법으로 얽혀 있다. AUT의 UI가 식별자, 레이아웃 또는 로그인 입력 및 처리 방법을 변경하는 경우 테스트 자체를 변경해야 한다.
- 이 로그인 페이지를 사용해야 하는 모든 테스트에서 ID 로케이터는 여러 테스트에 분산된다.

페이지 객체 기술을 적용하면, 이 예는 로그인 페이지에 대한 페이지 객체의 다음 예제와 같이 다시 작성될 수 있다.

```Java
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Page Object encapsulates the Sign-in page.
 */
public class SignInPage {
  protected WebDriver driver;

  // <input name="user_name" type="text" value="">
  private By usernameBy = By.name("user_name");
  // <input name="password" type="password" value="">
  private By passwordBy = By.name("password");
  // <input name="sign_in" type="submit" value="SignIn">
  private By signinBy = By.name("sign_in");

  public SignInPage(WebDriver driver){
    this.driver = driver;
     if (!driver.getTitle().equals("Sign In Page")) {
      throw new IllegalStateException("This is not Sign In Page," +
            " current page is: " + driver.getCurrentUrl());
    }
  }

  /**
    * Login as valid user
    *
    * @param userName
    * @param password
    * @return HomePage object
    */
  public HomePage loginValidUser(String userName, String password) {
    driver.findElement(usernameBy).sendKeys(userName);
    driver.findElement(passwordBy).sendKeys(password);
    driver.findElement(signinBy).click();
    return new HomePage(driver);
  }
}
```

홈 페이지의 페이지 개ㄱ체는 다음과 같다.

```Java
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Page Object encapsulates the Home Page
 */
public class HomePage {
  protected WebDriver driver;

  // <h1>Hello userName</h1>
  private By messageBy = By.tagName("h1");

  public HomePage(WebDriver driver){
    this.driver = driver;
    if (!driver.getTitle().equals("Home Page of logged in user")) {
      throw new IllegalStateException("This is not Home Page of logged in user," +
            " current page is: " + driver.getCurrentUrl());
    }
  }

  /**
    * Get message (h1 tag)
    *
    * @return String message text
    */
  public String getMessageText() {
    return driver.findElement(messageBy).getText();
  }

  public HomePage manageProfile() {
    // Page encapsulation to manage profile functionality
    return new HomePage(driver);
  }
  /* More methods offering the services represented by Home Page
  of Logged User. These methods in turn might return more Page Objects
  for example click on Compose mail button could return ComposeMail class object */
}
```

따라서 로그인 테스트에서는 이 두 페이지 객체를 다음과 같이 사용한다.

```Python
/***
 * Tests login feature
 */
public class TestLogin {

  @Test
  public void testLogin() {
    SignInPage signInPage = new SignInPage(driver);
    HomePage homePage = signInPage.loginValidUser("userName", "password");
    assertThat(homePage.getMessageText(), is("Hello userName"));
  }

}
```

페이지 객체를 설계하는 방법은 매우 유연하지만 테스트 코드에 대한 유지 관리성 향상을 위한 몇 가지 기본 규칙이 있다.

## 페이지 객체의 어썰션 (Assertions in Page Objects)
Page Objects 자체는 검증이나 어썰션을 만들어서는 안 된다. 이것은 테스트의 일부이며 페이지 객체가 아닌 항상 테스트 코드 내에 있어야 한다. 페이지 객체는 페이지의 표현을 포함하며, 페이지가 메소드를 통해 제공하는 서비스를 포함하지만 테스트 중인 것과 관련된 코드는 페이지 객체 내에 없어야 한다.

페이지 객체 내에 있을 수 있고 있어야 하는 하나의 단일 검증이 있으며, 이는 페이지와 페이지의 중요한 요소가 올바르게 로드되었는지 검증하는 것이다. 페이지 객체를 인스턴스화하는 동안 이를 검증해야 한다. 위의 예에서 SignInPage와 HomePage 생성자는 모두 예상 페이지를 사용할 수 있고 테스트 요청에 대한 준비가 되었는지 확인한다.

## 페이지 구성 요소 객체 (Page Component Objects)
페이지 객체가 반드시 페이지 자체의 모든 부분을 나타낼 필요는 없다. 페이지 객체에 사용되는 것과 동일한 원칙을 사용하여 페이지의 개별 청크를 나타내는 "페이지 구성요소 객체"를 만들 수 있으며 페이지 객체에 포함시킬 수 있다. 이러한 구성요소 객체는 이러한 개별 청크 내부의 요소에 대한 참조와 이들에 의해 제공되는 기능을 활용하는 방법을 제공할 수 있다. 더 복잡한 페이지를 위해 구성 요소 객체를 다른 구성 요소 객체 내부에 중첩할 수도 있다. AUT의 한 페이지에 여러 구성 요소가 있거나 사이트 전체에서 사용되는 공통 구성 요소(예: 탐색 모음)가 있는 경우 유지 관리성을 향상시키고 코드 중복을 줄일 수 있다.

## 테스트에 사용된 기타 설계 패턴 (Other Design Patterns Used in Testing)
테스트에 사용할 수 있는 설계 패턴도 있다. 페이지 객체를 인스턴스화하기 위해 페이지 팩토리를 사용하기도 한다. 이 모든 것에 대해 설명하는 것은 이 사용 설명서의 범위를 벗어납니다. 여기서, 단지 독자들이 할 수 있는 것들 중 일부를 인식하기 위한 개념들을 소개한다. 앞서 언급했듯이, 많은 사람들이 이 주제에 대해 블로그를 작성했으며, 독자들이 이러한 주제에 대해 블로그 검색을 권장한다.

## 구현 참고 사항 (Implementation Notes)
PageObjects는 동시에 두 방향으로 지원하는 것으로 생각할 수 있다. 테스트 개발자를 위해서, PageObjects는 특정 페이지에서 제공하는 서비스를 나타낸다. 개발자를 외면하면, PageObjects는 페이지(또는 페이지의 일부)의 HTML 구조에 대해 깊은 지식을 가진 유일한 객체이어야 한다. PageObject의 메서드는 페이지의 세부사항과 메커니즘을 노출하는 것이 아니라 페이지가 제공하는 "서비스"를 제공하는 것으로 생각하는 것이 가장 간단하다. 예를 들어, 웹 기반 전자 메일 시스템의 받은 편지함을 생각해 보자. 제공하는 서비스 중에는 새 전자 메일을 작성하고, 단일 전자 메일을 읽도록 선택하고, 받은 편지함에 전자 메일의 제목 줄을 나열하는 기능이 있다. 이것들이 어떻게 구현되았는지는 테스트에 중요하지 않을 것이다.

테스트 개발자들이 구현보다는 상호 작용하는 서비스에 대해 생각해 볼 것을 권장하기 때문에, PageObjects는 기본 WebDriver 인스턴스를 거의 노출시키지 않아야 한다. 이렇게 하려면 PageObject의 메서드는 다른 PageObject를 반환해야 한다. 즉, 애플리케이션을 통해 사용자의 여정을 효과적으로 모델링할 수 있다. 이 또한 페이지가 서로 관련된 방식이 변경될 경우 (예: 로그인 페이지에서 이전 서비스에 처음 로그인할 때 사용자에게 비밀번호를 변경하도록 요청할 때), 단순히 적절한 메서드의 시그네이처를 변경하는 것만으로도 테스트 컴파일에 실패하게 된다는 것을 의미한다. 다른 방법으로, 페이지 간의 관계를 변경하고 페이지 객체에 반영할 때 테스트를 실행할 필요 없이 어떤 테스트가 실패할지 알 수 있다.

이 접근 방식의 한 가지 결과는 성공적인 로그인과 실패한 로그인을 모두 모델링해야 할 수도 있고, 클릭 한 번으로 앱의 상태에 따라 다른 결과를 얻을 수도 있다는 뜻한다. 이 경우 PageObject에 여러 메서드가 있는 것이 일반적이다.

```Java
public class LoginPage {
    public HomePage loginAs(String username, String password) {
        // ... clever magic happens here
    }
    
    public LoginPage loginAsExpectingError(String username, String password) {
        //  ... failed login here, maybe because one or both of the username and password are wrong
    }
    
    public String getErrorMessage() {
        // So we can verify that the correct error is shown
    }
}
```

위에 제시된 코드는 중요한 점을 보여 주고 있다. 페이지 객체가 아닌 테스트가 페이지 상태에 대한 주장을 해야 한다.

```Java
public void testMessagesAreReadOrUnread() {
    Inbox inbox = new Inbox(driver);
    inbox.assertMessageWithSubjectIsUnread("I like cheese");
    inbox.assertMessageWithSubjectIsNotUnread("I'm not fond of tofu");
}
```

다음과 같이 다시 쓸 수 있다.

```Java
public void testMessagesAreReadOrUnread() {
    Inbox inbox = new Inbox(driver);
    assertTrue(inbox.isMessageWithSubjectIsUnread("I like cheese"));
    assertFalse(inbox.isMessageWithSubjectIsUnread("I'm not fond of tofu"));
}
```

물론 모든 지침과 마찬가지로 예외가 있으며 PageObjects에서 일반적으로 볼 수 있는 하나는 PageObject를 인스턴스화할 때 WebDriver가 올바른 페이지에 있는지 확인하는 것이다. 이 작업은 아래 [예](#ex)에서 수행된다.

마지막으로, PageObject가 전체 페이지를 나타낼 필요는 없다. 사이트 탐색과 같이 사이트 또는 페이지 내에서 자주 나타나는 섹션을 나타낼 수 있다. 기본 원칙은 테스트 세트에서 특정 페이지의 HTML 구조에 대한 지식을 가진 한 곳이 한 페이지만 있다는 것이다.

## 요약 (Summary)

- 공용 메소드는 페이지가 제공하는 서비스를 나타낸다
- 페이지의 내부를 노출하지 않도록 한다
- 일반적으로 어썰션을 포함하지 않는다
- 메서드가 다른 페이지 객체를 반환한다
- 전체 페이지를 나타낼 필요가 없다
- 동일한 작업에 대한 다른 결과를 다른 방법으로 모델링한다

## 예<a name="ex"></a>

```Java
public class LoginPage {
    private final WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;

        // Check that we're on the right page.
        if (!"Login".equals(driver.getTitle())) {
            // Alternatively, we could navigate to the login page, perhaps logging out first
            throw new IllegalStateException("This is not the login page");
        }
    }

    // The login page contains several HTML elements that will be represented as WebElements.
    // The locators for these elements should only be defined once.
        By usernameLocator = By.id("username");
        By passwordLocator = By.id("passwd");
        By loginButtonLocator = By.id("login");

    // The login page allows the user to type their username into the username field
    public LoginPage typeUsername(String username) {
        // This is the only place that "knows" how to enter a username
        driver.findElement(usernameLocator).sendKeys(username);

        // Return the current page object as this action doesn't navigate to a page represented by another PageObject
        return this;	
    }

    // The login page allows the user to type their password into the password field
    public LoginPage typePassword(String password) {
        // This is the only place that "knows" how to enter a password
        driver.findElement(passwordLocator).sendKeys(password);

        // Return the current page object as this action doesn't navigate to a page represented by another PageObject
        return this;	
    }

    // The login page allows the user to submit the login form
    public HomePage submitLogin() {
        // This is the only place that submits the login form and expects the destination to be the home page.
        // A seperate method should be created for the instance of clicking login whilst expecting a login failure. 
        driver.findElement(loginButtonLocator).submit();

        // Return a new page object representing the destination. Should the login page ever
        // go somewhere else (for example, a legal disclaimer) then changing the method signature
        // for this method will mean that all tests that rely on this behaviour won't compile.
        return new HomePage(driver);	
    }

    // The login page allows the user to submit the login form knowing that an invalid username and / or password were entered
    public LoginPage submitLoginExpectingFailure() {
        // This is the only place that submits the login form and expects the destination to be the login page due to login failure.
        driver.findElement(loginButtonLocator).submit();

        // Return a new page object representing the destination. Should the user ever be navigated to the home page after submiting a login with credentials 
        // expected to fail login, the script will fail when it attempts to instantiate the LoginPage PageObject.
        return new LoginPage(driver);	
    }

    // Conceptually, the login page offers the user the service of being able to "log into"
    // the application using a user name and password. 
    public HomePage loginAs(String username, String password) {
        // The PageObject methods that enter username, password & submit login have already defined and should not be repeated here.
        typeUsername(username);
        typePassword(password);
        return submitLogin();
    }
}
```

## WebDriver에서 지원
이 패턴에 대한 지원을 제공하는 지원 패키지에는 PageFactory가 있으며, 동시에 Page Object에서 일부 보일러 플레이트 코드를 제거할 수도 있다.
# 장려하는 사항 (Encouraged behavior)
<span style="font-size:120%">Selenium 프로젝트의 테스트에 대한 몇 가지 지침과 권장 사항.</span>

모범 사례에 대한 참고 사항: 이 문서에서는 "Best Practices"라는 문구를 의도적으로 피했다. 모든 상황에서 효과가 있는 것은 아니다. "가이드라인과 권고사항"이라는 아이디어를 선호한다. 이러한 내용을 알고 특정 환경에 적합한 접근 방식을 신중하게 결정할 것을 권장한다.

기능 테스트는 여러 가지 이유로 정확하게 하기가 어렵다. 마치 애플리케이션 상태, 복잡성 및 의존성이 테스트를 충분히 어렵게 하는 것처럼, 브라우저(특히 브라우저 간 비호환성)를 다루는 것은 좋은 테스트를 작성하는 것을 어렵게 만든다.

---

### [Page Objets 모델](encouraged/page-object-models.md)

### [특정 도에인 언어 (Doamin Specific Language)](encouraged/domain-specific-language.md)

### [애플리케이션 상태 생성](encouraged/generating-application-state.md)

### [외부 서비스 흉내](encouraged/mock-external-services.md)

### [개선된 리포팅](encouraged/improved-reporting.md)

### [상태 공유 회피](encouraged/avoid-sharing-state.md)

### [로케이터 작업 팁](encouraged/locators.md)
코드에서 어떤 로케이터를 사용할 것인지와 어떻게 관리할 것인지를 알려준다.

### [테스트 독립성](encouraged/test-independency.md)

### [유연한 API 사용에 대한 고려](encouraged/consider-using-a-fluent-api.md)

### [테스트별 브라우저 새로 고침](encouraged/fresh-browser-per-test.md)


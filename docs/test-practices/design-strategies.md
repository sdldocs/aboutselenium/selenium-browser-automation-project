# 설계 패턴 및 개발 전략

## 개요
프로젝트는 시간이 지남에 따라 많은 수의 테스트를 축적하는 경향이 있다. 테스트 횟수가 증가함에 따라 코드베이스를 변경하기가 어려워진다. 단 한 번의 "단순한" 변경은 애플리케이션 프로그램이 제대로 작동하더라도 수많은 테스트가 실패할 수 있다. 이러한 문제는 피할 수 없는 경우도 있지만, 문제가 발생하면 가능한 한 빨리 다시 시작해 실행하기를 바란다. 다음과 같은 설계 패턴과 전략은 이전에 WebDriver와 함께 사용되어 테스트를 작성하고 유지관리하기 쉽게 만들었다. 설계 패턴과 전략은 여러분을 도울 수 있다.

- [DomainDrivenDesign](encouraged/domain-specific-language.md): 앱 최종 사용자의 언어로 테스트 내용을 표현한다. 
- [PageObjects](encouraged/page-object-models.md): 웹 앱의 UI에 대한 간단한 추상화이다. 
- 로드 가능한 구성요소: 페이지 개체를 구성요소로 모델링한다. 
- BotStyleTests: PageObjects가 권장하는 객체 기반 접근 방식이 아닌 명령 기반 접근 방식을 사용한 테스트 자동화

## 로드 가능한 구성요소

### 무엇?
LoadableComponent는 PageObjects 사용하기 쉽도록 하는 기본 클래스이다. 페이지 로드를 보장하는 표준 방법을 제공하고 페이지 로드 실패를 더 편학게 디버깅할 수 있도록 후크를 제공함으로써 이를 수행한다. 이 도구를 사용하면 테스트에서 상용 코드의 양을 줄일 수 있으므로 테스트를 유지 관리하는 노력을 줄일 수 있다.

현재 Java에는 Selenium 2의 일부로 제공되는 구현체가 있지만, 사용되는 접근법은 어떤 언어로도 구현될 수 있을 정도로 간단하다.

### 간단한 사용법
모델링할 UI의 예로 [새 이슈](https://github.com/SeleniumHQ/selenium/issues/new) 페이지를 사용한다. 테스트 작성자의 관점에서, 이는 새로운 문제를 제출할 수 있는 서비스를 제공한다. 기본 PageObject는 다음과 같다.

<span style="color:red">이하 Java 코드는 Python 코드로 교체될 계획이다.</span>

```Java
package com.example.webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EditIssue {

  private final WebDriver driver;

  public EditIssue(WebDriver driver) {
    this.driver = driver;
  }

  public void setSummary(String summary) {
    WebElement field = driver.findElement(By.name("summary"));
    clearAndType(field, summary);
  }

  public void enterDescription(String description) {
    WebElement field = driver.findElement(By.name("comment"));
    clearAndType(field, description);
  }

  public IssueList submit() {
    driver.findElement(By.id("submit")).click();
    return new IssueList(driver);
  }

  private void clearAndType(WebElement field, String text) {
    field.clear();
    field.sendKeys(text);
  }
}
```

이 구성요소를 로드 가능한 구성요소로 변경하려면 기본 타입으로 설정하면 된다.

```Java
public class EditIssue extends LoadableComponent<EditIssue> {
  // rest of class ignored for now
}

이 스그니처는 약간 특이해 보이지만, 이 클래스가 EditIssue 페이지를 로드하는 LoadableComponent를 나타낸다는 것을 의미한다.

이 기본 클래스를 확장하여 두 가지 새로운 방법을 구현해야 한다.

```Java
  @Override
  protected void load() {
    driver.get("https://github.com/SeleniumHQ/selenium/issues/new");
  }

  @Override
  protected void isLoaded() throws Error {
    String url = driver.getCurrentUrl();
    assertTrue("Not on the issue entry page: " + url, url.endsWith("/new"));
  }
```

`load` 메소드는 페이지를 탐색하는 데 사용되는 반면, `isLoaded` 메소드는 바른 페이지에 있는지 여부를 확인하는 데 사용된다. 메소드는 결과로 부울 값을 반환해야 하는 것처럼 보이지만, 대신 JUNit의 Assert 클래스를 사용하여 일련의 어설션을 수행한다. 원하는 만큼 어썰션이 적거나 많을 수 있다. 이러한 어썰션을 사용하면 클래스 사용자에게 테스트 디버그에 사용할 수 있는 명확한 정보를 제공할 수 있다.

약간의 수정을 한 PageObject는 다음과 같다.

```Java
package com.example.webdriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static junit.framework.Assert.assertTrue;

public class EditIssue extends LoadableComponent<EditIssue> {

  private final WebDriver driver;
  
  // By default the PageFactory will locate elements with the same name or id
  // as the field. Since the summary element has a name attribute of "summary"
  // we don't need any additional annotations.
  private WebElement summary;
  
  // Same with the submit element, which has the ID "submit"
  private WebElement submit;
  
  // But we'd prefer a different name in our code than "comment", so we use the
  // FindBy annotation to tell the PageFactory how to locate the element.
  @FindBy(name = "comment") private WebElement description;
  
  public EditIssue(WebDriver driver) {
    this.driver = driver;
    
    // This call sets the WebElement fields.
    PageFactory.initElements(driver, this);
  }

  @Override
  protected void load() {
    driver.get("https://github.com/SeleniumHQ/selenium/issues/new");
  }

  @Override
  protected void isLoaded() throws Error {
    String url = driver.getCurrentUrl();
    assertTrue("Not on the issue entry page: " + url, url.endsWith("/new"));
  }
  
  public void setSummary(String issueSummary) {
    clearAndType(summary, issueSummary);
  }

  public void enterDescription(String issueDescription) {
    clearAndType(description, issueDescription);
  }

  public IssueList submit() {
    submit.click();
    return new IssueList(driver);
  }

  private void clearAndType(WebElement field, String text) {
    field.clear();
    field.sendKeys(text);
  }
}
```

별로인 것 보인다. 한 가지 방법은 페이지로 이동하는 방법에 대한 정보를 페이지 자체에 캡슐화하는 것이다. 즉, 이 정보는 코드 베이스내에 분산되지 않는다. 이는 테스트에서 다음과 같은 작업을 수행할 수 있음을 의미한다.

```Java
EditIssue page = new EditIssue(driver).get();
```

필요한 경우 이 호출을 통해 드라이버가 페이지로 이동한다.

### 중첩된 구성요소
LoadableComponents는 다른 LoadableComponents와 함께 사용할 때 더욱 유용해지질 수 있다. 이 예를 사용하여 (결국 해당 사이트의 탭을 통해 액세스하는) "이슈 편집" 페이지를 프로젝트 웹 사이트 내의 구성 요소로 만들 수 있다. 또한 문제를 제출하려면 로그인해야 한다. 중첩된 구성요소의 트리로 모델링할 수 있다.

```Java
+ ProjectPage
 +---+ SecuredPage
     +---+ EditIssue
```

코드로 보면 어떻게 보일까? 먼저, 각 논리적 구성요소는 고유한 클래스를 갖는다. 각각의 "로드" 방법은 부모로부터 상속되어 "얻을" 수 있을 것이다. 위의 EditIssue 클래스에 더하여 최종 결과는 다음과 같다.

ProjectPage.java:

```Java
package com.example.webdriver;

import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertTrue;

public class ProjectPage extends LoadableComponent<ProjectPage> {

  private final WebDriver driver;
  private final String projectName;

  public ProjectPage(WebDriver driver, String projectName) {
    this.driver = driver;
    this.projectName = projectName;
  }

  @Override
  protected void load() {
    driver.get("http://" + projectName + ".googlecode.com/");
  }

  @Override
  protected void isLoaded() throws Error {
    String url = driver.getCurrentUrl();

    assertTrue(url.contains(projectName));
  }
}
```

그리고 SecuredPage.java:

```Java
package com.example.webdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.fail;

public class SecuredPage extends LoadableComponent<SecuredPage> {

  private final WebDriver driver;
  private final LoadableComponent<?> parent;
  private final String username;
  private final String password;

  public SecuredPage(WebDriver driver, LoadableComponent<?> parent, String username, String password) {
    this.driver = driver;
    this.parent = parent;
    this.username = username;
    this.password = password;
  }

  @Override
  protected void load() {
    parent.get();

    String originalUrl = driver.getCurrentUrl();

    // Sign in
    driver.get("https://www.google.com/accounts/ServiceLogin?service=code");
    driver.findElement(By.name("Email")).sendKeys(username);
    WebElement passwordField = driver.findElement(By.name("Passwd"));
    passwordField.sendKeys(password);
    passwordField.submit();

    // Now return to the original URL
    driver.get(originalUrl);
  }

  @Override
  protected void isLoaded() throws Error {
    // If you're signed in, you have the option of picking a different login.
    // Let's check for the presence of that.

    try {
      WebElement div = driver.findElement(By.id("multilogin-dropdown"));
    } catch (NoSuchElementException e) {
      fail("Cannot locate user name link");
    }
  }
}
```

EditIssue의 "load" 메서드는 다음과 같다.

```Java
  @Override
  protected void load() {
    securedPage.get();

    driver.get("https://github.com/SeleniumHQ/selenium/issues/new");
  }
```

이것은 모든 구성요소가 서로 "내포"되어 있음을 보인다. EditIssue에서 get()를 호출하면 모든 종속성도 로드된다. 

```Java
public class FooTest {
  private EditIssue editIssue;

  @Before
  public void prepareComponents() {
    WebDriver driver = new FirefoxDriver();

    ProjectPage project = new ProjectPage(driver, "selenium");
    SecuredPage securedPage = new SecuredPage(driver, project, "example", "top secret");
    editIssue = new EditIssue(driver, securedPage);
  }

  @Test
  public void demonstrateNestedLoadableComponents() {
    editIssue.get();

    editIssue.setSummary("Summary");
    editIssue.enterDescription("This is an example");
  }
}
```

테스트에서 [Guiceberry](https://github.com/zorzella/guiceberry)와 같은 라이브러리를 사용하는 경우 PageObjects 설정들이 생략되어 읽기 쉬운 테스트가 수행될 수 있다.

## Bot 패턴

PageObjects는 테스트에서 중복을 줄일 수 있는 유용한 방법이지만 팀이 항상 따르는 패턴은 아니다. 대안적인 접근법은 보다 "명령과 같은" 방식의 테스트를 따르는 것이다.

"Bot"은 원시 Selenium API에 대한 action-oriented 추상화이다. 즉, 명령이 앱에 대해 바른 작업을 수행하지 않는 경우 명령을 쉽게 변경할 수 있다. 예:

```Java
public class ActionBot {
  private final WebDriver driver;

  public ActionBot(WebDriver driver) {
    this.driver = driver;
  }

  public void click(By locator) {
    driver.findElement(locator).click();
  }

  public void submit(By locator) {
    driver.findElement(locator).submit();
  }

  /** 
   * Type something into an input field. WebDriver doesn't normally clear these
   * before typing, so this method does that first. It also sends a return key
   * to move the focus out of the element.
   */
  public void type(By locator, String text) { 
    WebElement element = driver.findElement(locator);
    element.clear();
    element.sendKeys(text + "\n");
  }
}
```

이러한 추상화가 구축되고 테스트에서 중복이 확인되면 봇의 최상위 페이지 객체를 계층화할 수 있다.

# Selenium IDE
<span style="font-size:120%">Selenium IDE는 사용자의 명령을 기록하고 재생하는 브라우저 확장이다.</span>

Selenium의 Integrated Development Environment([Selenium IDE](https://selenium.dev/selenium-ide))는 기존 Selenium 명령을 사용하여 브라우저에서 각 요소의 컨텍스트가 정의한 매개 변수와 함께 사용자의 동작을 기록하는 사용하기 쉬운 브라우저 확장이다. Selenium 구문을 배우는 훌륭한 방법을 제공한다. Google Chrome, Mozilla Firefox 및 Microsoft Edge에서 사용할 수 있다.

자세한 내용은 전체 [Selenium IDE 문서](https://www.selenium.dev/selenium-ide/docs/en/introduction/getting-started)를 참조하세요.
